<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="BMC1556-276X-6-105">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="EditorialNotes">
            <ArticleID>39</ArticleID>
            <ArticleDOI>10.1186/1556-276X-6-105</ArticleDOI>
            <ArticleCitationID>105</ArticleCitationID>
            <ArticleSequenceNumber>104</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Preface to Symposium E: Nanoscaled Si, Ge based Materials</ArticleTitle>
            <ArticleCategory>Editorial</ArticleCategory>
            <ArticleCollection Type="Regular" ID="AC_86dded82898b270afb72ed99bf8f738c">
              <ArticleCollectionTitle>Symposium E : Nanoscaled Si, Ge based Materials</ArticleCollectionTitle>
              <ArticleCollectionEditor>Artur Podhorodecki</ArticleCollectionEditor>
            </ArticleCollection>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2011</Year>
                <Month>1</Month>
                <Day>31</Day>
              </RegistrationDate>
              <Received>
                <Year>2011</Year>
                <Month>1</Month>
                <Day>31</Day>
              </Received>
              <Accepted>
                <Year>2011</Year>
                <Month>1</Month>
                <Day>31</Day>
              </Accepted>
              <OnlineDate>
                <Year>2011</Year>
                <Month>1</Month>
                <Day>31</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Podhorodecki; licensee Springer.</CopyrightHolderName>
              <CopyrightYear>2011</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd. This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/2.0</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/2.0"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>11671</JournalID>
              <VolumeIDStart>6</VolumeIDStart>
              <VolumeIDEnd>6</VolumeIDEnd>
              <IssueIDStart>1</IssueIDStart>
              <IssueIDEnd>1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Fabrice</GivenName>
                  <FamilyName>Gourbilleau</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>fabrice.gourbilleau@ensicaen.fr</Email>
                </Contact>
              </Author>
              <Author CorrespondingAffiliationID="Aff2" AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Artur</GivenName>
                  <FamilyName>Podhorodecki</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>artur.p.podhorodecki@pwr.wroc.pl</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.424453.0</OrgID>
                <OrgID Type="ISNI" Level="Institution">000000008694431X</OrgID>
                <OrgName>CIMAP, CNRS/CEA/ENSICAEN/UCBN Ecole Nationale Superieure d’Ingenieurs de Caen</OrgName>
                <OrgAddress>
                  <City>Caen</City>
                  <Country Code="FR">France</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.7005.2</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000098053178</OrgID>
                <OrgDivision>Laboratory of Advanced Optical Spectroscopy, Institute of Physics</OrgDivision>
                <OrgName>Wroclaw University of Technology</OrgName>
                <OrgAddress>
                  <Country Code="PL">Wybrzeze Wyspianskiego 27</Country>
                  <Postcode>50-370</Postcode>
                  <City>Wroclaw</City>
                  <Country Code="PL">Poland</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>This special volume of <Emphasis Type="Italic">Nanoscale Research Letters</Emphasis> features selected papers presented at the Symposium E: &quot;Nanoscaled Si, Ge based Materials&quot; of the EMRS Fall meeting, held in Warsaw, Poland from September 13-<Superscript>h</Superscript>16, 2010.</Para>
            <Para>The symposium was organized by Fabrice Gourbilleau (CIMAP laboratory, Caen France), Artur Podhorodecki (Wroclaw University of Technology, Institute of Physics, Poland) and Nicola Daldosso (University of Trento, Physical Department, Trento, Italy). CNANO North West (GDR 2975, CNRS, FRANCE) kindly sponsored the symposium.</Para>
            <Para>The goal of the symposium was to describe exciting, state-of- the- art applications of Si or Ge nanoparticle-based materials, doped or not with rare earth ions, in the fields of (i) information storage, (ii) optoelectronic devices, (iii) telecommunications, as well as (iv) life sciences. using the. The origin of such intense research activity in these different domains can be explained by the potential of Group IV nanostructures to lead to either (i) future chips with optical interconnects in which CMOS compatible electronic and photonic and/or biosensing components aree integrated or (ii) the future generation of solar cells with lower cost and/or higher efficiency.</Para>
            <Para>The four-day symposium included 13 sessions in which 48 oral and 18 poster presentations gave a comprehensive overview of progress toward a wide range of applications as well as the development of new promising techniques for microstructural investigations at the atomic scale. The papers published in this special volume have been selected after careful peer review, and form an authoritative reference for future applications.</Para>
            <Para>The organizers wish to thank the EMRS staff for their help in the organization of the symposium.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


