The code can be tested using the content_analysis.py by providing the data directory to the method 'process_data(<data_dir>)'.

Already generated output can be fetched using the REST API, which can be activated using: 'python test.py', and use below operations:
	http://localhost:4567/related_sentences_in_docs	to find out the sentences which mention about consent/ disclosure for the research articles related to any of the species (Human/ Animal)
	
	http://localhost:4567/test_faulty_docs to find out the documents which mentioned about experiments with ANimal/HUman, but did not have anything about disclosures.
	
	http://localhost:4567/documents_with_no_info_on_species to find out the documents which had no mention about the experiments with Animal/ HUman.
	
	http://localhost:4567/documents_with_info_on_species to find out the documents which mention about the experiments with Animal/ Human.