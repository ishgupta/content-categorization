<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-017-0493-4">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>493</ArticleID>
            <ArticleDOI>10.1007/s13238-017-0493-4</ArticleDOI>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Gene editing using CRISPR/Cas9: implications for dual-use and biosecurity</ArticleTitle>
            <ArticleCategory>Commentary</ArticleCategory>
            <ArticleFirstPage>239</ArticleFirstPage>
            <ArticleLastPage>240</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2017</Year>
                <Month>11</Month>
                <Day>25</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2017</Year>
                <Month>12</Month>
                <Day>4</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>The Author(s)</CopyrightHolderName>
              <CopyrightYear>2017</CopyrightYear>
              <License Version="4.0" Type="OpenAccess" SubType="CC BY">
                <SimplePara>
                    <Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution 4.0 International License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/4.0/</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/4.0/"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided you give appropriate credit to the original author(s) and the source, provide a link to the Creative Commons license, and indicate if changes were made.</SimplePara>
              </License>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author ID="Au1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Diane</GivenName>
                  <FamilyName>DiEuliis</FamilyName>
                </AuthorName>
              </Author>
              <Author ID="Au2" CorrespondingAffiliationID="Aff2" AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>James</GivenName>
                  <FamilyName>Giordano</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>james.giordano@georgetown.edu</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="ISNI" Level="Institution">0000 0001 2176 6230</OrgID>
                <OrgID Type="GRID" Level="Institution">grid.431364.7</OrgID>
                <OrgDivision>Center for the Study of Weapons of Mass Destruction</OrgDivision>
                <OrgName>National Defense University</OrgName>
                <OrgAddress>
                  <City>Washington</City>
                  <State>DC</State>
                  <Postcode>20057</Postcode>
                  <Country Code="US">USA</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="ISNI" Level="Institution">0000 0001 2186 0438</OrgID>
                <OrgID Type="GRID" Level="Institution">grid.411667.3</OrgID>
                <OrgDivision>Departments of Neurology and Biochemistry and Neuroethics Studies Program-Pellegrino Center for Clinical Bioethics</OrgDivision>
                <OrgName>Georgetown University Medical Center</OrgName>
                <OrgAddress>
                  <City>Washington</City>
                  <State>DC</State>
                  <Postcode>20057</Postcode>
                  <Country Code="US">USA</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para ID="Par1">We have read with interest the recent paper by Kang, et al (<CitationRef CitationID="CR7">2017</CitationRef>), addressing clinical and ethical issues related to the safe and responsible use of CRISPR/Cas. The authors provide a number of important considerations about the current capabilities offered by this novel gene-editing tool, including germ line editing in embryos, and potential diverse uses in adult human applications. The authors posit that the tool in of itself does not represent a threat, and that periodic assessment will ensure its responsible use.</Para>
            <Para ID="Par2">We agree, but with caveat: namely, that any tool that imparts great capability also involves at least some risk, if not threat, that the power conferred by such capacity can be used to leverage or evoke a variety of ends. This deviation of intent is a principal concern of dual-use research and its applications. Apropos such potential, we provide a complementary view that considers capricious, if not nefarious use of CRISPR to modify biological entities (e.g., microbes, plants, animals, and humans) and/or produce bioagents that pose risk and/or threat to public safety (DiEuliis and Giordano, <CitationRef CitationID="CR2">2017</CitationRef>). We view this as a real, present and certainly near-term future concern, and propose a set of additional considerations that we feel are important security components that will be essential to assessing the use of CRISPR/Cas.</Para>
            <Para ID="Par3">We propose that agreed-upon international, ethical “norms” for human modification for therapeutic purposes are relevant and applicable to any such use of this technique. Kang et al (<CitationRef CitationID="CR7">2017</CitationRef>) advocate international standards of ethics, and note efforts made to date by the National Academies (<CitationRef CitationID="CR10">2017</CitationRef>) in this regard. We concur with the need for international ethical standards and guidelines, and also note the need for more engaged discourse to define the needs, values and ethical system(s) and principles to be employed (Palchik, Chen, and Giordano, <CitationRef CitationID="CR11">2017</CitationRef>; Lanzilao, Shook, Benedikter, and Giordano, <CitationRef CitationID="CR8">2013</CitationRef>) Furthermore, in recognition of the potential risk/threat posed by genetic modification, we strongly endorse involvement of the Biological Toxins and Weapons Convention (BTWC), in order to ensure inclusion of biosafety and biosecurity communities in any such deliberation and determination of standards. Templates may exist and could be consulted for the development of international norms and best practices through engagement of expertise in technical aspects of emergent technology and security fields (Talinn Manual, NATO, Carnegie Endowment <CitationRef CitationID="CR14">2017</CitationRef>). Expanding the scope and platform of international dialogue can be instrumental to ensuring that all aspects of emerging biotechnological tools and methods are evaluated for their potential to be weaponized or used in other ways that threaten public safety (Gerstein and Giordano, <CitationRef CitationID="CR3">2017</CitationRef>).</Para>
            <Para ID="Par4">Importantly, many of the agents modified or designed for therapeutics will have dual uses of concern. A particularly good example are those that are able to modify human biological, cognitive and/or behavioral function(s). A recent study of US public attitudes toward genetic enhancement showed that while the use of CRISPR for therapeutics was viewed as being appropriate, its use for bio-enhancement was regarded as far less acceptable (Scheufele et al., <CitationRef CitationID="CR13">2017</CitationRef>). But public attitudes often do not reflect the needs and values desiderata of the military, and CRISPR and related techniques offer considerable potential to both create novel weaponizable entities, and to modify aspects of human performance in ways that are relevant and meaningful to warfare operations. In considering implications on a global scale, countries could embed these genetic modification programs within academic and/or commercial research and development infrastructures to make dual-use applications difficult to detect (Giordano, <CitationRef CitationID="CR4">2016</CitationRef>). As well, the relative availability of this technique enable increasing use by public research and do-it-yourself (i.e., biohacking) communities which could foster risk incurred by both inadvertent misuse and/or intentional development of products that threaten public safety (Giordano, <CitationRef CitationID="CR5">2017</CitationRef>). These dual use aspects should be included in critical discussions.</Para>
            <Para ID="Par5">Uses of CRISPR in contexts outside of human biology—to modify plants, agricultural products, animals, and insects, prompted recent review of the Coordinated Framework for the Regulation of Biotechnology (National Academies, <CitationRef CitationID="CR10">2017</CitationRef>), which concluded that synthetic biology will innovate a much wider number of products requiring regulatory oversight and governance. In addition, new commercial enterprises and lowered barriers to market entry may alter the number and types of regulated entities to blur jurisdictional concepts of “product sponsor,” “product developer,” or “manufacturer”, and obfuscate responsibilities for development and use.</Para>
            <Para ID="Par6">But techniques such as CRISPR need not be only regarded for their dual use potential. To be sure, gene editing can directly affect the traditional bioweapons arena. There have been discussions of the potential for CRISPR to be used to create or further manipulate viruses, bacteria and bacterially produced toxins (i.e., select agents) (Clapper, <CitationRef CitationID="CR1">2016</CitationRef>). Previous bioweapons programs focused on molecular experimentation with select agents to make them more weaponizable. Now, techniques such as CRISPR can both enable improved capacity to manipulate pathogens for this purpose, and may make such manipulations easier and quicker to accomplish. As well, CRISPR-based approaches could be used to incur disease by inducing harmful genetic modifications <Emphasis Type="Italic">in vivo</Emphasis>. Just as CRISPR kits can be made to order from industry providers, means of directly delivering CRISPR to incur genetic modifications are also being developed (Mullin, <CitationRef CitationID="CR9">2017</CitationRef>). Nefarious actors using these techniques would not care about “off target” effects, as long as the agent achieves disruptive or destructive effect(s). In such cases, simply engineering a molecule capable of producing harmful modification to human DNA might provide a viable (and none too difficult) biothreat option.</Para>
            <Para ID="Par7">In conclusion, we argue that further discussion is needed to evaluate both the scope and extent of current capabilities—and the challenges and opportunities to further develop and/or exploit such techniques—to be used in ways that may impact public safety and health. For as science and technology advance, there will be need to iteratively re-assess current perspectives and definitions of what constitutes weaponizable biology, and to engage international discourse toward more effective, efficient guidance and governance of these agents and the activities and entities that are capable of producing them.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Acknowledgments>
              <Heading>Acknowledgements</Heading>
              <SimplePara>This work was supported in part by funding from the AEHS Foundation, in conjunction with Project Neuro-HOPE (JG); by the European Union’s Horizon 2020 Research and Innovation Programme under grant agreement 720270: HBP SGA1 (JG), and by federal funds UL1TR001409 from the National Center for Advancing Translational Sciences (NCATS), National Institutes of Health, through the Clinical and Translational Science Awards Program (CTSA), a trademark of the Department of Health and Human Services, part of the Roadmap Initiative, “Re-Engineering the Clinical Research Enterprise (JG)”.</SimplePara>
            </Acknowledgments>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibUnstructured>Clapper J (2016) Director of National Intelligence Statement for the Record, Worldwide Threat Assessment of the US Intelligence Community Senate Armed Services Committee. 9 Feb 2016</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>DiEuliis</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">Why gene editors like CRISPR/Cas9 may be a game-changer for neuroweapons</ArticleTitle>
                  <JournalTitle>Health Secur</JournalTitle>
                  <VolumeID>15</VolumeID>
                  <IssueID>3</IssueID>
                  <FirstPage>296</FirstPage>
                  <LastPage>302</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1089/hs.2016.0120</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>28574731</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>DiEuliis D, Giordano J (2017) Why gene editors like CRISPR/Cas9 may be a game-changer for neuroweapons. Health Secur 15(3):296–302</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Gerstein</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">Re-thinking the biological toxin and weapons convention?</ArticleTitle>
                  <JournalTitle>Health Secur</JournalTitle>
                  <VolumeID>15</VolumeID>
                  <IssueID>6</IssueID>
                  <FirstPage>1</FirstPage>
                  <LastPage>3</LastPage>
                </BibArticle>
                <BibUnstructured>Gerstein D, Giordano J (2017) Re-thinking the biological toxin and weapons convention? Health Secur 15(6):1–3</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2016</Year>
                  <ArticleTitle xml:lang="en" Language="En">The neuroweapons threat</ArticleTitle>
                  <JournalTitle>Bull Atomic Sci</JournalTitle>
                  <VolumeID>72</VolumeID>
                  <IssueID>3</IssueID>
                  <FirstPage>1</FirstPage>
                  <LastPage>4</LastPage>
                </BibArticle>
                <BibUnstructured>Giordano J (2016) The neuroweapons threat. Bull Atomic Sci 72(3):1–4</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">Battlescape brain: engaging neuroscience in defense operations</ArticleTitle>
                  <JournalTitle>HDIAC J</JournalTitle>
                  <VolumeID>3</VolumeID>
                  <IssueID>4</IssueID>
                  <FirstPage>13</FirstPage>
                  <LastPage>16</LastPage>
                </BibArticle>
                <BibUnstructured>Giordano J (2017) Battlescape brain: engaging neuroscience in defense operations. HDIAC J 3(4):13–16</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <BibUnstructured>Industry Association of Synthetic Biology (IASB) code of conduct for best practices in gene synthesis; <ExternalRef>
                    <RefSource>https://repository.library.georgetown.edu/handle/10822/515024</RefSource>
                    <RefTarget TargetType="URL" Address="https://repository.library.georgetown.edu/handle/10822/515024"/>
                  </ExternalRef> . Accessed 20 Oct 2017</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>XJ</Initials>
                    <FamilyName>Kang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>CIN</Initials>
                    <FamilyName>Caparas</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>BS</Initials>
                    <FamilyName>Soh</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Fan</FamilyName>
                  </BibAuthorName>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">Addressing challenges in the clinical applications associated with CRISPR/Cas9 technology and ethical questions to prevent its misuse</ArticleTitle>
                  <JournalTitle>Protein Cell</JournalTitle>
                  <VolumeID>8</VolumeID>
                  <FirstPage>791</FirstPage>
                  <LastPage>795</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s13238-017-0477-4</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>28986765</Handle>
                  </Occurrence>
                  <Occurrence Type="PMCID">
                    <Handle>5676596</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Kang XJ, Caparas CIN, Soh BS, Fan Y (2017) Addressing challenges in the clinical applications associated with CRISPR/Cas9 technology and ethical questions to prevent its misuse. Protein Cell 8:791–795</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>E</Initials>
                    <FamilyName>Lanzilao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Shook</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Benedikter</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2013</Year>
                  <ArticleTitle xml:lang="en" Language="En">Advancing neuroscience on the 21st century world stage: the need for—and proposed structure of—an internationally relevant neuroethics</ArticleTitle>
                  <JournalTitle>Ethics Biol Eng Med</JournalTitle>
                  <VolumeID>4</VolumeID>
                  <IssueID>3</IssueID>
                  <FirstPage>211</FirstPage>
                  <LastPage>229</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1615/EthicsBiologyEngMed.2014010710</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Lanzilao E, Shook J, Benedikter R, Giordano J (2013) Advancing neuroscience on the 21st century world stage: the need for—and proposed structure of—an internationally relevant neuroethics. Ethics Biol Eng Med 4(3):211–229</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <BibUnstructured>Mullin E (2017) Five ways to get CRISPR into the body. MIT Technology Review. <ExternalRef>
                    <RefSource>https://www.technologyreview.com/s/608898/five-ways-to-get-crispr-into-the-body/</RefSource>
                    <RefTarget TargetType="URL" Address="https://www.technologyreview.com/s/608898/five-ways-to-get-crispr-into-the-body/"/>
                  </ExternalRef>. Accessed Sept 2017</BibUnstructured>
              </Citation>
              <Citation ID="CR10">
                <BibUnstructured>National Academies of Sciences, Engineering, and Medicine (2017) Preparing for Future Products of Biotechnology. Washington, DC: The National Academies Press. <ExternalRef>
                    <RefSource>https://doi.org/10.17226/24605</RefSource>
                    <RefTarget TargetType="DOI" Address="10.17226/24605"/>
                  </ExternalRef>
                </BibUnstructured>
              </Citation>
              <Citation ID="CR11">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>G</Initials>
                    <FamilyName>Palchik</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>C</Initials>
                    <FamilyName>Chen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Giordano</FamilyName>
                  </BibAuthorName>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">Monkey business? Development, influence and ethics of potentially dual-use brain science on the world stage</ArticleTitle>
                  <JournalTitle>Neuroethics</JournalTitle>
                  <VolumeID>10</VolumeID>
                  <FirstPage>1</FirstPage>
                  <LastPage>4</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s12152-017-9310-2</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Palchik G, Chen C, Giordano J (2017) Monkey business? Development, influence and ethics of potentially dual-use brain science on the world stage. Neuroethics 10:1–4</BibUnstructured>
              </Citation>
              <Citation ID="CR12">
                <BibUnstructured>Protecting financial data in cyberspace, Carnegie Endowment. <ExternalRef>
                    <RefSource>http://carnegieendowment.org/2017/08/24/protecting-financial-data-in-cyberspace-precedent-for-further-progress-on-cyber-norms-pub-72907</RefSource>
                    <RefTarget TargetType="URL" Address="http://carnegieendowment.org/2017/08/24/protecting-financial-data-in-cyberspace-precedent-for-further-progress-on-cyber-norms-pub-72907"/>
                  </ExternalRef>; Accessed 21 Oct 2017</BibUnstructured>
              </Citation>
              <Citation ID="CR13">
                <BibArticle>
                  <BibAuthorName>
                    <NoInitials/>
                    <FamilyName>Scheufele</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2017</Year>
                  <ArticleTitle xml:lang="en" Language="En">U.S. attitudes on human genome editing</ArticleTitle>
                  <JournalTitle>Science</JournalTitle>
                  <VolumeID>357</VolumeID>
                  <FirstPage>6351</FirstPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1126/science.aan3708</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Scheufele et al (2017) U.S. attitudes on human genome editing. Science 357:6351</BibUnstructured>
              </Citation>
              <Citation ID="CR14">
                <BibUnstructured>Talinn Manual, NATO. <ExternalRef>
                    <RefSource>https://ccdcoe.org/research.html</RefSource>
                    <RefTarget TargetType="URL" Address="https://ccdcoe.org/research.html"/>
                  </ExternalRef>. Accessed 20 Oct 2017</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>17391.zip</EditorialManuscriptNumber>
            </JobSheet></Publisher>


