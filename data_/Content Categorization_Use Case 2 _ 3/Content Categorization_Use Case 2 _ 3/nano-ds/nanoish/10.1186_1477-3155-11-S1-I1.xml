<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <Article OutputMedium="All" ID="BMC1477-3155-11-S1-I1">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>253</ArticleID>
            <ArticleDOI>10.1186/1477-3155-11-S1-I1</ArticleDOI>
            <ArticleCitationID>I1</ArticleCitationID>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">CNRS School &quot;Nanophysics for Health&quot;, 5-9 November 2012, Mittelwhir, France</ArticleTitle>
            <ArticleCategory>Preface</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2013</Year>
                <Month>12</Month>
                <Day>10</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2013</Year>
                <Month>12</Month>
                <Day>10</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Léonard et al.; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2013</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd. This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/2.0</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/2.0"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited. The Creative Commons Public Domain Dedication waiver (<ExternalRef>
                    <RefSource>http://creativecommons.org/publicdomain/zero/1.0/</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/publicdomain/zero/1.0/"/>
                  </ExternalRef>) applies to the data made available in this article, unless otherwise stated.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>12951</JournalID>
              <VolumeIDStart>11</VolumeIDStart>
              <VolumeIDEnd>11</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Jérémie</GivenName>
                  <FamilyName>Léonard</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>Jeremie.Leonard@ipcms.unistra.fr</Email>
                </Contact>
              </Author>
              <Author AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Didier</GivenName>
                  <FamilyName>Rouxel</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>Didier.Rouxel@ijl.nancy-universite.fr</Email>
                </Contact>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Pascal</GivenName>
                  <FamilyName>Hébraud</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>Pascal.Hebraud@ipcms.unistra.fr</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.11843.3f</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000121579291</OrgID>
                <OrgDivision>Institut de Physique et Chimie des Matériaux de Strasbourg &amp; Labex NIE, CNRS UMR 7504</OrgDivision>
                <OrgName>Université de Strasbourg</OrgName>
                <OrgAddress>
                  <Postcode>F-67034</Postcode>
                  <City>Strasbourg</City>
                  <Country Code="FR">France</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.29172.3f</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000121946418</OrgID>
                <OrgDivision>Institut Jean Lamour, CNRS UMR 7198</OrgDivision>
                <OrgName>Université de Lorraine</OrgName>
                <OrgAddress>
                  <Postcode>F- 54506</Postcode>
                  <City>Vandoeuvre-lès-Nancy</City>
                  <Country Code="FR">France</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Atomic Force Microscopy</Keyword>
              <Keyword>Single Molecule</Keyword>
              <Keyword>Experimental Development</Keyword>
              <Keyword>Biomedical Technology</Keyword>
              <Keyword>Innovative Technology</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Section1 ID="Sec1">
              <Heading>Acknowledgements</Heading>
              <Para>This supplement to <Emphasis Type="Italic">Journal of Nanobiotechnology</Emphasis> gathers eight tutorials corresponding to the topics of eight lectures given at the CNRS School entitled &quot;Nanophysics for Health&quot;, which took place in Mittelwhir, France, on 5 - 9 November 2012. The scope of the school was the implementation and relevance of tools and concepts from nanophysics applied to biological sciences and biomedical technologies. The onset of new, powerful methods for the investigation of biomolecular or cellular systems, as well as the development of innovative technologies for diagnosis or therapy are among the great promises of nanotechnologies in general, and of nanophysics in particular. This field of research is very dynamic and intrinsically multidisciplinary. It requires a tight collaboration between partners from different scientific horizons, which is favored by their sharing common, multidisciplinary scientific and technological skills. The goal of the school was to contribute to the building of this common scientific background and language.</Para>
              <Para>Fifty scientists (including lecturers) with diverse backgrounds in nanoscience, physics, biophysics, biology or chemistry attended the school. The program was essentially composed of keynote lectures focusing on three main topics: Nanophysics for Molecular Biology, Nanophysics for Cellular Biology, and Nanophysics for Diagnosis and Therapy. Practical sessions focusing on the use of Atomic Force Microscopy for the investigation of biological samples were also organized. In total, twelve 2-hour lectures were delivered, eight of them being published as tutorials in the present supplement of <Emphasis Type="Italic">Journal of Nanobiotechnology</Emphasis>.</Para>
              <Para>An introductory lecture (by D. Riveline) did present the manipulation of single molecules, and in particular the reasons why single molecule observation and manipulation are a unique tool to understand the dynamical properties of macromolecules. Then, a set of lectures was devoted to the structure, dynamics and dynamical assembly of proteins, including experimental developments in the firled of single molecule spectroscopy (B. Schuler), atomic force microscopy (F. Rico), and 3-dimensional imaging with electron microscopy (P. Schultz) as well as theoretical developments (K. Kruse). The last set of lectures showed that manufactured nanoparticles are very promising tools for nanomedicine, in the field of therapy (by P. Couvreur), as well as imaging and diagnosis (by F. Gazeau, L. Bonacina).</Para>
              <Para>For the school organizing committee, the guest editors: Jérémie Léonard, Didier Rouxel, Pascal Hébraud.</Para>
            </Section1>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <ArticleNote Type="Misc">
              <Heading>Competing interests</Heading>
              <SimplePara>The authors declare that they have no competing interests.</SimplePara>
            </ArticleNote>
            <Acknowledgments>
              <SimplePara>This article has been published as part of <Emphasis Type="Italic">Journal of Nanobiotechnology</Emphasis> Volume 11 Supplement 1, 2013: Nanophysics for Health. The full contents of the supplement are available online at <ExternalRef>
                  <RefSource>http://www.jnanobiotechnology.com/supplements/11/S1</RefSource>
                  <RefTarget TargetType="URL" Address="http://www.jnanobiotechnology.com/supplements/11/S1"/>
                </ExternalRef>. Publication charges for this tutorial were funded by the CNRS School &quot;Nanophysics for Health&quot;, 5 - 9 November 2012, Mittelwhir, France</SimplePara>
            </Acknowledgments>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


