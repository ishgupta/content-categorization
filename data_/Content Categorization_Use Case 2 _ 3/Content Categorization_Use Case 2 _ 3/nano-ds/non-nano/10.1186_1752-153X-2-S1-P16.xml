<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <Article OutputMedium="All" ID="BMC1752-153X-2-S1-P16">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="Abstract">
            <ArticleID>90</ArticleID>
            <ArticleDOI>10.1186/1752-153X-2-S1-P16</ArticleDOI>
            <ArticleCitationID>P16</ArticleCitationID>
            <ArticleSequenceNumber>32</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">The use of quantum chemistry in the prediction of ADME-Tox properties</ArticleTitle>
            <ArticleCategory>Poster presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2008</Year>
                <Month>03</Month>
                <Day>26</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2008</Year>
                <Month>03</Month>
                <Day>26</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Van Damme and Bultinck</CopyrightHolderName>
              <CopyrightYear>2008</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>2</VolumeIDStart>
              <VolumeIDEnd>2</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>S</GivenName>
                  <FamilyName>Van Damme</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>P</GivenName>
                  <FamilyName>Bultinck</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.5342.0</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000120697798</OrgID>
                <OrgDivision>Department of Inorganic and Physical Chemistry</OrgDivision>
                <OrgName>University of Ghent</OrgName>
                <OrgAddress>
                  <Street>Krijgslaan 281 S3</Street>
                  <Postcode>B-9000</Postcode>
                  <City>Gent</City>
                  <Country>Belgium</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Quantum Chemical</Keyword>
              <Keyword>Drug Design</Keyword>
              <Keyword>Active Molecule</Keyword>
              <Keyword>QSAR Model</Keyword>
              <Keyword>Pharmaceutical Research</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>ADME-Tox properties are very important in pharmaceutical research, determining the fate of many molecules in the drug design sequence. Knowledge of ADME-Tox properties in the earliest stages of drug design is therefore highly desirable. The aim of this investigation is to construct low throughput in silico QSAR models in which ADME-Tox properties of single compounds are predicted with high accuracy based on Quantum Chemical information [<CitationRef CitationID="CR1">1</CitationRef>].</Para>
            <Para>The possible role of quantum chemical information in chemoinformatics is discussed, with a closer look to the advantages, disadvantages and capabilities of quantum chemical descriptors in QSAR environments.</Para>
            <Para>The use of quantum chemical information is explained by a worked-out example concerning the distribution of medicinal active molecules through the blood-brain barrier [<CitationRef CitationID="CR2">2</CitationRef>].</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibChapter>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Karelson</FamilyName>
                  </BibAuthorName>
                  <Year>2004</Year>
                  <ChapterTitle xml:lang="en" Language="En">Quantum chemical descriptors in QSAR</ChapterTitle>
                  <BibEditorName>
                    <Initials>P</Initials>
                    <FamilyName>Bultinck</FamilyName>
                  </BibEditorName>
                  <BibEditorName>
                    <Initials>H</Initials>
                    <FamilyName>De Winter</FamilyName>
                  </BibEditorName>
                  <BibEditorName>
                    <Initials>W</Initials>
                    <FamilyName>Langenaeker</FamilyName>
                  </BibEditorName>
                  <BibEditorName>
                    <Initials>JP</Initials>
                    <FamilyName>Tollenaere</FamilyName>
                  </BibEditorName>
                  <Eds/>
                  <BookTitle>Computational Medicinal Chemistry for Drug Discovery</BookTitle>
                  <PublisherName>Dekker Inc.</PublisherName>
                  <PublisherLocation>NY</PublisherLocation>
                  <FirstPage>641</FirstPage>
                  <LastPage>667</LastPage>
                </BibChapter>
                <BibUnstructured>Karelson M: Quantum chemical descriptors in QSAR. Computational Medicinal Chemistry for Drug Discovery. Edited by: Bultinck P, De Winter H, Langenaeker W, Tollenaere JP. 2004, Dekker Inc., NY, 641-667.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibChapter>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Van Damme</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>W</Initials>
                    <FamilyName>Langenaeker</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Bultinck</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <ChapterTitle xml:lang="en" Language="En">Prediction of Blood-brain partitioning: a model based on ab initio calculated quantum chemical descriptors</ChapterTitle>
                  <BookTitle>Journal of Molecular graphics and modelling</BookTitle>
                  <BibComments>in press. doi:10.1016/j.jmgm.2007.11.004.</BibComments>
                </BibChapter>
                <BibUnstructured>Van Damme S, Langenaeker W, Bultinck P: Prediction of Blood-brain partitioning: a model based on ab initio calculated quantum chemical descriptors. Journal of Molecular graphics and modelling. 2007, in press. doi:10.1016/j.jmgm.2007.11.004.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


