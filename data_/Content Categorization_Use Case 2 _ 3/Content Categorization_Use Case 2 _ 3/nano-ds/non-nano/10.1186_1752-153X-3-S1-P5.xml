<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <IssueHeader>
          <EditorGroup>
            <Editor>
              <EditorName DisplayOrder="Western">
                <GivenName>Oellien</GivenName>
                <FamilyName>Frank</FamilyName>
              </EditorName>
            </Editor>
          </EditorGroup>
        </IssueHeader>
        <Article OutputMedium="All" ID="BMC1752-153X-3-S1-P5">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="Abstract">
            <ArticleID>150</ArticleID>
            <ArticleDOI>10.1186/1752-153X-3-S1-P5</ArticleDOI>
            <ArticleCitationID>P5</ArticleCitationID>
            <ArticleSequenceNumber>10</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Complexity effects in fingerprint similarity searching</ArticleTitle>
            <ArticleCategory>Poster presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Wang et al; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2009</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Y</GivenName>
                  <FamilyName>Wang</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>H</GivenName>
                  <FamilyName>Geppert</FamilyName>
                </AuthorName>
              </Author>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>J</GivenName>
                  <FamilyName>Bajorath</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.10388.32</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000122403300</OrgID>
                <OrgDivision>Department of Life Science Informatics</OrgDivision>
                <OrgName>B-IT, LIMES Program Unit Chemical Biology and Medicinal Chemistry, Rheinische Friedrich-Wilhelms-Universtität Bonn</OrgName>
                <OrgAddress>
                  <Street>Dahlmannstr. 2</Street>
                  <Postcode>D-53113</Postcode>
                  <City>Bonn</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Complex Molecule</Keyword>
              <Keyword>Similarity Metrics</Keyword>
              <Keyword>Search Calculation</Keyword>
              <Keyword>Conventional Calculation</Keyword>
              <Keyword>Chemical Database</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>Similarity searching using fingerprint representations of molecules is widely applied for mining of chemical databases [<CitationRef CitationID="CR1">1</CitationRef>]. Known active compounds are used as templates to search for novel hits using similarity measures for quantitative bit string comparison. A variety of similarity metrics are being used for this purpose including the popular Tanimoto coefficient [<CitationRef CitationID="CR1">1</CitationRef>] and the Tversky coefficients [<CitationRef CitationID="CR2">2</CitationRef>].</Para>
            <Para>Differences in molecular complexity and size are known to bias the evaluation of fingerprint similarity [<CitationRef CitationID="CR3">3</CitationRef>]. Complex molecules tend to produce fingerprints with higher bit density than simpler ones, which often leads to artificially high similarity values in search calculations. For example, we have thoroughly analyzed similarity value distributions and demonstrated that apparent asymmetry in Tversky similarity search calculations is a direct consequence of differences in fingerprint bit densities [<CitationRef CitationID="CR4">4</CitationRef>].</Para>
            <Para>There are in principle two approaches to balance complexity effects; either by designing fingerprints that have constant bit density, regardless of the nature of test molecules, or, alternatively, by introducing similarity metrics that equally weight bit positions that are set on or off. We have shown that a size-independent fingerprint with constant bit density does not produce asymmetrical search results [<CitationRef CitationID="CR4">4</CitationRef>]. In addition, a novel similarity metric has been developed, which not only balances complexity effects, but also results in further improved search performance compared to conventional calculations on Tanimoto similarity [<CitationRef CitationID="CR5">5</CitationRef>]. However, highly complex molecules are generally much less suitable as reference compounds for fingerprint searching than active compounds having complexity comparable to the screening database [<CitationRef CitationID="CR5">5</CitationRef>]. Random deletion of bits that are set on in complex templates has been shown to increase compound recall, despite the associated loss in chemical information content [<CitationRef CitationID="CR6">6</CitationRef>]. Taking relative chemical complexity of reference and database compounds into account makes it possible to increase the success rates of fingerprint similarity searching.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Willett</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1998</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Chem Inf Comput Sci</JournalTitle>
                  <VolumeID>38</VolumeID>
                  <IssueID>6</IssueID>
                  <FirstPage>983</FirstPage>
                  <LastPage>96</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK1cXks1ynu74%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ci9800211</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Willett P, et al: J Chem Inf Comput Sci. 1998, 38 (6): 983-96.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>X</Initials>
                    <FamilyName>Chen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>F</Initials>
                    <FamilyName>Brown</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Chem Med Chem</JournalTitle>
                  <VolumeID>2</VolumeID>
                  <IssueID>2</IssueID>
                  <FirstPage>180</FirstPage>
                  <LastPage>2</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2sXjsVWjtL4%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1002/cmdc.200600161</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Chen X, Brown F: Chem Med Chem. 2007, 2 (2): 180-2.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Flower</FamilyName>
                  </BibAuthorName>
                  <Year>1998</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Chem Comput Sci</JournalTitle>
                  <VolumeID>38</VolumeID>
                  <IssueID>3</IssueID>
                  <FirstPage>379</FirstPage>
                  <LastPage>86</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK1cXislShsbg%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ci970437z</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Flower D: J Chem Comput Sci. 1998, 38 (3): 379-86.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Wang</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2007</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Chem Med Chem</JournalTitle>
                  <VolumeID>2</VolumeID>
                  <IssueID>7</IssueID>
                  <FirstPage>1037</FirstPage>
                  <LastPage>42</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2sXotVyktbs%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1002/cmdc.200700050</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Wang Y, et al: Chem Med Chem. 2007, 2 (7): 1037-42.</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <CitationNumber>5.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Wang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Bajorath</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Chem Inf Model</JournalTitle>
                  <VolumeID>48</VolumeID>
                  <IssueID>1</IssueID>
                  <FirstPage>75</FirstPage>
                  <LastPage>84</LastPage>
                  <BibArticleDOI>10.1021/ci700314x</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2sXhsVehu7vE</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ci700314x</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Wang Y, Bajorath J: J Chem Inf Model. 2008, 48 (1): 75-84. 10.1021/ci700314x.</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <CitationNumber>6.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Wang</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2008</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Chem Biol Drug Design</JournalTitle>
                  <VolumeID>71</VolumeID>
                  <IssueID>6</IssueID>
                  <FirstPage>511</FirstPage>
                  <LastPage>7</LastPage>
                  <BibArticleDOI>10.1111/j.1747-0285.2008.00664.x</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD1cXnt1elurk%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1111/j.1747-0285.2008.00664.x</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Wang Y, et al: Chem Biol Drug Design. 2008, 71 (6): 511-7. 10.1111/j.1747-0285.2008.00664.x.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


