<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-011-1099-x">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="News">
            <ArticleID>1099</ArticleID>
            <ArticleDOI>10.1007/s13238-011-1099-x</ArticleDOI>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" Language="En">Ageing in worms: N-acylethanolamines take control</ArticleTitle>
            <ArticleCategory>News and Views</ArticleCategory>
            <ArticleFirstPage>689</ArticleFirstPage>
            <ArticleLastPage>690</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2011</Year>
                <Month>10</Month>
                <Day>5</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2011</Year>
                <Month>10</Month>
                <Day>6</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Higher Education Press and Springer-Verlag Berlin Heidelberg</CopyrightHolderName>
              <CopyrightYear>2011</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="Regular">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Run</GivenName>
                  <FamilyName>Shen</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>rshen@salk.edu</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.250671.7</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000106627144</OrgID>
                <OrgName>The Salk Institute for Biological Studies</OrgName>
                <OrgAddress>
                  <Street>10010 North Torrey Pines Road</Street>
                  <City>La Jolla</City>
                  <State>CA</State>
                  <Postcode>92037</Postcode>
                  <Country Code="US">USA</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
          </ArticleHeader>
          <Body Type="PDF">
            <page id="pg01">Protein Cell 2011, 2(9): 689–690 DOI 10.1007/s13238-011-1099-x Protein &amp; Cell NEWS AND VIEWS Ageing in worms: N-acylethanolamines take control Run Shen ✉ The Salk Institute for Biological Studies, 10010 North Torrey Pines Road, La Jolla, CA 92037, USA ✉ Correspondence: rshen@salk.edu Studies of dietary restriction’s effect on lifespan extension in C. elegans and other model organisms have greatly expanded our knowledge about genetic network underlying stress response, ageing, and ageing related diseases. Among key molecules and signaling pathways implicated are Foxo (daf-16) and Foxa (pha-4) transcriptional factors and the nutrient sensing insulin/TOR pathway (Wolkow et al., 2000; Panowski et al., 2007; Hansen et al., 2008). However, details about how diet-derived metabolic signals connect nutrient availability to ageing are largely unknown. Recently, a group led by Mathew Gill discovered that lipid derived N- acylethanolamines (NAEs) respond to dietary restriction and are both necessary and sufﬁcient to control lifespan in worms (Lucanic et al., 2011). This ﬁnding is as much revealing as it is surprising, because the endocannabinoid system, to which NAEs belong, is believed to operate only in higher organisms (McPartland et al., 2006). It also underscores the power of model organisms such as the lowly earth worm in dissecting the molecular mechanisms controlling ageing. NAEs are a type of compounds formed by linking certain acyl groups to the nitrogen of ethanolamine. Bioactive N- acylethanolamines bind and activate the cannabinoid recep- tor 1 (CB1), which also binds and is activated by tetrahy- drocannabinol (THC), the major psychotropic component in marijuana. The ﬁrst two of the endogenous CB1 ligands puriﬁed are N-arachidonoyl ethanolamine (anandamide) and 2-arachidonoyl glycerol (2-AG) (Di Marzo and Matias, 2005). The endocannabinoid system is essential for controlling food intake and maintaining energy balance in mammals, but whether it plays any direct role in dietary restriction mediated lifespan extension is not known until this current study. To ﬁnd out more about how dietary signals control ageing, Gill and colleagues set out to ask if C. elegans has an active endocannabinoid system in play. They identiﬁed a set of NAEs, including eicosapentaenoyl ethanolamide (EPEA) and arachidonoyl ethanolamide (AEA) in C. elegans. In mammals, NAE levels are controlled by N-acyl-phosphatidylethanola- mine-speciﬁc phospholipase D (NAPE-PLD), which controls the last step of biosynthesis of NAEs, and fatty acid amide hydrolase (FAAH), which inactivates NAE molecules via hydrolysis. By manipulating FAAH expression in worms, Gill and colleagues could effectively detect changes in NAE levels, suggesting that a conserved pathway regulating NAEs exists in worms. Nutrient availability directly controls animal growth and reproductive development. To examine a possible role of NAEs in this context, Gill and colleagues altered the levels of endogenous and exogenous NAEs and found that NAEs regulate reproductive growth and dauer formation. This ﬁnding begs the questions of how endogenous NAE levels are regulated, and if they play a role in lifespan regulation. Interestingly, they found that the level of endogenous NAEs decreases in response to dietary restriction but recovers when the animals are refed. Then, the question is whether decreased NAE levels may partly mimic dietary restriction and extend lifespan. And they found reducing endogenous NAEs by faah-1 overexpression extended lifespan in the presence of abundant food but not under optimal dietary restriction, suggesting this faah-1 overexpression meditated effect shares some common mechanism with dietary restric- tion. To further deﬁne the function of NAEs in the genetic network underlining lifespan extension by dietary restriction, Gill and colleagues interrogated the interactions among faah- 1, daf-16, and pha-4. daf-16 has been extensively studied and is known to upregulate gene expression favoring lifespan extension including superoxide dismutases (SODs) and autophagy (Hansen et al., 2008). Transcription of daf-16 is negatively regulated by insulin/Akt signaling. Lifespan exten- sion mediated by faah-1 overexpression is not dependent on daf-16 function, whereas Pha-4, a transcriptional factor promoting longevity in worms, is required. Since reducing NAE levels was shown to be sufﬁcient to extend lifespan, it would be interesting to ask if elevating NAE levels would © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011 689</page>
            <page id="pg02">Protein &amp; Cell Run Shen suppress stress resistance and lifespan extension. To their prepared minds, exogenous NAE treatment extended not only the lifespan of wild type control, but also daf-2 mutants. The exogenous NAEs’ effect on daf-2 mutants is consistent with previous studies showing that insulin/Foxo function is dispensable for NAE signaling. Also, consistent is that elevating NAE levels did not decrease lifespan under high food in-take conditions since endogenous NAE response in this case had much less potential to be upregulated compared with dietary restriction. Up until this ﬁnding by Gill and colleagues, the endocanna- binoid system is not known to exist in C. elegans. In mammals, endocannabinoids are known to regulate energy balance by modulating the hypothalamic circuitry controlling food intake and energy expenditure. Several hypothalamic and orexigenic mediators may be the targets, including corticotrophin-releas- ing hormone (CRH), melanin-concentrating hormone and pre- pro-orexin. In peripheral tissues, endocannabinoids and receptors are also present, particularly in the adipose tissue, to regulate adipogenesis, glucose uptake, lipogenesis and fat storage (Di Marzo and Matias, 2005). Yet, little is known about how this system regulates stress resistance and ageing. In Gill and colleague’s study, genetic epistasis analysis places NAEs downstream of daf-2/daf-16, but upstream of pha-4, which has been proposed to regulate lifespan through upregulating expression of sod-2 and sod-4 (Panowski et al., 2007). The decrease of NAEs associated with dietary restriction may correlate with high levels of sods, which helps to explain the role of NAEs in lifespan regulation in worms. The endocanna- binoid system in human has been implicated in the pathophy- siology of ischemic stroke and neurodegenerative diseases of immune origin, including multiple sclerosis (Howlett et al., 2006). So, the study by Gill and colleagues may enhance our understanding of these diseases in humans, given the connection it begins to establish between the endocannabi- noid system and ageing. On the other hand, studies in mammals may also provide insight to elucidate the mechan- ism of endocannabinoid signaling in worms. Peroxisome proliferator-activated receptors (PPARs) are a family of nuclear receptors that, upon activating ligand-binding, act as transcription factors to regulate gene expression for lipid metabolism. An increasing body of evidence suggests that the ability to bind PPARs is an attribute common to many endocannabinoids and is of physiological relevance (Fu et al., 2003). Since it appears that worms lack clear orthologues of cannabinoid receptors, PPARs may take on the task of relaying endocannabinoid signals. Additionally, this study establishes a powerful genetic system for future research on the roles of endocannabinoid system. The transparent body and low redundancy in gene functions, coupled with the ease of applying the power of RNAi screen, make C. elegans an advantageous system for elucidating the mechanism of endocannabinoid system and generating potential targets for drug design. Even better, since automated high-content live animal drug screening has been developed for worms, now compound libraries can be applied directly to target speciﬁc reporters or phenotypes (Gosai et al., 2010). REFERENCES Di Marzo, V., and Matias, I. (2005). Endocannabinoid control of food intake and energy balance. Nat Neurosci 8, 585–589. Fu, J., Gaetani, S., Oveisi, F., Lo Verme, J., Serrano, A., Rodríguez De Fonseca, F., Rosengarth, A., Luecke, H., Di Giacomo, B., Tarzia, G., et al. (2003). Oleylethanolamide regulates feeding and body weight through activation of the nuclear receptor PPAR- alpha. Nature 425, 90–93. Gosai, S.J., Kwak, J.H., Luke, C.J., Long, O.S., King, D.E., Kovatch, K.J., Johnston, P.A., Shun, T.Y., Lazo, J.S., Perlmutter, D.H., et al. (2010). Automated high-content live animal drug screening using C. elegans expressing the aggregation prone serpin α1-antitrypsin Z. PLoS ONE 5, e15460. Hansen, M., Chandra, A., Mitic, L.L., Onken, B., Driscoll, M., and Kenyon, C. (2008). A role for autophagy in the extension of lifespan by dietary restriction in C. elegans. PLoS Genet 4, e24. Howlett, A.C., Mukhopadhyay, S., and Norford, D.C. (2006). Endocannabinoids and reactive nitrogen and oxygen species in neuropathologies. J Neuroimmune Pharmacol 1, 305–316. Lucanic, M., Held, J.M., Vantipalli, M.C., Klang, I.M., Graham, J.B., Gibson, B.W., Lithgow, G.J., and Gill, M.S. (2011). N-acylethano- lamine signalling mediates the effect of diet on lifespan in Caenorhabditis elegans. Nature 473, 226–229. McPartland, J.M., Matias, I., Di Marzo, V., and Glass, M. (2006). Evolutionary origins of the endocannabinoid system. Gene 370, 64–74. Panowski, S.H., Wolff, S., Aguilaniu, H., Durieux, J., and Dillin, A. (2007). PHA-4/Foxa mediates diet-restriction-induced longevity of C. elegans. Nature 447, 550–555. Wolkow, C.A., Kimura, K.D., Lee, M.S., and Ruvkun, G. (2000). Regulation of C. elegans life-span by insulinlike signaling in the nervous system. Science 290, 147–150. 690 © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011</page>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>V.</Initials>
                    <FamilyName>Marzo</FamilyName>
                    <Particle>Di</Particle>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I.</Initials>
                    <FamilyName>Matias</FamilyName>
                  </BibAuthorName>
                  <Year>2005</Year>
                  <ArticleTitle xml:lang="en" Language="En">Endocannabinoid control of food intake and energy balance</ArticleTitle>
                  <JournalTitle>Nat Neurosci</JournalTitle>
                  <VolumeID>8</VolumeID>
                  <FirstPage>585</FirstPage>
                  <LastPage>589</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nn1457</Handle>
                  </Occurrence>
                  <BibComments>15856067, 10.1038/nn1457</BibComments>
                </BibArticle>
                <BibUnstructured>Di Marzo, V., and Matias, I. (2005). Endocannabinoid control of food intake and energy balance. Nat Neurosci 8, 585–589.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J.</Initials>
                    <FamilyName>Fu</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Gaetani</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>F.</Initials>
                    <FamilyName>Oveisi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.</Initials>
                    <FamilyName>Verme</FamilyName>
                    <Particle>Lo</Particle>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A.</Initials>
                    <FamilyName>Serrano</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>F.</Initials>
                    <FamilyName>Rodríguez De Fonseca</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A.</Initials>
                    <FamilyName>Rosengarth</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>H.</Initials>
                    <FamilyName>Luecke</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>B.</Initials>
                    <FamilyName>Giacomo</FamilyName>
                    <Particle>Di</Particle>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>G.</Initials>
                    <FamilyName>Tarzia</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2003</Year>
                  <ArticleTitle xml:lang="en" Language="En">Oleylethanolamide regulates feeding and body weight through activation of the nuclear receptor PPAR-alpha</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>425</VolumeID>
                  <FirstPage>90</FirstPage>
                  <LastPage>93</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature01921</Handle>
                  </Occurrence>
                  <BibComments>12955147, 10.1038/nature01921, 1:CAS:528:DC%2BD3sXmvFansrY%3D</BibComments>
                </BibArticle>
                <BibUnstructured>Fu, J., Gaetani, S., Oveisi, F., Lo Verme, J., Serrano, A., Rodríguez De Fonseca, F., Rosengarth, A., Luecke, H., Di Giacomo, B., Tarzia, G., <Emphasis Type="Italic">et al.</Emphasis> (2003). Oleylethanolamide regulates feeding and body weight through activation of the nuclear receptor PPAR-alpha. Nature 425, 90–93.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.J.</Initials>
                    <FamilyName>Gosai</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.H.</Initials>
                    <FamilyName>Kwak</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>C.J.</Initials>
                    <FamilyName>Luke</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>O.S.</Initials>
                    <FamilyName>Long</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D.E.</Initials>
                    <FamilyName>King</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>K.J.</Initials>
                    <FamilyName>Kovatch</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P.A.</Initials>
                    <FamilyName>Johnston</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T.Y.</Initials>
                    <FamilyName>Shun</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.S.</Initials>
                    <FamilyName>Lazo</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D.H.</Initials>
                    <FamilyName>Perlmutter</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2010</Year>
                  <ArticleTitle xml:lang="en" Language="En">Automated high-content live animal drug screening using <Emphasis Type="Italic">C. elegans</Emphasis> expressing the aggregation prone serpin α1-antitrypsin Z</ArticleTitle>
                  <JournalTitle>PLoS ONE</JournalTitle>
                  <VolumeID>5</VolumeID>
                  <FirstPage>e15460</FirstPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1371/journal.pone.0015460</Handle>
                  </Occurrence>
                  <BibComments>21103396, 10.1371/journal.pone.0015460</BibComments>
                </BibArticle>
                <BibUnstructured>Gosai, S.J., Kwak, J.H., Luke, C.J., Long, O.S., King, D.E., Kovatch, K.J., Johnston, P.A., Shun, T.Y., Lazo, J.S., Perlmutter, D.H., <Emphasis Type="Italic">et al.</Emphasis> (2010). Automated high-content live animal drug screening using <Emphasis Type="Italic">C. elegans</Emphasis> expressing the aggregation prone serpin α1-antitrypsin Z. PLoS ONE 5, e15460.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>M.</Initials>
                    <FamilyName>Hansen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A.</Initials>
                    <FamilyName>Chandra</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>L.L.</Initials>
                    <FamilyName>Mitic</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>B.</Initials>
                    <FamilyName>Onken</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.</Initials>
                    <FamilyName>Driscoll</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>C.</Initials>
                    <FamilyName>Kenyon</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <ArticleTitle xml:lang="en" Language="En">A role for autophagy in the extension of lifespan by dietary restriction in <Emphasis Type="Italic">C. elegans</Emphasis></ArticleTitle>
                  <JournalTitle>PLoS Genet</JournalTitle>
                  <VolumeID>4</VolumeID>
                  <FirstPage>e24</FirstPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1371/journal.pgen.0040024</Handle>
                  </Occurrence>
                  <BibComments>18282106, 10.1371/journal.pgen.0040024</BibComments>
                </BibArticle>
                <BibUnstructured>Hansen, M., Chandra, A., Mitic, L.L., Onken, B., Driscoll, M., and Kenyon, C. (2008). A role for autophagy in the extension of lifespan by dietary restriction in <Emphasis Type="Italic">C. elegans</Emphasis>. PLoS Genet 4, e24.</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>A.C.</Initials>
                    <FamilyName>Howlett</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Mukhopadhyay</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D.C.</Initials>
                    <FamilyName>Norford</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <ArticleTitle xml:lang="en" Language="En">Endocannabinoids and reactive nitrogen and oxygen species in neuropathologies</ArticleTitle>
                  <JournalTitle>J Neuroimmune Pharmacol</JournalTitle>
                  <VolumeID>1</VolumeID>
                  <FirstPage>305</FirstPage>
                  <LastPage>316</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s11481-006-9022-6</Handle>
                  </Occurrence>
                  <BibComments>18040807, 10.1007/s11481-006-9022-6</BibComments>
                </BibArticle>
                <BibUnstructured>Howlett, A.C., Mukhopadhyay, S., and Norford, D.C. (2006). Endocannabinoids and reactive nitrogen and oxygen species in neuropathologies. J Neuroimmune Pharmacol 1, 305–316.</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>M.</Initials>
                    <FamilyName>Lucanic</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.M.</Initials>
                    <FamilyName>Held</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.C.</Initials>
                    <FamilyName>Vantipalli</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I.M.</Initials>
                    <FamilyName>Klang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.B.</Initials>
                    <FamilyName>Graham</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>B.W.</Initials>
                    <FamilyName>Gibson</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>G.J.</Initials>
                    <FamilyName>Lithgow</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.S.</Initials>
                    <FamilyName>Gill</FamilyName>
                  </BibAuthorName>
                  <Year>2011</Year>
                  <ArticleTitle xml:lang="en" Language="En">N-acylethanolamine signalling mediates the effect of diet on lifespan in Caenorhabditis elegans</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>473</VolumeID>
                  <FirstPage>226</FirstPage>
                  <LastPage>229</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature10007</Handle>
                  </Occurrence>
                  <BibComments>21562563, 10.1038/nature10007, 1:CAS:528:DC%2BC3MXlvFGmtLw%3D</BibComments>
                </BibArticle>
                <BibUnstructured>Lucanic, M., Held, J.M., Vantipalli, M.C., Klang, I.M., Graham, J.B., Gibson, B.W., Lithgow, G.J., and Gill, M.S. (2011). N-acylethanolamine signalling mediates the effect of diet on lifespan in Caenorhabditis elegans. Nature 473, 226–229.</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J.M.</Initials>
                    <FamilyName>McPartland</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I.</Initials>
                    <FamilyName>Matias</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>V.</Initials>
                    <FamilyName>Marzo</FamilyName>
                    <Particle>Di</Particle>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.</Initials>
                    <FamilyName>Glass</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <ArticleTitle xml:lang="en" Language="En">Evolutionary origins of the endocannabinoid system</ArticleTitle>
                  <JournalTitle>Gene</JournalTitle>
                  <VolumeID>370</VolumeID>
                  <FirstPage>64</FirstPage>
                  <LastPage>74</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.gene.2005.11.004</Handle>
                  </Occurrence>
                  <BibComments>16434153, 10.1016/j.gene.2005.11.004, 1:CAS:528:DC%2BD28Xit1Wqt7k%3D</BibComments>
                </BibArticle>
                <BibUnstructured>McPartland, J.M., Matias, I., Di Marzo, V., and Glass, M. (2006). Evolutionary origins of the endocannabinoid system. Gene 370, 64–74.</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.H.</Initials>
                    <FamilyName>Panowski</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Wolff</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>H.</Initials>
                    <FamilyName>Aguilaniu</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J.</Initials>
                    <FamilyName>Durieux</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A.</Initials>
                    <FamilyName>Dillin</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <ArticleTitle xml:lang="en" Language="En">PHA-4/Foxa mediates diet-restriction-induced longevity of <Emphasis Type="Italic">C. elegans</Emphasis></ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>447</VolumeID>
                  <FirstPage>550</FirstPage>
                  <LastPage>555</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature05837</Handle>
                  </Occurrence>
                  <BibComments>17476212, 10.1038/nature05837, 1:CAS:528:DC%2BD2sXlvVyktb4%3D</BibComments>
                </BibArticle>
                <BibUnstructured>Panowski, S.H., Wolff, S., Aguilaniu, H., Durieux, J., and Dillin, A. (2007). PHA-4/Foxa mediates diet-restriction-induced longevity of <Emphasis Type="Italic">C. elegans</Emphasis>. Nature 447, 550–555.</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>C.A.</Initials>
                    <FamilyName>Wolkow</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>K.D.</Initials>
                    <FamilyName>Kimura</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.S.</Initials>
                    <FamilyName>Lee</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>G.</Initials>
                    <FamilyName>Ruvkun</FamilyName>
                  </BibAuthorName>
                  <Year>2000</Year>
                  <ArticleTitle xml:lang="en" Language="En">Regulation of <Emphasis Type="Italic">C. elegans</Emphasis> life-span by insulinlike signaling in the nervous system</ArticleTitle>
                  <JournalTitle>Science</JournalTitle>
                  <VolumeID>290</VolumeID>
                  <FirstPage>147</FirstPage>
                  <LastPage>150</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1126/science.290.5489.147</Handle>
                  </Occurrence>
                  <BibComments>11021802, 10.1126/science.290.5489.147, 1:CAS:528:DC%2BD3cXnt1Chtrw%3D</BibComments>
                </BibArticle>
                <BibUnstructured>Wolkow, C.A., Kimura, K.D., Lee, M.S., and Ruvkun, G. (2000). Regulation of <Emphasis Type="Italic">C. elegans</Emphasis> life-span by insulinlike signaling in the nervous system. Science 290, 147–150.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


