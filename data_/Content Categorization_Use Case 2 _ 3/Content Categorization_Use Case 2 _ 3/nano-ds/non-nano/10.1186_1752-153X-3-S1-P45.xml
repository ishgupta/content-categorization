<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <IssueHeader>
          <EditorGroup>
            <Editor>
              <EditorName DisplayOrder="Western">
                <GivenName>Oellien</GivenName>
                <FamilyName>Frank</FamilyName>
              </EditorName>
            </Editor>
          </EditorGroup>
        </IssueHeader>
        <Article OutputMedium="All" ID="BMC1752-153X-3-S1-P45">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="Abstract">
            <ArticleID>209</ArticleID>
            <ArticleDOI>10.1186/1752-153X-3-S1-P45</ArticleDOI>
            <ArticleCitationID>P45</ArticleCitationID>
            <ArticleSequenceNumber>69</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Glide XP fragment docking and structurebased pharmacophores</ArticleTitle>
            <ArticleCategory>Poster presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Sherman and Friesner; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2009</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>W</GivenName>
                  <FamilyName>Sherman</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>R</GivenName>
                  <FamilyName>Friesner</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.421925.9</OrgID>
                <OrgName>Schrödinger Inc</OrgName>
                <OrgAddress>
                  <Street>120 West 45th Street, 29th Floor</Street>
                  <City>New York</City>
                  <State>NY</State>
                  <Postcode>10036</Postcode>
                  <Country>USA</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.21729.3f</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000419368729</OrgID>
                <OrgDivision>Department of Chemistry</OrgDivision>
                <OrgName>Columbia University</OrgName>
                <OrgAddress>
                  <Street>3000 Broadway, Mail Code 3110</Street>
                  <City>New York</City>
                  <State>NY</State>
                  <Postcode>1002</Postcode>
                  <Country>USA</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Validation Study</Keyword>
              <Keyword>Enrichment Factor</Keyword>
              <Keyword>Dock</Keyword>
              <Keyword>Computational Approach</Keyword>
              <Keyword>Drug Design</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>In recent years, fragment-based drug design has become increasingly popular. Common computational approaches include building fragments up sequentially, or linking disparate fragments. However, the former approach can restrict the exploration of chemical space and may produce ligands that are not sufficiently drug-like, whereas the latter approach may result in difficulties when linking together the individual fragments.</Para>
            <Para>Here, we describe a third approach that avoids these problems with the use of fragment-derived pharmacophore hypotheses. In our computational workflow a pharmacophore hypothesis is created using fragments docked and ranked by Glide XP, and virtual databases are screened against this hypothesis using Phase [<CitationRef CitationID="CR1">1</CitationRef>]. In an initial validation study on P38 Map Kinase, known active compounds were successfully retrieved and good enrichment factors were obtained.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>SL</Initials>
                    <FamilyName>Dixon</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Comput Aided Mol Des</JournalTitle>
                  <VolumeID>20</VolumeID>
                  <FirstPage>647</FirstPage>
                  <LastPage>671</LastPage>
                  <BibArticleDOI>10.1007/s10822-006-9087-6</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD28XhtlCksbvE</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s10822-006-9087-6</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Dixon SL: J Comput Aided Mol Des. 2006, 20: 647-671. 10.1007/s10822-006-9087-6.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


