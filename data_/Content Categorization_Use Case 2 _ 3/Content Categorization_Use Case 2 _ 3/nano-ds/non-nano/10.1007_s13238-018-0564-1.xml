<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <JournalOnlineFirst>
      <Article OutputMedium="All" ID="s13238-018-0564-1">
        <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
          <ArticleID>564</ArticleID>
          <ArticleDOI>10.1007/s13238-018-0564-1</ArticleDOI>
          <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Mini-biography for Mr. Xitao Cai: the pioneer botanist of the plant kingdom</ArticleTitle>
          <ArticleCategory>Recollection</ArticleCategory>
          <ArticleFirstPage>1</ArticleFirstPage>
          <ArticleLastPage>4</ArticleLastPage>
          <ArticleHistory>
            <RegistrationDate>
              <Year>2018</Year>
              <Month>6</Month>
              <Day>27</Day>
            </RegistrationDate>
            <OnlineDate>
              <Year>2018</Year>
              <Month>7</Month>
              <Day>6</Day>
            </OnlineDate>
          </ArticleHistory>
          <ArticleCopyright>
            <CopyrightHolderName>The Author(s)</CopyrightHolderName>
            <CopyrightYear>2018</CopyrightYear>
            <License Version="4.0" Type="OpenAccess" SubType="CC BY">
              <SimplePara><Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution 4.0 International License (<ExternalRef>
                  <RefSource>http://creativecommons.org/licenses/by/4.0/</RefSource>
                  <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/4.0/"/>
                </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided you give appropriate credit to the original author(s) and the source, provide a link to the Creative Commons license, and indicate if changes were made.</SimplePara>
            </License>
          </ArticleCopyright>
          <ArticleGrants Type="OpenChoice">
            <MetadataGrant Grant="OpenAccess"/>
            <AbstractGrant Grant="OpenAccess"/>
            <BodyPDFGrant Grant="OpenAccess"/>
            <BodyHTMLGrant Grant="OpenAccess"/>
            <BibliographyGrant Grant="OpenAccess"/>
            <ESMGrant Grant="OpenAccess"/>
          </ArticleGrants>
        </ArticleInfo>
        <ArticleHeader>
          <PageHeaders>
            <RunningTitle>Mr. Xitao Cai: the pioneer botanist of the plant kingdom</RunningTitle>
            <RunningAuthor>Gaibian Wang et al.</RunningAuthor>
          </PageHeaders>
          <AuthorGroup>
            <Author ID="Au1" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Gaibian</GivenName>
                <FamilyName>Wang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au2" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Quanxing</GivenName>
                <FamilyName>Zhang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au3" CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Yongping</GivenName>
                <FamilyName>Yang</FamilyName>
              </AuthorName>
              <Contact>
                <Email>yangyp@mail.kib.ac.cn</Email>
              </Contact>
            </Author>
            <Affiliation ID="Aff1">
              <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
              <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
              <OrgDivision>Kunming Institute of Botany</OrgDivision>
              <OrgName>Chinese Academy of Sciences</OrgName>
              <OrgAddress>
                <City>Kunming</City>
                <Postcode>650201</Postcode>
                <Country Code="CN">China</Country>
              </OrgAddress>
            </Affiliation>
          </AuthorGroup>
        </ArticleHeader>
        <Body Type="XML">
          <Para ID="Par1">On March 9th, 1981, Mr. Xitao Cai (Hse-Tao Tsai) (Fig. <InternalRef RefID="Fig1">1</InternalRef>) one of the most famous pioneer botanists in China, passed away at his age of 70 in Kunming. It has been 37 years since he left us. Mr. Cai founded Yunnan Institute of Agricultural and Forestry Botany in 1938, the predecessor of Kunming Institute of Botany, Chinese Academy of Sciences. At the celebration of the 80th anniversary for the institute, we deeply cherish Mr. Cai, the founder of the institute, who has dedicated his entire life to the development of botany science in China.<Figure ID="Fig1" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 1</CaptionNumber>
                <CaptionContent>
                  <SimplePara>Mr. Xitao Cai (Hse-Tao TSAI, April 10th, 1911–March 9th, 1981)</SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO1">
                <ImageObject Width="879" Type="Halftone" Resolution="300" Rendition="HTML" Height="1160" Format="JPEG" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-018-0564-1/MediaObjects/13238_2018_564_Fig1_HTML.jpg"/>
              </MediaObject>
            </Figure>
</Para>
          <Para ID="Par2">Mr. Xitao Cai was born in Zhejiang Province on April 10th, 1911. He was admitted into the Department of Physics in Kwang Hua University in Shanghai in 1929. During his studies in Shanghai, he frequently visited his brother-in-law, Mr. Wangdao Chen and was influenced by many revolutionary predecessors and literary giants, such as Mr. Qiubai Qu, Mr. Da Li, Mr. Zhengnong Xia, Mr. Lu Xun, Mr. Xuefeng Feng and Mr. Yuzhi Hu. In September 1930, recommended by Mr. Wangdao Chen, Mr. Cai became a trainee in the Fan Memorial Institute of Biology. During this time, Professor Xiansu Hu (Hsen-Hsu Hu), the Director of the institute, often sent him to collect plant specimens near Peking. The next year, Mr. Cai (Fig. <InternalRef RefID="Fig2">2</InternalRef>) published a research paper <Emphasis Type="Italic">An Study on Lamiaceae in Sichuan Province</Emphasis> co-author with Prof. Hu (Xun et al., <CitationRef CitationID="CR5">1993</CitationRef>). In February 1932, Mr. Cai was dispatched to Yunnan on a botanical expedition organized by the Fan Memorial Institute of Biology (Fig. <InternalRef RefID="Fig3">3</InternalRef>). He traveled through Yunnan, risked his life and conducted field survey and plant collection in the remote mountainous areas. During this expedition, he collected more than 21,000 specimens including 427 new species, which unveiled the mask of Yunnan Province as a “Plant Kingdom” and contributed significantly to the development of the plant science in Yunnan. Later on, Mr. Cai translated the book <Emphasis Type="Italic">Origin of Cultivated Plants</Emphasis> written by de Candolle into Chinese, together with Mr. Dejun Yu (Te-Tsun Yü) (Jiang, <CitationRef CitationID="CR1">2017</CitationRef>), and published a series of research papers on the family Leguminosaceae, Rosaceae and the genus <Emphasis Type="Italic">Amorphophallus</Emphasis> plants. His early works enabled the Chinese botanical community to have better understandings about the origins of crops and served as invaluable references for the following studies till now. In addition, Mr. Cai was inspired by the customs of the ethnic minorities and wrote several essays which has been described as “very impressive” by famous Chinese writer, Mr. Lu Xun.<Figure ID="Fig2" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 2</CaptionNumber>
                <CaptionContent>
                  <SimplePara>Mr. Xitao Cai (1933)</SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO2">
                <ImageObject Width="531" Type="Halftone" Resolution="300" Rendition="HTML" Height="850" Format="JPEG" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-018-0564-1/MediaObjects/13238_2018_564_Fig2_HTML.jpg"/>
              </MediaObject>
            </Figure> <Figure ID="Fig3" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 3</CaptionNumber>
                <CaptionContent>
                  <SimplePara>Mr. Xitao Cai (left) on the field trip to Nujiang Prefecture of Yunnan (1932)</SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO3">
                <ImageObject Width="945" Type="Halftone" Resolution="300" Rendition="HTML" Height="1225" Format="JPEG" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-018-0564-1/MediaObjects/13238_2018_564_Fig3_HTML.jpg"/>
              </MediaObject>
            </Figure></Para>
          <Para ID="Par3">In 1937, the War of Resistance against Japan broke out. Many institutions in Peking started to move to Southwest China. The Fan Memorial Institute of Biology planned to establish a botanical research base in Yunnan. Mr. Cai moved to Yunnan with his family. In July 1938, Yunnan Institute of Agricultural and Forestry Botany, the predecessor of Kunming Institute of Botany, was established in the Heilongtan Park, Kunming City. Mr. Cai was appointed as the park manager as well. In the spring of 1940, the institute bought a land site near the Heilongtan Park to construct the office building. And the construction of the office building was completed in 1941. The motto “Explore every mountain and river, name every grass and tree” was carved in stone and embedded into the wall. Mr. Zhengyi Wu (Cheng-Yhi Wu), who once took students from the Southwest Associated University for internships in Yunnan Institute of Agricultural and Forestry Botany, described the institute as “the highest botanical institution in China at that time” and “a research center for plant taxonomy in old China” in his commemorative essay to Mr. Cai (Wu, <CitationRef CitationID="CR4">1991</CitationRef>).</Para>
          <Para ID="Par4">In 1945, after the victory of the Resistance against Japanese Aggression War, many institutions moved back from Yunnan Province. With the outbreak of the civil war, prices soared and it became very difficult to live. In order to maintain the life of the employees and protect more than 100,000 plant specimens, Mr. Cai organized the employees to grow vegetables, flowers and tobacco to save themselves. He opened a parrot shop to sell cutting flowers, ornamental plants and pets, and struggled till the liberation of Kunming. For the institute, Mr. Cai was not only the founder and the actual manager, but also the one who persisted from the beginning to the end. In the essay of <Emphasis Type="Italic">Recalling Mr. Xitao</Emphasis>’<Emphasis Type="Italic">s life</Emphasis> by Prof. Fenghuai Chen (Feng-Hwai Chen), who worked together with Mr. Cai during 1930s–1940s. It is said that, “Mr. Cai made the most important contributions to the establishment of Yunnan Institute of Agricultural and Forestry Botany.”</Para>
          <Para ID="Par5">In April 1950, Yunnan Institute of Agricultural and Forestry Botany was merged and re-named as Kunming Station of the Institute of Plant Taxonomy, Chinese Academy of Sciences. Mr. Cai was appointed as the director of Kunming Station. With the special care from Premier Zhou Enlai (Fig. <InternalRef RefID="Fig4">4</InternalRef>), Mr. Cai focused on the development and construction. The campus of Kunming Station was expanded, three modern buildings used as administration office and research laboratories were constructed. In research fields, Mr. Cai established the first chemistry laboratory to study medicinal and aromatic plants (the predecessor of the State Key Laboratory of Phytochemistry and Sustainable Use of Plant Resources of Kunming Institute of Botany). Later on, Kunming Station was upgraded as an independent institute, Kunming Institute of Botany, Chinese Academy of Sciences. Mr. Zhengyi Wu was appointed as the Director and Mr. Xitao Cai as the deputy director.<Figure ID="Fig4" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 4</CaptionNumber>
                <CaptionContent>
                  <SimplePara>Premier Zhou Enlai had a cordial talk with Mr. Cai (right) under rubber forest in Xishuangbanna</SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO4">
                <ImageObject Width="1653" Type="Halftone" Resolution="300" Rendition="HTML" Height="1280" Format="JPEG" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-018-0564-1/MediaObjects/13238_2018_564_Fig4_HTML.jpg"/>
              </MediaObject>
            </Figure></Para>
          <Para ID="Par6">In 1959, under the propose and leadership of Mr. Cai, Kunming Institute of Botany started to found the first tropical botanical garden of China in Huludao Peninsula, Xishuangbanna, South Yunnan. Mr. Cai made great contribution to the foundation of botanical garden, and all institute staff were deeply touched by Mr. Cai’s scientific spirit and noble personalities. Mr. Cai and his team have overcome many unimaginable difficulties and made great achievements in the construction of the garden and scientific researches. They also cultivated many talents who later became famous scientists in China and abroad. In 1978, the tropical botanical garden was renamed as Yunnan Institute of Tropical Botany, the predecessor of Xishuangbanna Tropical Botanical Garden, Chinese Academy of Sciences (Qin, <CitationRef CitationID="CR3">1991</CitationRef>).</Para>
          <Para ID="Par7">As early as 1940s, Mr. Cai paid attention to tobacco production in Yunnan. In 1945, his team succeed in the domestication and cultivation of Mammoth Gold in Kunming, a tobacco cultivar introduced by Mr. Huanyong Chen (Woon-Young Chun) (Huang, <CitationRef CitationID="CR2">2016</CitationRef>) from Virginia, USA. Mr. Cai organized a series of training workshops for tobacco cultivar selection, demonstration and large-scale cultivation, and made earliest contributions for tobacco production in Yunnan. In the 1950s, he led an investigation team and explored the rubber resources in Yunnan. He proposed that Xishuangbanna was the most suitable place for rubber tree plantation, which was adopted by the State Council to build the rubber plantation farm in China. And as a member of national rubber research team, Mr. Cai was awarded the First Prize for National Invention by the National Science Committee in 1982. Furthermore, he and his team explored and introduced many important medicinal plants, oil plants, aromatic and spice plants, valuable fast-growing species and so on, such as dragon blood trees, hodgsonia squash, camphor oil tree and mytenus plants. They have provided new resources and technologies for the economic and social development in the tropical areas in China.</Para>
          <Para ID="Par8">As the saying goes, “it takes ten years to grow a tree but it takes a hundred years to rear people”. As a master in science, Mr. Cai guided many projects and researches, established lots of scientific research divisions and promoted a great deal of scientific research findings. As an educator, Mr. Cai has brought up numerous professionals and therefore built up a multi-disciplinary research team. He taught the younger generations English, Latin, botanical courses and guided them in botanical expedition, plant domestication, phytochemical analysis, plant cultivation and scientific paper writing. He clearly stated that the young scientists should learn in practice, accumulate and expand their knowledge and explore their own way to success. Mr. Yaozong Feng, one of Mr Cai’s students, wrote in <Emphasis Type="Italic">Teacher Cai Xitao—My Guide in Scientific Exploration</Emphasis>, “Mr. Cai is not only my enlightenment teacher but also the leader that guides me along all the time”.</Para>
          <Para ID="Par9">Mr. Cai has made outstanding contributions to botanical researches in Yunnan, the “Plant Kingdom”. His devotion to science will always be an inspiration for people in Xishuangbanna Tropical Botanical Garden and Kunming Institute of Botany, two botanical institutions founded by him.</Para>
        </Body>
        <BodyRef TargetType="OnlinePDF"/>
        <ArticleBackmatter>
          <Bibliography ID="Bib1">
            <Heading>References</Heading>
            <Citation ID="CR1">
              <BibArticle>
                <BibAuthorName>
                  <Initials>H</Initials>
                  <FamilyName>Jiang</FamilyName>
                </BibAuthorName>
                <Year>2017</Year>
                <ArticleTitle xml:lang="en" Language="En">Dejun Yu: a patriotic botanist and his contributions</ArticleTitle>
                <JournalTitle>Protein Cell</JournalTitle>
                <VolumeID>8</VolumeID>
                <IssueID>11</IssueID>
                <FirstPage>785</FirstPage>
                <LastPage>787</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1007/s13238-017-0381-y</Handle>
                </Occurrence>
                <Occurrence Type="PID">
                  <Handle>28224292</Handle>
                </Occurrence>
                <Occurrence Type="PMCID">
                  <Handle>5676588</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Jiang H (2017) Dejun Yu: a patriotic botanist and his contributions. Protein Cell 8(11):785–787</BibUnstructured>
            </Citation>
            <Citation ID="CR2">
              <BibArticle>
                <BibAuthorName>
                  <Initials>R-L</Initials>
                  <FamilyName>Huang</FamilyName>
                </BibAuthorName>
                <Year>2016</Year>
                <ArticleTitle xml:lang="en" Language="En">Prof Huan-Yong Chen: a leading botanist and taxonomist, one of the pioneers and founders of modern plant taxonomy in China</ArticleTitle>
                <JournalTitle>Protein Cell</JournalTitle>
                <VolumeID>7</VolumeID>
                <IssueID>11</IssueID>
                <FirstPage>773</FirstPage>
                <LastPage>776</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1007/s13238-016-0311-4</Handle>
                </Occurrence>
                <Occurrence Type="PID">
                  <Handle>27679499</Handle>
                </Occurrence>
                <Occurrence Type="PMCID">
                  <Handle>5084154</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Huang R-L (2016) Prof Huan-Yong Chen: a leading botanist and taxonomist, one of the pioneers and founders of modern plant taxonomy in China. Protein Cell 7(11):773–776</BibUnstructured>
            </Citation>
            <Citation ID="CR3">
              <BibUnstructured>Qin R (1991) Professor Cai Xitao-Trailblazer of Yunnan Botany Institute, festschrift of Cai Xitao, Yunnan Science and Technology Press (秦仁昌. 云南植物研究的拓荒者——蔡希陶教授,蔡希陶纪念文集,云南科技出版社: 13–14)</BibUnstructured>
            </Citation>
            <Citation ID="CR4">
              <BibUnstructured>Wu Z (1991) Belated missing, festschrift of Cai Xitao, Yunnan Science and Technology Press, pp 10–12 (吴征镒.也是迟来的怀念,蔡希陶纪念文集,云南科技出版社: 10–12).</BibUnstructured>
            </Citation>
            <Citation ID="CR5">
              <BibUnstructured>Xun W, Wang Z, Xiao Y (1993) Brief Biography of Cai Xitao, International cultural publishing co., Ltd., pp 21–22 (旭文,王振淮,晓戈.蔡希陶传略.国际文化出版公司: 21–22).</BibUnstructured>
            </Citation>
          </Bibliography>
        </ArticleBackmatter>
      </Article>
    </JournalOnlineFirst>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>18913.zip</EditorialManuscriptNumber>
            </JobSheet></Publisher>


