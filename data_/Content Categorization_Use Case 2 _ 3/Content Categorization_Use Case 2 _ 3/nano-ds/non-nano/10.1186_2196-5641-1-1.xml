<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="BMC2196-5641-1-1">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="EditorialNotes">
            <ArticleID>1</ArticleID>
            <ArticleDOI>10.1186/2196-5641-1-1</ArticleDOI>
            <ArticleCitationID>1</ArticleCitationID>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Editorial</ArticleTitle>
            <ArticleCategory>Editorial</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>2</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2014</Year>
                <Month>1</Month>
                <Day>31</Day>
              </RegistrationDate>
              <Received>
                <Year>2014</Year>
                <Month>1</Month>
                <Day>31</Day>
              </Received>
              <Accepted>
                <Year>2014</Year>
                <Month>1</Month>
                <Day>31</Day>
              </Accepted>
              <OnlineDate>
                <Year>2014</Year>
                <Month>3</Month>
                <Day>13</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Piccolo; licensee Springer.</CopyrightHolderName>
              <CopyrightYear>2014</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd. This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/2.0</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/2.0"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>40538</JournalID>
              <VolumeIDStart>1</VolumeIDStart>
              <VolumeIDEnd>1</VolumeIDEnd>
              <IssueIDStart>1</IssueIDStart>
              <IssueIDEnd>1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1 Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Alessandro</GivenName>
                  <FamilyName>Piccolo</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>Alessandro.piccolo@unina.it</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgDivision>Università di Napoli Federico II</OrgDivision>
                <OrgName>Centro Interdipartimentale di Ricerca sulla Risonanza Magnetico Nucleare per l&apos;Ambiente, l&apos;Agroalimentare ed i Nuovi Materiali (CERMANU)</OrgName>
                <OrgAddress>
                  <Street>Via Università 100</Street>
                  <Postcode>80055</Postcode>
                  <City>Portici</City>
                  <Country Code="IT">Italy</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgName>Department of Agriculture</OrgName>
                <OrgAddress>
                  <Street>Via Università 100</Street>
                  <Postcode>80055</Postcode>
                  <City>Portici</City>
                  <Country Code="IT">Italy</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>It appears particularly unappealing to present a new scientific journal today. There are already so many different journals that one may tend to say, ‘no thanks, not another one!’ However, a more thoughtful reflection shows that there is a well-founded answer to the general questions: why another journal, and why an open access journal? We may even find a sounder answer to the question: why this specific journal?</Para>
            <Para>I believe that it is necessary to overcome the boundaries among disciplines and even more so in the domain of agriculture, in which scientific and technological advancement determines the future of the human generations to come. In fact, agriculture is called on to increase productivity in order to feed an ever increasing population, but it must also conjugate its more intense efficiency with the protection of the agro-ecosystem and, in doing so, the overall sustainability of the global environment. This objective cannot be achieved if agriculture does not begin to be the recipient of the most advanced molecular knowledge reached by basic sciences like chemistry, biochemistry, and biology, and transformed into technologies to be applied for a sustainable and environmentally conscious agricultural productivity.</Para>
            <Para>This is the aim of the new SpringerOpen journal <Emphasis Type="Italic">Chemical and Biological Technologies in Agriculture</Emphasis> (CBTA): an interdisciplinary forum to present visions and projects for innovative agricultural technologies increasingly based on molecular understanding of mechanisms and processes.</Para>
            <Para>I have been in the science business for a long time and have reached a satisfying publication rate in traditional journals which are sold to libraries or as personal subscriptions. However, I have become aware that the traditional subscription model for publishing journals is not the only, or best, way of publishing research in certain fields. Indeed, the content of subscription journals may sometimes represent a serious limitation to the circulation of the ever increasing body of scientific knowledge being produced by not only developed but also emerging countries.</Para>
            <Para>Open access journals offer a refreshing alternative. Institutions no longer need to pay to access a journal, and researchers can read scientific articles from anywhere, provided they have access to the internet. Moreover, the cost of publication for authors is not a loss of precious research funds, as it could appear at first glance. In fact, the reward is not only the rapidity of online publication, but also the fact that anyone anywhere in the world is able to read the published results, thereby increasing scientific visibility and related bibliometric pmeters (e.g., H-index). Through this open access journal, I want to allow researchers from a wide variety of disciplines to read and write about important research in agriculture, and I hope that the accessibility of this journal will bring different branches of the community together.</Para>
            <Para>All aspects of Agriculture activities can be the subject of manuscripts submitted to CBTA, provided that they address applications of modern biomolecular technologies. Obvious issues are related to plant-soil-microbe interactions in order to improve knowledge on plant nutrition that in some respect is still anchored to Liebig&apos;s traditional concepts of inorganic nitrogen uptake, while the complex world of chemical, biochemical, and microbial interactions involved in the ‘organic’ nutrition of plants is still to be unraveled. However, it is not only important to comprehend the natural bio-molecular mechanisms of plant growth but also to develop the potentials of modern supramolecular and nanomaterial chemistry to precisely and specifically release the required elements and molecules to plant systems. Again, the understanding of plant biochemistry and physiology deepened by the application of the most advanced instrumentation (NMR, mass spectrometry, etc.) should go along with modern biomolecular techniques to improve the genetic response of plants to external technological systems. Structure-activity relationships between plant-rhizosphere-microbes and biomolecules, as well as advanced molecular releasing systems, will be of primary importance for a future sustainable agriculture. Modern techniques such as the omics, coupled to bio-informatic and chemical computational systems, should guide the establishment of innovative technologies for agricultural practices.</Para>
            <Para>A challenge for CBTA is to stimulate new bio-molecular approaches to fight the causes of climate change derived from wrong or still rudimental agricultural practices, which are responsible for a large part of the enhancement of greenhouse gases in the atmosphere. Moreover, chemical and biological technologies should be applied to ensure safe and sustainable production of non-food biomasses to support the recently born bio-refinery industry and convince its actors to wisely and profitably recycle the unused or wasted biomasses in agriculture.</Para>
            <Para>Finally, CBTA will also host manuscripts devoted to food security and health through applications of advanced analytical techniques such as the omics and physical-chemical instrumentation. In particular, new technologies for the biomolecular appraisal of quality and origin (traceability) of food products will be welcomed by this journal.</Para>
            <Para>Although the challenge today for a new open access journal is great, our aspiration is for CBTA to become an innovative vehicle of interdisciplinary biomolecular sciences in the agricultural sector, and, with time, CBTA will be comparable to journals of monothematic disciplines (soil vs. plant vs. microbes). CBTA will hopefully stimulate scientists to a broader, and therefore more effective, approach in their investigation, thus paying a contribution to a more sustainable but still more productive future for agriculture.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


