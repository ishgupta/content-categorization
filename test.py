# -*- coding: utf-8 -*-
"""
Created on Sat Aug 18 15:42:34 2018

@author: ishgupta
"""


# In[]
from flask import Flask                                     # to deploy as a flask service
from flask import jsonify                                   # to parse the response as JSON
from flask_cors import CORS                                 # to enable Cross-Origin Resource Sharing
import json
from pathlib import Path
# In[]
app = Flask(__name__)
CORS(app)

# In[]
@app.route("/documents_with_info_on_species", methods = ['GET'])
def test_nouns():
    output_dir = Path("output")
    if output_dir.is_dir():
        output_file = Path("output/documents_with_info_on_species.json")
        if output_file.is_file():
            with open(output_file) as output:
                result = json.load(output)
                
    return jsonify(result)

# In[]
@app.route("/documents_with_no_info_on_species", methods = ['GET'])
def test_keywords():
    output_dir = Path("output")
    if output_dir.is_dir():
        output_file = Path("output/documents_with_no_info_on_species.json")
        if output_file.is_file():
            with open(output_file) as output:
                result = json.load(output)
                
    return jsonify(result)


# In[]
@app.route("/related_sentences_in_docs", methods = ['GET'])
def test_doc_groups():
    output_dir = Path("output")
    if output_dir.is_dir():
        output_file = Path("output/related_sentences_in_docs.json")
        if output_file.is_file():
            with open(output_file) as output:
                result = json.load(output)
                
    return jsonify(result)

# In[]:
@app.route("/test_faulty_docs", methods = ['GET'])
def test_faulty_docs():
    output_dir = Path("output")
    if output_dir.is_dir():
        output_file = Path("output/faulty_docs.json")
        if output_file.is_file():
            with open(output_file) as output:
                result = json.load(output)
                
    return jsonify(result)



# In[]
app.run(host="0.0.0.0", port = 4567, threaded = True)      
