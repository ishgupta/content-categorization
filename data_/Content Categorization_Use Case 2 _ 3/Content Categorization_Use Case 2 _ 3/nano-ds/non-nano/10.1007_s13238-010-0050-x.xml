<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-010-0050-x">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>50</ArticleID>
            <ArticleDOI>10.1007/s13238-010-0050-x</ArticleDOI>
            <ArticleSequenceNumber>3</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" Language="En">Shizhang Bei (Sitsan Pai) and his theory of cell reformation</ArticleTitle>
            <ArticleCategory>Recollection</ArticleCategory>
            <ArticleFirstPage>315</ArticleFirstPage>
            <ArticleLastPage>318</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2010</Year>
                <Month>5</Month>
                <Day>5</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2010</Year>
                <Month>4</Month>
                <Day>1</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Higher Education Press and Springer-Verlag Berlin Heidelberg</CopyrightHolderName>
              <CopyrightYear>2010</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="Regular">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Guyan</GivenName>
                  <FamilyName>Wang</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>wang_gy2000@yahoo.com.cn</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Institute of Biophysics</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Cell Origin</Keyword>
              <Keyword>Cell Reproduction</Keyword>
              <Keyword>Prokaryotic Cell</Keyword>
              <Keyword>Trachoma</Keyword>
              <Keyword>Compilation Cell</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="PDF">
            <page id="pg01">Protein Cell 2010, 1(4): 315–318 DOI 10.1007/s13238-010-0050-x Protein &amp; Cell RECOLLECTION Shizhang Bei (Sitsan Pai) and his theory of cell reformation Guyan Wang ✉ Institute of Biophysics, Chinese Academy of Sciences, Beijing 100101, China ✉ Correspondence: wang_gy2000@yahoo.com.cn THE DISCOVERY OF THE PHENOMENON OF CELL REFORMATION For a long period of time, people always studied the cell reproduction and proliferation from the traditional viewpoints and in particular focused on cell division and mitosis. Cell division was thought to be the only way of cell reproduction and proliferation, little attention was drawn to other possibi- lities. Until the spring of 1932, Professor Shizhang Bei (Sitsan Pai) found an intersex strain of Chirocephalus nankinensis, which belongs to Phyllopod of Crustacea, among the speci- mens collected in a paddy ﬁeld of Songmuchang on the outskirts of Hangzhou. Individual of intersex has character- istics of both sexes at the same time during a certain period of development stage, and intersex in Chirocephalus has not been recorded before. The intersex of Chirocephalus could be divided into ﬁve types according to the extents of sexual inclination to male or female. The whole process of sex reversal of gonad was studied, and during its reversal, the transformations of germ cells including cell deformation and cell reformation were observed. In 1934, Professor Bei reported the observation and his viewpoint of such phenomenon at a seminar in the Biology Department of Zhejiang University, where he held a position of professor. However, the report was not published until 1942 and 1943. The reason of the delayed publication of the report was multiple. Firstly, under the turbulent circumstances of the Anti-Japanese war, Zhejiang University was wandering from Hangzhou into inland and was moving from place to place many times. Secondly, there was no suitable journal to which the scientiﬁc report could be submitted for publication. Until 1942, the report was not formally published on a newly established periodical Science Record. These reasons were all the objective facts, and the subjective reason is that he also felt some hesitation in publishing the report to the public because his observation and viewpoint would be a challenge to the traditional theory of cell proliferation. In the two papers, Professor Bei analyzed the deformation and reformation of germ cells during the sex reversal process about all ﬁve types of intersexes, and discussed the possible mechanism. He called this phenomenon reformation instead of new formation because the reformation only means resurgence and the yolk granules possess all the raw materials of the cellular constituents. When the cells deformed, the yolk granules were yielded, which in turn provided the reformation of cells with the raw materials. It suggests that when the material basis for cell restitution and a suitable environment exist, the cell may be self-reconstructed not through the way of cell division. But in the circle of biology, it is a traditional view that a cell comes from its parent cell and the cell division is the only way of cell reproduction and proliferation. Therefore, the idea that the cell can be reproduced and proliferated by the way of cell reformation besides cell division would be considered as a profane conduct against the biological science. Some unfair words In memory of Shizhang Bei (Sitsan Pai) (1903–2009), the founder of Chinese biophysics and the Biophysical Society of China. © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010 315</page>
            <page id="pg02">Protein &amp; Cell Guyan Wang The intersex of Chirocephalus. such as ridicule, ignorance or arrogance, etc. might be also imposed upon the idea. These doubts and misgivings should be beneath notice for the reports were published after all and did not incur censure. Perhaps somebody thinks that the cell reformation is not easy or frequent to observe if it exists. However, the traditional point of view is always accepted as an infallible law, the inﬂuence of which is wide and lasting. So, Professor Bei’s research appeared to be an isolated example and was not enough to elucidate a general rule of cell proliferation. Since there was no response, his lonely feeling was hard to avoid and at that time he was busy at other work, so he could only put this research aside. Time was slipping away fast and more than twenty years had elapsed when the study on cell reformation was started again in 1970. Since then, the research has entered a new period and had a new appearance as compared to that in the 1930s. In the past, the work was done mainly by one person, but now, they have established a research group in the Institute of Biophysics under the Chinese Academy of Sciences; in the past the experimental material was only one kind, i.e., Chirocephalus nankinensis, but now they use chicken embryo, mouse bone marrow, Chlamydia trachoma- tis and soy bean nodule bacteria besides Chirocephalus nankinensis as the studying materials. Light microscopic observation was the sole way used to study cell reformation in the 1930s, but now they have used many kinds of new techniques and methods such as electron microscopy, time- lapse microcinematography, phase contrast microscopy, radioautography, ﬂuorescence polarization, energy transfer with double ﬂuorescence labels, ﬂuorescence recovery after photobleaching, Raman spectra and many other biochemical methods. On the basis of the past research work, they have acquired new discoveries and understanding about cell reformation. For example, they have found DNA, histones and chromatin in the yolk granules of chicken eggs. A new discovery that the yolk granules of chicken eggs possess chromatin has never been reported before in the biological science. Furthermore, the chromatin of yolk granules and that Electron micrographs of the cell reformation of the yolk granules within the mature oocyte in situ of Chirocephalus nankinensis. (A) Structural change of the yolk granules. (B) Further development of the yolk granules. (C) The yolk granules beginning to develop into nucleus-like structures. (D) Three “naked nuclei”. Here the nuclear membranes and reticular structures of nuclei are obvious. (E) A reformed nucleus. Cytoplasm and a prat of plasma membrane just being formed. (F) A relatively full- developed cell, but the morphological structure of the whole cell is rather primitive. 316 © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010</page>
            <page id="pg03">Shizhang Bei and his theory of cell reformation Protein &amp; Cell of nucleus have the same structure and behavior. The shapes of DNA molecules from both origins are similar too. The chromatin is believed, according to the traditional biology, to exist only speciﬁcally in nucleus, so the fact that the yolk granules also possess chromatin cannot but attract the attention of the biological scientists. THE FOUNDATION OF CELL REFORMATION THEORY According to the results of experimental research, Professor Bei and his colleagues have published 5 papers in Scientia Sinica (Series B, 1983), 24 papers in compilation Cell Reformation (Series 1, 1988) and 18 papers in compilation Cell Reformation (Series 2, 2003). Professor Bei had obtained fairly systematic knowledge on cell reformation through experimental research and founded the theory of cell reformation. It can be summarized as follows: 1) Cell reformation is a process of self-organization. It is able to take place in organism in situ or in cell-free preparations in vitro only if there exist a material basis for cell restitution and suitable conditions. 2) Cell reformation exists extensively in the nature. It can be found not only in eukaryotic cells, but also in prokaryotic cells; not only in germ cells, but also in somatic cells of developing and adult animals. 3) The superﬁcial yolk granules beneath the chicken blastoderm contain DNA, histones and chromatin, and can reform cell under suitable environments. It should be pointed out that the chromatin is not a speciﬁc substance belonging to nucleus only and the yolk granules are not merely a non-living inclusion of cell. 4) Cell and nucleus can be reformed from cytoplasm. It suggests that there is no strict barrier between nucleus and cytoplasm. 5) Cell reformation is probably the reﬂection of cell origin on the earth in the present living world. It is an epitome of the long progress of a cell from the simplest living form. The research on cell reformation would help to elucidate the evolution of life. 6) Cell division is a &quot;closed&quot; type of cell reproduction, when cells divide, they are isolated from their surrounding environ- ment by a cellular membrane; while cell reformation is an &quot;open&quot; type of cell reproduction, during the whole process of cell reformation the constituents of cell and the surrounding environment are merged into one. So, if the study of cell division is combined together with that of cell reformation and the simulation of cell reformation is combined together with the induction of cell reformation, then they would provide a new approach of changing the cell structures and properties, selecting good qualities and eliminating harmful factors, controlling directive breeding, i.e., promoting and developing the cell technology and cell engineering. That is to say, cell proliferation in the living organism includes not only the replication of present cells (cell devision), but also the reproduction of new cell by self- organization from the raw materials of cellular constituents (cell reformation). In cell reformation, new cells can start from the very beginning, non-cellular form, and be reproduced gradually from the materials in cytoplasm of mother cell. Generally, the process of reproduction includes the initial formation of nucleus as the naked nucleus state, then the cytoplasm and cellular membrane, and ﬁnally the intact reformed cell. CELL REFORMATION IS A TRACE LEFT BEHIND BY CELL ORIGIN PROCESS Professor Bei believed that cell reformation research involves the fundamental issues in cytology and will have an important impact on the knowledge of the origin and life activities of cell, as well as on the future development of biology. Self-organization is an important process in living activities. The all biological structures at different levels can be formed by self-organization. For example, during evolution, the human cerebrum improves its structures by self-organization to adapt the environmental changes, cell reformation is a process of self-organization, similarly, the cell origin is also a self-organization process. As the basic unit of life origin and evolution, cell is a typical self-organizing system. During evolution, life adapts the environment changes by improving its own structures. For example, in the process of chemistry evolution, once the organic molecule formed, it follows the rule of physics (thermodynamics), and tends to stabilize in a structure state by self-organization, then forms chain, ring, helix and fold, it presents special biological activities. Then, these molecules form a related multi-molecular system. After the original life appears, it further improves the structure through self- organization, and evolves to cellular form from the non- cellular form. Finally the original cell develops to be the prokaryotic and eukaryotic cell with sophisticated structure The chromatin contained in yolk granules beneath the chicken blastoderm. © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010 317</page>
            <page id="pg04">Protein &amp; Cell Guyan Wang and functions, such as enzymic catalysis, DNA replication, transcription and translation. Professor Bei’s hypothesis on “cell reformation may be an epitome of cell origin on the earth” means that cell reformation is probably a trace of the progress of the cell origin in present living activities. We can imagine that the original lives exist in a non-cellular form of molecular aggregates, such as the microsphere proposed by S.W.Fox (1965), or the coacervate proposed by A.I.Oparin (1969). Then, gradually, the mole- cular aggregates reform to be the original cell in a process of self-organization, like cell reformation. Therefore, cell refor- mation should be a trace of the progress of the cell origin in reproduction and proliferation of the present life, while further knowledge of cell reformation in present organism is undoubtedly an important approach to explore the origin of cell. Cell reformation is the trace left behind by the life origin in present organism, just like that tails and gill slits in early embryos of ﬁsh, amphibians, reptiles, birds, mammals and human, the human brain&apos;s the old cortex and the ancient cortex preserved in the evolution under neocortex of the human brain, the ancestral genes retained in the human genome. A thorough study on these traces and search for the process of changes of non-cellular form into cellular form are important approaches to explore the origin and evolution of life. CELL REFORMATION IS AN APPROACH TO EXPLORE THE ORIGIN OF CELL Cell origin and cell reformation are similar processes, both of which are self-organization processes from non-cellular form to cellular form. Therefore exploring the process of cell reformation could provide valuable hints for cell origin. 1) The origin of cell by can be investigated through understanding nucleus reformation and cell reformation, the transformation from non-cell form to cell form, or the transformation from the cell products to cell. These phenom- ena may exist in present organism when cells are changing greatly, such as wound healing, marrow blood-forming, insect metamorphosis, carcinogenesis, radio injuring and repairing, or animal’s sex translation. 2) To determine the mechanism of cell origin, cell reformation can be artiﬁcially induced by electric, magnetic or gravitational ﬁeld, which is similar to the ancient conditions on earth. 3) Cell reformation can also be artiﬁcially induced by changing the concentration of one-valence and two-valence metal ions. For example, we can simulate the processes of disappearance and reformation of nuclear membrane under artiﬁcial micro-environment to understand the origin of the nuclear membrane, the simulation of cell reformation can be used to explore the techniques to synthesize cells, which is also important to study the origin of cell. 4) Currently, it is widely accepted that eukaryotic cells are evolved from the prokaryotic cells. However, latest studies have shown that both the prokaryotic and the eukaryotic cells can be reconstructed from non-cellular living materials through self-organization or cell reformation. Therefore, we are not able to conclude whether the prokaryotic and eukaryotic cells are independently originated. That is to say, the eukaryotic cells are not necessarily evolved from the prokaryotic cells. 5) The origin of nucleus and cell may be different from endosymbiotic hypothesis and compartmental hypothesis, which support that some structures are the basis of origin. In contrast, it is suggested that as far as there exist a material basis for cell restitution and suitable conditions, the original nucleus and cell may be self-reconstructed from non-cellular form through the processes of cell reformation. REFERENCES Sitsan Pai. (1942). Diploide Intersexen bei Chirocephalus nankinen- sis Shen. Sci Rec 1, 187–197. Shizhang Bei. (1943). Yolk granule and cell reformation. Science (in Chinese) 26, 38–49. (贝时璋. (1943). 卵黄粒与细胞之重建. 科学. 26, 38–49.) Sitsan Pai. (1943). Ueber die Transformation der Genitalzellen bei den Chirocephalus-Intersexen. Sci Rec 2, 573–583. Shizhang Bei, Maosun Cao, et al. (1983). Time-lapse microcinema- tography and phase contrast studies on the cell reformation of chirocephalus yolk granules in vitro. Sci Sin B 26, 454–459. Shizhang Bei, Chuchu Chen, et al. (1983). An Electron microscopy study on cell reformation of chirocephalus yolk granules cultured in vitro. Sci Sin B 26, 592–597. Shizhang Bei, Yuan Li, et al. (1983). Electron microscopy study on the chromatin of blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs. Sci Sin B 26, 708–713. Shizhang Bei, Yuan Li, et al. (1983). Electron microscopy of DNA molecules from blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs. Sci Sin B 26, 818–821. Shizhang Bei, Yuan Li, et al. (1983). Chromatin and DNA of yolk granules beneath “blastoderm region” of unfertilized chicken egg. Sci Sin B 26, 822–826. Shizhang Bei. (1988). Cell Reformation (Series 1). Beijing, Science Press. (贝时璋. (1988). 细胞重建(第一集). 北京, 科学出版社.) Shizhang Bei. (2003). Cell Reformation (Series 2). Beijing, Science Press. (贝时璋. (2003). 细胞重建(第二集). 北京, 科学出版社.) 318 © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010</page>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Pai</FamilyName>
                  </BibAuthorName>
                  <Year>1942</Year>
                  <ArticleTitle xml:lang="en" Language="En">Diploide Intersexen bei Chirocephalus nankinensis Shen</ArticleTitle>
                  <JournalTitle>Sci Rec</JournalTitle>
                  <VolumeID>1</VolumeID>
                  <FirstPage>187</FirstPage>
                  <LastPage>197</LastPage>
                </BibArticle>
                <BibUnstructured>Sitsan Pai. (1942). Diploide Intersexen bei Chirocephalus nankinensis Shen. Sci Rec 1, 187–197.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <Year>1943</Year>
                  <ArticleTitle xml:lang="en" Language="En">Yolk granule and cell reformation</ArticleTitle>
                  <JournalTitle>Science</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>38</FirstPage>
                  <LastPage>49</LastPage>
                  <BibComments>(贝时璋. (1943). 卵黄粒与细胞之重建. 科学. 26, 38–49.)</BibComments>
                </BibArticle>
                <BibUnstructured>Shizhang Bei. (1943). Yolk granule and cell reformation. Science (in Chinese) 26, 38–49. (贝时璋. (1943). 卵黄粒与细胞之重建. 科学. 26, 38–49.)</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Pai</FamilyName>
                  </BibAuthorName>
                  <Year>1943</Year>
                  <ArticleTitle xml:lang="de" Language="De">Ueber die Transformation der Genitalzellen bei den Chirocephalus-Intersexen</ArticleTitle>
                  <JournalTitle>Sci Rec</JournalTitle>
                  <VolumeID>2</VolumeID>
                  <FirstPage>573</FirstPage>
                  <LastPage>583</LastPage>
                </BibArticle>
                <BibUnstructured>Sitsan Pai. (1943). Ueber die Transformation der Genitalzellen bei den Chirocephalus-Intersexen. Sci Rec 2, 573–583.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M.</Initials>
                    <FamilyName>Cao</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1983</Year>
                  <ArticleTitle xml:lang="en" Language="En">Time-lapse microcinematography and phase contrast studies on the cell reformation of chirocephalus yolk granules in vitro</ArticleTitle>
                  <JournalTitle>Sci Sin B</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>454</FirstPage>
                  <LastPage>459</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:STN:280:DyaL3s3kvFSqsw%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>6867685</Handle>
                  </Occurrence>
                  <BibComments>1:STN:280:DyaL3s3kvFSqsw%3D%3D, 6867685</BibComments>
                </BibArticle>
                <BibUnstructured>Shizhang Bei, Maosun Cao, <Emphasis Type="Italic">et al.</Emphasis> (1983). Time-lapse microcinematography and phase contrast studies on the cell reformation of chirocephalus yolk granules in vitro. Sci Sin B 26, 454–459.</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>C.</Initials>
                    <FamilyName>Chen</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1983</Year>
                  <ArticleTitle xml:lang="en" Language="En">An Electron microscopy study on cell reformation of chirocephalus yolk granules cultured in vitro</ArticleTitle>
                  <JournalTitle>Sci Sin B</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>592</FirstPage>
                  <LastPage>597</LastPage>
                </BibArticle>
                <BibUnstructured>Shizhang Bei, Chuchu Chen, <Emphasis Type="Italic">et al.</Emphasis> (1983). An Electron microscopy study on cell reformation of chirocephalus yolk granules cultured in vitro. Sci Sin B 26, 592–597.</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y.</Initials>
                    <FamilyName>Li</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1983</Year>
                  <ArticleTitle xml:lang="en" Language="En">Electron microscopy study on the chromatin of blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs</ArticleTitle>
                  <JournalTitle>Sci Sin B</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>708</FirstPage>
                  <LastPage>713</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:STN:280:DyaL2c%2FksVOhtA%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>6685342</Handle>
                  </Occurrence>
                  <BibComments>1:STN:280:DyaL2c%2FksVOhtA%3D%3D, 6685342</BibComments>
                </BibArticle>
                <BibUnstructured>Shizhang Bei, Yuan Li, <Emphasis Type="Italic">et al.</Emphasis> (1983). Electron microscopy study on the chromatin of blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs. Sci Sin B 26, 708–713.</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y.</Initials>
                    <FamilyName>Li</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1983</Year>
                  <ArticleTitle xml:lang="en" Language="En">Electron microscopy of DNA molecules from blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs</ArticleTitle>
                  <JournalTitle>Sci Sin B</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>818</FirstPage>
                  <LastPage>821</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaL3sXlvV2qtbw%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>6684789</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DyaL3sXlvV2qtbw%3D, 6684789</BibComments>
                </BibArticle>
                <BibUnstructured>Shizhang Bei, Yuan Li, <Emphasis Type="Italic">et al.</Emphasis> (1983). Electron microscopy of DNA molecules from blastoderm nuclei and yolk granules beneath blastoderm of fertilized and unincubated chicken eggs. Sci Sin B 26, 818–821.</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y.</Initials>
                    <FamilyName>Li</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>1983</Year>
                  <ArticleTitle xml:lang="en" Language="En">Chromatin and DNA of yolk granules beneath “blastoderm region” of unfertilized chicken egg</ArticleTitle>
                  <JournalTitle>Sci Sin B</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>822</FirstPage>
                  <LastPage>826</LastPage>
                </BibArticle>
                <BibUnstructured>Shizhang Bei, Yuan Li, <Emphasis Type="Italic">et al.</Emphasis> (1983). Chromatin and DNA of yolk granules beneath “blastoderm region” of unfertilized chicken egg. Sci Sin B 26, 822–826.</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <BibBook>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <Year>1988</Year>
                  <BookTitle>Cell Reformation (Series 1)</BookTitle>
                  <PublisherName>Science Press</PublisherName>
                  <PublisherLocation>Beijing</PublisherLocation>
                  <BibComments>(贝时璋. (1988). 细胞重建(第一集). 北京, 科学出版社.)</BibComments>
                </BibBook>
                <BibUnstructured>Shizhang Bei. (1988). Cell Reformation (Series 1). Beijing, Science Press. (贝时璋. (1988). 细胞重建(第一集). 北京, 科学出版社.)</BibUnstructured>
              </Citation>
              <Citation ID="CR10">
                <BibBook>
                  <BibAuthorName>
                    <Initials>S.</Initials>
                    <FamilyName>Bei</FamilyName>
                  </BibAuthorName>
                  <Year>2003</Year>
                  <BookTitle>Cell Reformation (Series 2)</BookTitle>
                  <PublisherName>Science Press</PublisherName>
                  <PublisherLocation>Beijing</PublisherLocation>
                  <BibComments>(贝时璋. (2003). 细胞重建(第二集). 北京, 科学出版社.)</BibComments>
                </BibBook>
                <BibUnstructured>Shizhang Bei. (2003). Cell Reformation (Series 2). Beijing, Science Press. (贝时璋. (2003). 细胞重建(第二集). 北京, 科学出版社.)</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


