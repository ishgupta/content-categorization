<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-015-0214-9">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>214</ArticleID>
            <ArticleDOI>10.1007/s13238-015-0214-9</ArticleDOI>
            <ArticleSequenceNumber>2</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">The journey of nisin development in China, a natural-green food preservative</ArticleTitle>
            <ArticleCategory>Recollection</ArticleCategory>
            <ArticleFirstPage>709</ArticleFirstPage>
            <ArticleLastPage>711</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2015</Year>
                <Month>9</Month>
                <Day>8</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2015</Year>
                <Month>9</Month>
                <Day>30</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>The Author(s)</CopyrightHolderName>
              <CopyrightYear>2015</CopyrightYear>
              <License Version="4.0" Type="OpenAccess" SubType="CC BY">
                <SimplePara>
                           <Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution 4.0 International License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/4.0/</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/4.0/"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided you give appropriate credit to the original author(s) and the source, provide a link to the Creative Commons license, and indicate if changes were made.</SimplePara>
              </License>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author ID="Au1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Jie</GivenName>
                  <FamilyName>Zhang</FamilyName>
                </AuthorName>
              </Author>
              <Author ID="Au2" CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Jin</GivenName>
                  <FamilyName>Zhong</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>zhongj@im.ac.cn</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Institute of Microbiology</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Foreign Company</Keyword>
              <Keyword>Biosynthetic Gene Cluster</Keyword>
              <Keyword>Clostridium Botulinum</Keyword>
              <Keyword>Food Preservative</Keyword>
              <Keyword>Technique Foundation</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para ID="Par1">In 2005, the project “Research and Development of Nisin” led by Prof. Liandong Huan was awarded the National Science &amp; Technology Progress Prize (Second Class Prize) and China National Food Industry Association (CNFIA) Science &amp; Technology Prize (Special Prize).</Para>
            <Para ID="Par2">Nisin is a lantibiotic (bacteriocin containing unusual amino acids) (Fig. <InternalRef RefID="Fig1">1</InternalRef>) mainly produced by gram-positive lactic acid bacteria, such as <Emphasis Type="Italic">Lactococcus lactis</Emphasis>. It shows a broad spectrum of antimicrobial activity against many food-borne pathogens such as <Emphasis Type="Italic">Listeria monocytogenes</Emphasis>, <Emphasis Type="Italic">Staphylococcus aureus</Emphasis>, and <Emphasis Type="Italic">Clostridium botulinum</Emphasis>, at very low levels (nmol/L range). In addition, nisin is heat-stable, non-toxic, and sensitive to digestive proteases. So far, nisin is the only bacteriocin approved by both United States FDA and the WHO for use as a food preservative and it is utilized in more than 50 countries around the world. It is widely used in canned foods, dairy products, meat products, alcoholic beverages, etc.<Figure ID="Fig1" Float="Yes" Category="Standard">
                <Caption xml:lang="en" Language="En">
                  <CaptionNumber>Figure 1</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>The primary structure of nisin Z</SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject ID="MO1">
                  <ImageObject Type="Linedraw" Rendition="HTML" Format="GIF" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-015-0214-9/MediaObjects/13238_2015_214_Fig1_HTML.gif"/>
                </MediaObject>
              </Figure>
                  </Para>
            <Para ID="Par3">Nisin was first discovered in milk in 1928, around the same time as the first antibiotic penicillin was discovered. In the 1950s, nisin was commercially produced by Aplin &amp; Barrett in Britain. The study of nisin in China began in the late 1980s. At the end of 1987, scientists from the Institute of Microbiology, Chinese Academy of Sciences (IMCAS), realized that nisin was a product of great potential for food safety and human health. However, the production of nisin exclusively belonged to only one foreign company at that time. The former director of IMCAS, Prof. Yugu Xue (Fig. <InternalRef RefID="Fig2">2</InternalRef>), showed great foresight and immediately initiated a research project on nisin. Under her leadership, researchers from the Department of Genetics pioneered fundamental research on nisin in China. Based on genetic analysis, a new method to screen the producing strain of nisin was developed (Huan et al., <CitationRef CitationID="CR1">1997</CitationRef>), and an over-producer strain was obtained by physical and chemical mutagenesis and several rounds of mutation breeding. Moreover, the group also cloned the biosynthetic gene cluster of nisin and found that the product was a new type (nisin Z), which is different from the foreign nisin A.<Figure ID="Fig2" Float="Yes" Category="Standard">
                <Caption xml:lang="en" Language="En">
                  <CaptionNumber>Figure 2</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>Prof. Yugu Xue (1982)</SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject ID="MO2">
                  <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-015-0214-9/MediaObjects/13238_2015_214_Fig2_HTML.jpg"/>
                </MediaObject>
              </Figure>
                  </Para>
            <Para ID="Par4">During development, the group led by Prof. Liandong Huan (Fig. <InternalRef RefID="Fig3">3</InternalRef>) innovatively used cheap plant peptone and yeast powder as fermentation medium, which was much cheaper than the milk used by the foreign company. The lab trials indicated that they had successfully produced nisin. Large scale production pilot trials were carried out in Zhejiang Tiantai Silver-Elephant Bio-chemical Engineering factory (the former Zhejiang Silver-Elephant bioengineering Co., Ltd.) and succeeded two years later with much effort of the researchers. In 1995, the first production line of nisin with the ability to produce 20 tons every year was built in China. In 1996, the project “Special Preservative Nisin” was included in National Science and Technique Foundation during the “9th Five-Year Plan”. During this time, the production of nisin was extended to industrial scale. Furthermore, with the support of National Science and Technique Foundation during the “10th Five-Year Plan” and national high-tech industrialization demonstration project, the producing strain of nisin and the techniques for fermentation and extraction were further improved, resulting in a large increase in production and at a lower cost. Consequently, in 2005, the biggest modern factory of the world was founded in Tiantai county of Zhejiang province. Its production of nisin reached to 150 tons each year and the market share was also the highest of the world, creating great benefits for economy and society.<Figure ID="Fig3" Float="Yes" Category="Standard">
                <Caption xml:lang="en" Language="En">
                  <CaptionNumber>Figure 3</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>Prof. Liandong Huan (middle), associate Prof. Xiuzhu Chen (right), and Prof. Jin Zhong (left) at Silver-Elephant company</SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject ID="MO3">
                  <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-015-0214-9/MediaObjects/13238_2015_214_Fig3_HTML.jpg"/>
                </MediaObject>
              </Figure>
                  </Para>
            <Para ID="Par5">The process of research and development of nisin was tortuous. At first, the product was too advanced and expensive to people, leading to poor sales. The Silver-Elephant company was also in serious trouble with financing. Meanwhile, the institute gave a great deal of trust and support, rejecting the requests to sell the intellectual property from many domestic and foreign companies. Moreover, nisin appeared to have a limited scope of application during the development. However, with the institute inviting experts of other institutions to participate in the application tests of nisin, the application of nisin in low temperature meat products, dairy products, and canned food was accelerated. Eventually, many well-known food companies such as Shuanghui, Wahaha, Sanyuan, and Huiyuan, became fixed clients.</Para>
            <Para ID="Par6">The success of research and development of nisin was attributed to a hard-working team and the fruits of their labour resulted from the effort of several generations of scientists. Prof. Yugu Xue, the pioneering and leading scientist at nearly seventy-years old, continued to make outstanding contributions to the project and Associate Prof. Xiuzhu Chen (Fig. <InternalRef RefID="Fig3">3</InternalRef>) devoted many hours of her free-time to lead the staffs from the Silver-Elephant company to solve production problems. After untold hardships, the fermentation titer was increased from 3000 units to over 8000 units per milliliter. The work reached the leading level at home and abroad and made a great step forward for the research and development of nisin. Besides, many graduate students also contributed to the project. For example, Ying Cai was involved in the establishment of method to directly screen the producing strain of nisin; Haiqing Hu cloned the complete gene cluster of nisin; Jing Yuan constructed nisin mutants with broader antimicrobial spectra (Yuan et al., <CitationRef CitationID="CR2">2004</CitationRef>), and so on. The related work on nisin has been published in more than 30 research papers and obtained 5 authorized national invention patents.</Para>
            <Para ID="Par7">The research and development of nisin is still going on in IMCAS. On the basis of previous work, the group led by Prof. Jin Zhong (Fig. <InternalRef RefID="Fig3">3</InternalRef>) has clarified the structure-activity relationships, the mode of action, and the resistance mechanism of nisin (Sun et al., <CitationRef CitationID="CR3">2009</CitationRef>). They also have worked on the resolution of the biosynthetic machinery, especially the self-induction molecular mechanism (Teng et al., <CitationRef CitationID="CR4">2014</CitationRef>). Additionally, they have exploited many potential lantibiotics from microbial resources or genomic resources. All these works will benefit the wide application of nisin-like active substances in food, medicine, and other fields.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibUnstructured>Huan LD, Chen XZ, Cai Y, Zhuang ZH, Xue YG (1997) Directional screening of nisin-producing <Emphasis Type="Italic">Lactococcus lactis</Emphasis> and identification of its product. Acta Microbiol Sin 37:292–300. (还连栋, 陈秀珠, 才迎, 庄增辉, 薛禹谷. (1997). 乳链菌肽产生菌的定向筛选及发酵产物的鉴定. 微生物学报, 37, 292–300)</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>Z</Initials>
                    <FamilyName>Sun</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Zhong</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>XB</Initials>
                    <FamilyName>Liang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JL</Initials>
                    <FamilyName>Liu</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>XZ</Initials>
                    <FamilyName>Chen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>LD</Initials>
                    <FamilyName>Huan</FamilyName>
                  </BibAuthorName>
                  <Year>2009</Year>
                  <ArticleTitle xml:lang="en" Language="En">Novel mechanism for nisin resistance via proteolytic degradation of nisin by the nisin resistance protein NSR</ArticleTitle>
                  <JournalTitle>Antimicrob Agents Chemother</JournalTitle>
                  <VolumeID>53</VolumeID>
                  <FirstPage>1964</FirstPage>
                  <LastPage>1973</LastPage>
                  <Occurrence Type="PMCID">
                    <Handle>2681560</Handle>
                  </Occurrence>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD1MXlvVGgtbY%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1128/AAC.01382-08</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>19273681</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Sun Z, Zhong J, Liang XB, Liu JL, Chen XZ, Huan LD (2009) Novel mechanism for nisin resistance via proteolytic degradation of nisin by the nisin resistance protein NSR. Antimicrob Agents Chemother 53:1964–1973</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>KL</Initials>
                    <FamilyName>Teng</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Zhang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>X</Initials>
                    <FamilyName>Zhang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>XX</Initials>
                    <FamilyName>Ge</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Gao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Wang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>YH</Initials>
                    <FamilyName>Lin</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Zhong</FamilyName>
                  </BibAuthorName>
                  <Year>2014</Year>
                  <ArticleTitle xml:lang="en" Language="En">Identification of ligand specificity determinants in lantibiotic bovicin HJ50 and the receptor BovK, a multitransmembrane histidine kinase</ArticleTitle>
                  <JournalTitle>J Biol Chem</JournalTitle>
                  <VolumeID>289</VolumeID>
                  <FirstPage>9823</FirstPage>
                  <LastPage>9832</LastPage>
                  <Occurrence Type="PMCID">
                    <Handle>3975027</Handle>
                  </Occurrence>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BC2cXls1Wkt7s%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1074/jbc.M113.513150</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>24526683</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Teng KL, Zhang J, Zhang X, Ge XX, Gao Y, Wang J, Lin YH, Zhong J (2014) Identification of ligand specificity determinants in lantibiotic bovicin HJ50 and the receptor BovK, a multitransmembrane histidine kinase. J Biol Chem 289:9823–9832</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Yuan</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>ZZ</Initials>
                    <FamilyName>Zhang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>XZ</Initials>
                    <FamilyName>Chen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>W</Initials>
                    <FamilyName>Yang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>LD</Initials>
                    <FamilyName>Huan</FamilyName>
                  </BibAuthorName>
                  <Year>2004</Year>
                  <ArticleTitle xml:lang="en" Language="En">Site-directed mutagenesis of the hinge region of nisin Z and properties of nisin Z mutants</ArticleTitle>
                  <JournalTitle>Appl Microbiol Biotechnol</JournalTitle>
                  <VolumeID>64</VolumeID>
                  <FirstPage>806</FirstPage>
                  <LastPage>815</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2cXktlyrsr0%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s00253-004-1599-1</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>15048591</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Yuan J, Zhang ZZ, Chen XZ, Yang W, Huan LD (2004) Site-directed mutagenesis of the hinge region of nisin Z and properties of nisin Z mutants. Appl Microbiol Biotechnol 64:806–815</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>15915.zip</EditorialManuscriptNumber>
            </JobSheet></Publisher>


