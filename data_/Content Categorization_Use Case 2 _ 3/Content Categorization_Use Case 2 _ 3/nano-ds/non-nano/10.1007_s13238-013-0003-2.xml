<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-013-0003-2">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>3</ArticleID>
            <ArticleDOI>10.1007/s13238-013-0003-2</ArticleDOI>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Cells derived from iPSC can be immunogenic — Yes or No?</ArticleTitle>
            <ArticleCategory>Discussion</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>3</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2013</Year>
                <Month>12</Month>
                <Day>16</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2014</Year>
                <Month>1</Month>
                <Day>30</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>The Author(s)</CopyrightHolderName>
              <CopyrightYear>2014</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Jiani</GivenName>
                  <FamilyName>Cao</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Xiaoyan</GivenName>
                  <FamilyName>Li</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Xiao</GivenName>
                  <FamilyName>Lu</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Chao</GivenName>
                  <FamilyName>Zhang</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Honghao</GivenName>
                  <FamilyName>Yu</FamilyName>
                </AuthorName>
              </Author>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Tongbiao</GivenName>
                  <FamilyName>Zhao</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>tbzhao@ioz.ac.cn</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>State Key Laboratory of Reproductive Biology, Institute of Zoology</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <Abstract xml:lang="en" OutputMedium="All" Language="En" ID="Abs1">
              <Heading>Abstract</Heading>
              <Para>The induced pluripotent stem cells (iPSCs), derived by ectopic expression of reprogramming factors in somatic cells, can potentially provide unlimited autologous cells for regenerative medicine. In theory, the autologous cells derived from patient iPSCs should be immune tolerant by the host without any immune rejections. However, our recent studies have found that even syngeneic iPSC-derived cells can be immunogenic in syngeneic hosts by using a teratoma transplantation model (Nature 474:212–215, <CitationRef CitationID="CR13">2011</CitationRef>). Recently two research groups differentiated the iPSCs into different germ layers or cells, transplanted those cells to the syngeneic hosts, and evaluated the immunogenicity of those cells. Both of the two studies support our conclusions that some certain but not all tissues derived from iPSCs can be immunogenic, although they claimed either “negligible” or “lack of” immunogenicity in iPSC derivatives (Nature 494:100–104, <CitationRef CitationID="CR1">2013</CitationRef>; Cell Stem Cell 12:407–412, <CitationRef CitationID="CR5">2013</CitationRef>). To test the immunogenicity of clinically valuable cells differentiated from human iPSCs are emergently required for translation of iPSC technology to clinics.</Para>
            </Abstract>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Teratoma</Keyword>
              <Keyword>Pluripotent Stem Cell</Keyword>
              <Keyword>iPSC</Keyword>
              <Keyword>Autologous Cell</Keyword>
              <Keyword>Immune Rejection</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>The embryonic stem cells (ESCs), characterized by the capability to both self-renew and differentiate into each cell type, hold great promise for regenerative medicine. However, the ethical concern and immune rejection are the two major hurdles for clinical development of ESCs. The induced pluripotent stem cell (iPSC), developed by Yamanaka group in 2006 through ectopic expression of four reprogramming factors in terminally differentiated somatic cells (Takahashi and Yamanaka, <CitationRef CitationID="CR10">2006</CitationRef>), is believed to be able to get cross those barriers. This breakthrough discovery has greatly reshaped the scientific and political landscapes of stem cell biology. It provides an unprecedented opportunity to model human disease, re-understand the basic biology such as development and differentiation, identify new therapeutic targets and test new therapies etc. Most importantly, the iPSC can potentially provide autologous cell resources in regenerative medicine without concerning the immune rejections led by major histocompatibility complex restriction. It raised the hope that patient-specific iPSCs could become a renewable source of autologous cells for human therapy. Whereas it has been widely predicted that the autologous cells derived from patient-specific iPSCs are immune tolerant in that patient, the immunogenicity of cells derived from iPSC has not been widely examined since the discovery of the iPSCs.</Para>
            <Para>By using a teratoma formation model, we unexpectedly found that some but not all cells derived from mouse iPSC can be immunogenic (Zhao et al., <CitationRef CitationID="CR13">2011</CitationRef>), and the immune rejection response was T cell dependent, supported by the immune rejection was totally blocked in Rag knock-out recipients. Furthermore, in our system we identified two genes (<Emphasis Type="Italic">Hormad1</Emphasis> and <Emphasis Type="Italic">Zg16</Emphasis>), which were abnormally expressed in iPSC-teratoma but not in ES-teratoma, directly contribute to the immunogenicity of iPSC derivatives, supporting by the existence of primed T cells in the mice harboring the iPSC derived teratomas.</Para>
            <Para>The question whether iPSC derivatives are immnogeneic or not is straight forward; however, the answer to this question is very complicated due to the developmental randomness of iPSC and the nondeterminacy of the abnormal expression of the minor antigens. The first hurdle to resolve this question is the hardness to differentiate iPSC into each tissue of our body and test their immunogenicity one by one. The iPSC can produce any kind of tissues in a whole body theoretically, however the directed differentiation of iPSC is still hindered by the low efficiency and it is still impossible to differentiate iPSC into each tissue we need at current stage. The original idea to use teratoma as a model system to test iPSC immunogenicity is that teratoma contains many kinds of tissues differentiated from pluripotent stem cells and it has been successfully used to test the immunogenicity of ES derivatives (Koch et al., <CitationRef CitationID="CR7">2008</CitationRef>). A teratoma is an encapsulated benign tumor harboring many kinds of tissue or organ components resembling normal derivatives of all three germ layers (Tapper and Lack, <CitationRef CitationID="CR11">1983</CitationRef>; Chi et al., <CitationRef CitationID="CR2">1984</CitationRef>), which provide a possibility to probe the immunogenicity of iPSC derived tissues as much as possible in a time. Recently, Dr. Abe’s group studied the immunogenicity of different tissues derived from iPSC including skin cells, bone marrow cells and cardiomyocytes and found that iPSC derived cardiomyocytes <Emphasis Type="Italic">in vitro</Emphasis> but not skin and bone marrow cells are highly immunogenic (For further details, please refer to Sup Fig. 13 in Araki et al., <CitationRef CitationID="CR1">2013</CitationRef>). It is possible that the cardiomyocytes, but not skin and bone marrow cells, harbor the abnormal expressed minor antigens which contribute to the immunogenicity. Another concern is that using the bone marrow transplantation to study the intrinsic immunogenicity of iPCS derivatives is inappropriate, because the hematopoietic stem cells inside the bone marrow can itself develop into different linear of immune cells including regulatory T cells leading to immune tolerance of the graft. In clinic, the pre-transplantation of donor hematopoietic stem cell into the recipients before transplanting the designed organs is routinely used to induce tolerance of allografting. Meanwhile, by differentiation of iPSC into endothelial cells, hepatocytes and neuronal cells, Boyd group studied the immunogenicity of <Emphasis Type="Italic">in vitro</Emphasis> differentiated iPSC derivatives (Guha et al., <CitationRef CitationID="CR5">2013</CitationRef>). Although compared to the counterparts of allografts, the immune rejection response led by syngeneic iPSC descendents was mild, however, the endothelial cells derived from iPSCs showed higher apoptosis rate than syngeneic ES derivatives when cocultured with T cell isolated from endothelial cell experienced syngeneic mice, indicating the iPSC derived endothelial cells can induce immune rejection response (For further details, please refer to Fig. 2E in Guha et al., <CitationRef CitationID="CR5">2013</CitationRef>).</Para>
            <Para>The immunogenicity of iPSC derivatives is very complicated (Fig. <InternalRef RefID="Fig1">1</InternalRef>). It is widely accepted that reprogramming itself can induce both genetic and epigenetic defects in iPSCs (Doi et al., <CitationRef CitationID="CR3">2009</CitationRef>; Kim et al., <CitationRef CitationID="CR6">2010</CitationRef>; Polo et al., <CitationRef CitationID="CR9">2010</CitationRef>; Zhao and Xu, <CitationRef CitationID="CR12">2010</CitationRef>; Gore et al., <CitationRef CitationID="CR4">2011</CitationRef>; Lister et al., <CitationRef CitationID="CR8">2011</CitationRef>). It is possible that those defects can directly or indirectly contribute to the immunogenicity of iPSC derivatives. In supporting of this idea, we have identified a couple of genes overexpressed in iPSC-teratomas, with two genes were confirmed as direct antigens contribute to the immunogenicity of iPSC derivatives. This indicates that not all genes with abnormal expression during differentiation will contribute to the immune rejection responses after transplantation. On the other hand, it is very hard to link the abnormal antigen expression identified in teratomas to an exact cell type, due to the heterogenic structure of teratomas. And the complexity of developmental process makes it difficult to predict which tissue will inherit and present the defects induced by reprogramming during iPSC development and differentiation. So it is not surprising that both Abe and Boyd groups did not detect abnormal gene expression in a specific cell types. A possible solution to this question is to profile the immunogenicity of different tissues as much as possible. For those immunogenic iPSC derived specific tissues, gene expression profiling can be employed to probe the minor antigens contributed to the immunogenicity.<Figure ID="Fig1" Float="Yes" Category="Standard">
                <Caption xml:lang="en" Language="En">
                  <CaptionNumber>Figure 1</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>Differential immunogenicity of iPSC derivatives. The genetic and epigenetic defects induced by reprogramming are differentially presented during the iPSC differentiation leading to different consequences. The iPSC descendents without presentation of the defects during differentiation are not immunogenic. Tissues with pronounced defects are immunogenic, but may have distinct destiny</SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject>
                  <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-013-0003-2/MediaObjects/13238_2013_3_Fig1_HTML.gif"/>
                </MediaObject>
              </Figure></Para>
            <Para>In general, compared to the allograft, the immunogenicity of autologous iPSC derivatives is much weaker, supporting not only by the high teratoma formation rate by B6 iPSCs than allogeneic 129 ES cells in the B6 mice, but also the fact that T cells are only locally but not ubiquitously infiltrated into the teratomas formed by iPSC in B6 mice.</Para>
            <Para>It is noteworthy however, if one certain cell linear differentiated from iPSC expresses immunogenic minor antigens ubiquitously, it still can elicit serious rejection responses, leading to the complete rejection of that tissue (Fig. <InternalRef RefID="Fig1">1</InternalRef>). Which kind of tissues differentiated from iPSCs can be immunogenic and whether they are destined is still an important opening question. Furthermore, it is critical to evaluate the immunogenicity of clinical valuable cells derived from human iPSCs, which can significantly promote the translation of iPSC technology from bench to bedsides.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Acknowledgments>
              <Heading>Footnotes</Heading>
              <SimplePara>This study was supported by grants from the National Basic Research Program (973 Program) (No. 2012CB966900), the Strategic Priority Research Program of the Chinese Academy of Sciences XDA01040108, the National Natural Science Foundation of China (Grant No. 31271592), and the National Thousand Young Talents Program to T.Z.</SimplePara>
              <SimplePara>Jiani Cao, Xiaoyan Li, Xiao Lu, Chao Zhang, Honghao Yu, and Tongbiao Zhao declare that they have no conflict of interest.</SimplePara>
              <SimplePara>This article does not contain any studies with human or animalsubjects performed by the any of the authors</SimplePara>
              <FormalPara RenderingStyle="Style1">
                <Heading>Open Access</Heading>
                <Para>This article is distributed under the terms of the Creative Commons Attribution License which permits any use, distribution, and reproduction in any medium, provided the original author(s) and the source are credited.</Para>
              </FormalPara>
            </Acknowledgments>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Araki</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Uda</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Hoki</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Sunayama</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Nakamura</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2013</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>494</VolumeID>
                  <FirstPage>100</FirstPage>
                  <LastPage>104</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature11807</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3sXhtFSnt70%3D, 23302801, 10.1038/nature11807</BibComments>
                </BibArticle>
                <BibUnstructured>Araki R, Uda M, Hoki Y, Sunayama M, Nakamura M et al (2013) Nature 494:100–104</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>JG</Initials>
                    <FamilyName>Chi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>YS</Initials>
                    <FamilyName>Lee</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>YS</Initials>
                    <FamilyName>Park</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>KY</Initials>
                    <FamilyName>Chang</FamilyName>
                  </BibAuthorName>
                  <Year>1984</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Am J Clin Pathol</JournalTitle>
                  <VolumeID>82</VolumeID>
                  <FirstPage>115</FirstPage>
                  <LastPage>119</LastPage>
                  <BibComments>1:STN:280:DyaL2c3ltlKhsA%3D%3D, 6540049</BibComments>
                </BibArticle>
                <BibUnstructured>Chi JG, Lee YS, Park YS, Chang KY (1984) Am J Clin Pathol 82:115–119</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Doi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>IH</Initials>
                    <FamilyName>Park</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>B</Initials>
                    <FamilyName>Wen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Murakami</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>MJ</Initials>
                    <FamilyName>Aryee</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2009</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nat Genet</JournalTitle>
                  <VolumeID>41</VolumeID>
                  <FirstPage>1350</FirstPage>
                  <LastPage>1353</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/ng.471</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BD1MXhtlers7rM, 2958040, 19881528, 10.1038/ng.471</BibComments>
                </BibArticle>
                <BibUnstructured>Doi A, Park IH, Wen B, Murakami P, Aryee MJ et al (2009) Nat Genet 41:1350–1353</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Gore</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Z</Initials>
                    <FamilyName>Li</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>HL</Initials>
                    <FamilyName>Fung</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JE</Initials>
                    <FamilyName>Young</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Agarwal</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2011</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>471</VolumeID>
                  <FirstPage>63</FirstPage>
                  <LastPage>67</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature09805</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3MXis1CrsLg%3D, 3074107, 21368825, 10.1038/nature09805</BibComments>
                </BibArticle>
                <BibUnstructured>Gore A, Li Z, Fung HL, Young JE, Agarwal S et al (2011) Nature 471:63–67</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Guha</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JW</Initials>
                    <FamilyName>Morgan</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>G</Initials>
                    <FamilyName>Mostoslavsky</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>NP</Initials>
                    <FamilyName>Rodrigues</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>AS</Initials>
                    <FamilyName>Boyd</FamilyName>
                  </BibAuthorName>
                  <Year>2013</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Cell Stem Cell</JournalTitle>
                  <VolumeID>12</VolumeID>
                  <FirstPage>407</FirstPage>
                  <LastPage>412</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.stem.2013.01.006</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3sXhsFGjsbw%3D, 23352605, 10.1016/j.stem.2013.01.006</BibComments>
                </BibArticle>
                <BibUnstructured>Guha P, Morgan JW, Mostoslavsky G, Rodrigues NP, Boyd AS (2013) Cell Stem Cell 12:407–412</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>K</Initials>
                    <FamilyName>Kim</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Doi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>B</Initials>
                    <FamilyName>Wen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>K</Initials>
                    <FamilyName>Ng</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2010</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>467</VolumeID>
                  <FirstPage>285</FirstPage>
                  <LastPage>290</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature09342</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3cXhtFOht7rJ, 3150836, 20644535, 10.1038/nature09342</BibComments>
                </BibArticle>
                <BibUnstructured>Kim K, Doi A, Wen B, Ng K, Zhao R et al (2010) Nature 467:285–290</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>CA</Initials>
                    <FamilyName>Koch</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Geraldes</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JL</Initials>
                    <FamilyName>Platt</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Stem Cells</JournalTitle>
                  <VolumeID>26</VolumeID>
                  <FirstPage>89</FirstPage>
                  <LastPage>98</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1634/stemcells.2007-0151</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BD1cXhvVCktL4%3D, 17962705, 10.1634/stemcells.2007-0151</BibComments>
                </BibArticle>
                <BibUnstructured>Koch CA, Geraldes P, Platt JL (2008) Stem Cells 26:89–98</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Lister</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Pelizzola</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>YS</Initials>
                    <FamilyName>Kida</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>RD</Initials>
                    <FamilyName>Hawkins</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JR</Initials>
                    <FamilyName>Nery</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2011</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>471</VolumeID>
                  <FirstPage>68</FirstPage>
                  <LastPage>73</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature09798</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3MXhsVWjt78%3D, 3100360, 21289626, 10.1038/nature09798</BibComments>
                </BibArticle>
                <BibUnstructured>Lister R, Pelizzola M, Kida YS, Hawkins RD, Nery JR et al (2011) Nature 471:68–73</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>JM</Initials>
                    <FamilyName>Polo</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Liu</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>ME</Initials>
                    <FamilyName>Figueroa</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>W</Initials>
                    <FamilyName>Kulalert</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Eminli</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2010</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nat Biotechnol</JournalTitle>
                  <VolumeID>28</VolumeID>
                  <FirstPage>848</FirstPage>
                  <LastPage>855</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nbt.1667</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3cXovFOns7c%3D, 3148605, 20644536, 10.1038/nbt.1667</BibComments>
                </BibArticle>
                <BibUnstructured>Polo JM, Liu S, Figueroa ME, Kulalert W, Eminli S et al (2010) Nat Biotechnol 28:848–855</BibUnstructured>
              </Citation>
              <Citation ID="CR10">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>K</Initials>
                    <FamilyName>Takahashi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Yamanaka</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Cell</JournalTitle>
                  <VolumeID>126</VolumeID>
                  <FirstPage>663</FirstPage>
                  <LastPage>676</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.cell.2006.07.024</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BD28Xpt1aktbs%3D, 16904174, 10.1016/j.cell.2006.07.024</BibComments>
                </BibArticle>
                <BibUnstructured>Takahashi K, Yamanaka S (2006) Cell 126:663–676</BibUnstructured>
              </Citation>
              <Citation ID="CR11">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Tapper</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>EE</Initials>
                    <FamilyName>Lack</FamilyName>
                  </BibAuthorName>
                  <Year>1983</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Ann Surg</JournalTitle>
                  <VolumeID>198</VolumeID>
                  <FirstPage>398</FirstPage>
                  <LastPage>410</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1097/00000658-198309000-00016</Handle>
                  </Occurrence>
                  <BibComments>1:STN:280:DyaL3szgslKhsA%3D%3D, 1353316, 6684416, 10.1097/00000658-198309000-00016</BibComments>
                </BibArticle>
                <BibUnstructured>Tapper D, Lack EE (1983) Ann Surg 198:398–410</BibUnstructured>
              </Citation>
              <Citation ID="CR12">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Xu</FamilyName>
                  </BibAuthorName>
                  <Year>2010</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Trends Cell Biol</JournalTitle>
                  <VolumeID>20</VolumeID>
                  <FirstPage>170</FirstPage>
                  <LastPage>175</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.tcb.2009.12.004</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3cXisFemu7o%3D, 20061153, 10.1016/j.tcb.2009.12.004</BibComments>
                </BibArticle>
                <BibUnstructured>Zhao T, Xu Y (2010) Trends Cell Biol 20:170–175</BibUnstructured>
              </Citation>
              <Citation ID="CR13">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>ZN</Initials>
                    <FamilyName>Zhang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Z</Initials>
                    <FamilyName>Rong</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>Y</Initials>
                    <FamilyName>Xu</FamilyName>
                  </BibAuthorName>
                  <Year>2011</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>474</VolumeID>
                  <FirstPage>212</FirstPage>
                  <LastPage>215</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/nature10135</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BC3MXntFKktLo%3D, 21572395, 10.1038/nature10135</BibComments>
                </BibArticle>
                <BibUnstructured>Zhao T, Zhang ZN, Rong Z, Xu Y (2011) Nature 474:212–215</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>13909</EditorialManuscriptNumber>
            </JobSheet></Publisher>


