<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-011-1067-5">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>1067</ArticleID>
            <ArticleDOI>10.1007/s13238-011-1067-5</ArticleDOI>
            <ArticleSequenceNumber>2</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" Language="En">Dr. Chen-lu Tsou: a tireless advocate for advancement in the standards of scientific research in China</ArticleTitle>
            <ArticleCategory>Recollection</ArticleCategory>
            <ArticleFirstPage>435</ArticleFirstPage>
            <ArticleLastPage>436</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2011</Year>
                <Month>7</Month>
                <Day>11</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2011</Year>
                <Month>7</Month>
                <Day>12</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Higher Education Press and Springer-Verlag Berlin Heidelberg</CopyrightHolderName>
              <CopyrightYear>2011</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="Regular">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Chih-chen</GivenName>
                  <FamilyName>Wang</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1 Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Zhi-Xin</GivenName>
                  <FamilyName>Wang</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff3">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Baoyuan</GivenName>
                  <FamilyName>Zhang</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff3">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Ming</GivenName>
                  <FamilyName>Li</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Institute of Biophysics</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.12527.33</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000106623178</OrgID>
                <OrgDivision>School of Life Sciences</OrgDivision>
                <OrgName>Tsinghua University</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100084</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff3">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Beijing Institutes of Life Science</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Chinese Scientist</Keyword>
              <Keyword>Academic Standard</Keyword>
              <Keyword>Modern Biology</Keyword>
              <Keyword>Academic Ethic</Keyword>
              <Keyword>Domestic Journal</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="PDF">
            <page id="pg01">Protein Cell 2011, 2(6): 435–436 DOI 10.1007/s13238-011-1067-5 Protein &amp; Cell RECOLLECTION Dr. Chen-lu Tsou: a tireless advocate for advancement in the standards of scientiﬁc research in China Chih-chen Wang 1, Zhi-Xin Wang 1,2, Baoyuan Zhang 3, Ming Li 3 1 Institute of Biophysics, Chinese Academy of Sciences, Beijing 100101, China 2 School of Life Sciences, Tsinghua University, Beijing 100084, China 3 Beijing Institutes of Life Science, Chinese Academy of Sciences, Beijing 100101, China The history of modern science in China is relatively short compared to that in western countries. For example, it was not until the 1920s that genetics and evolutionary theory were introduced to China. Most of the earliest Chinese biologists were trained overseas and returned to their beloved home- land to become pioneers of biomedical disciplines like zoology, plant biology, physiology, and neuroscience in China. Those who returned in the 1940s focused more on biochemistry, and after “Reform and Opening” in the late 1970s, molecular biology gradually thrived. With the contin- uous efforts of numerous Chinese biologists, modern biology started to ﬂourish steadily despite a history of wars and political turbulence. However, even though Chinese biologists trained overseas brought back new knowledge and perspec- tives to China, the overall system of modern biology in China was still young, and many issues in scientiﬁc practice started to appear as time went by. One of them was lack of high academic standards. For years, the lack of academic standards, especially the lack of scientiﬁc writing standards, had been ignored by many Chinese scientists. It is commonly accepted that scientists must not only “do” science, but also “write” science. “Writing” science allows original scientiﬁc research to be reviewed by peer scientists, and once approved, to be added to the existing database of scientiﬁc knowledge. Global visibility and acces- sibility of scientiﬁc work is particularly important for the basic sciences. For science conducted in China, what this means is that they should be published in English—the universal scientiﬁc language—in internationally distributed journals. However, the society of Chinese biologists was once a “closed box” and there was little intellectual exchange with western countries. Most research ﬁndings were published in Chinese in scientiﬁc journals in China. No matter how novel and important the work, the international scientiﬁc community did not get the chance to learn about, appreciate, and review them. Addition- ally, many scientists and young students never received formal professional training on scientiﬁc writing standards, which were strictly followed in western scientiﬁc societies. This situation continued for years until a Chinese biochemist, Dr. Chen-lu Tsou, spoke out and advocated applying internation- ally approved scientiﬁc writing standards in China. Born in 1923, Dr. Chen-lu Tsou, received a Ph.D. degree at the University of Cambridge, UK, in 1951. He worked on protein functions during his entire academic career and pioneered the structure-function study of enzymes using partial proteolysis. He was also one of the few Chinese biologists who had published in Nature in the early years. As early as the 1980s, Dr. Tsou started to publicly advocate scientiﬁc writing standards. He believed that Chinese scientists must publish their basic research results in Dr. Chen-lu Tsou (1923–2006) © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011 435</page>
            <page id="pg02">Protein &amp; Cell Chih-chen Wang et al. internationally renowned peer-reviewed journals to make China’s scientiﬁc research known to the world. Furthermore, Chinese scientists have to follow international standards of scientiﬁc publishing and change their unethical conducts to gain respect in the international scientiﬁc community. For example, in order to attract more papers, many domestic journals often encouraged authors to submit their previous publications with minor modiﬁcations which is known as “multiple submission” and strictly forbidden in the West. In addition, many authors often incorrectly claimed to be the ﬁrst to ﬁnd something without adequate literature research in the ﬁeld. This not only showed disregard and disrespect towards the achievements of predecessors, but also led them to be ridiculed for ignorance by international colleagues. Further- more, some supervisors signed their name on articles written by their students and sent them for publication without spending time and effort in ensuring proper quality. They neglected the fact that signing means responsibility, and advisors must be responsible for any omissions or errors in the articles they are co-authors in. Additionally, some researchers directly quoted others’ results with the wrong interpretation and non-standard conversion of physical units. Due to such problems, Dr. Tsou suggested that we should change these attitudes in principal investigators (PIs) and promote academic writing standards to graduate students to lay a solid foundation for their research career. Dr. Tsou actively promoted these ideas not only to his peers but also to his students when he taught at the Graduate School of Chinese Academy of Sciences (CAS). Dr. Tsou also insisted that Chinese journals should become more international, and this was as important as publishing manuscripts in internationally renowned journals. On one hand, China should publish more English journals indepen- dently, which is the ﬁrst step to internationalization. On the other hand, it is necessary to apply internationally approved scientiﬁc writing standards and the peer-review process to journals published in China. In this way, China&apos;s scientiﬁc research and Chinese scientiﬁc journals can both get more attention from international scientists, which will in turn lead to the promotion of high academic standards in China. In the past twenty years, China has made signiﬁcant progress in these two fronts. The importance of high academic writing standards has been widely acknowledged in various scientiﬁc and technological ﬁelds in China. Increasingly more Chinese scientists have published high- quality papers in international academic journals. According to some recent statistics, the total number of scientiﬁc publications from China sky-rocketed and ranked No. 2 in the world in 2010, and the total citation number of scientiﬁc publications moved up to No.8 in the world. In addition, the inﬂuence of international English journals published in China has been growing. Most if not all of these positive changes can be ascribed one way or another to Dr. Tsou’s tireless advocacy of high scientiﬁc standards in China. His continued efforts won the respect of many researchers, and he has no doubt played a vital role in creating a favourable academic environment for scientiﬁc research in China. Dr. Tsou passed away in 2006, but his contribution has left a profound impression on young people of every generation. His articles on academic ethics and norms have been included in The Selection of Excellent Tutor Experience in the Graduate School of the Chinese Academy of Sciences, and have always been viewed as the guideline for all the CAS graduates. Dr. Tsou’s contribution to pushing China’s academic standards several notches higher will be remem- bered forever. We are soberly aware of the gap between China and developed countries in science and technology, and there is a long way ahead to strive and persist in. 436 © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011</page>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


