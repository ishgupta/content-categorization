<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Combined">
        
        <Article OutputMedium="All" ID="s40090-014-0025-5">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>25</ArticleID>
            <ArticleDOI>10.1007/s40090-014-0025-5</ArticleDOI>
            <ArticleSequenceNumber>6</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Evaluation of sodium isobutyl xanthate as a collector in the froth flotation of a carbonatitic copper ore</ArticleTitle>
            <ArticleCategory>Research</ArticleCategory>
            <ArticleFirstPage>107</ArticleFirstPage>
            <ArticleLastPage>110</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2014</Year>
                <Month>10</Month>
                <Day>15</Day>
              </RegistrationDate>
              <Received>
                <Year>2014</Year>
                <Month>2</Month>
                <Day>22</Day>
              </Received>
              <Accepted>
                <Year>2014</Year>
                <Month>10</Month>
                <Day>14</Day>
              </Accepted>
              <OnlineDate>
                <Year>2014</Year>
                <Month>10</Month>
                <Day>25</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>The Author(s)</CopyrightHolderName>
              <CopyrightYear>2014</CopyrightYear>
              <License Version="4.0" Type="OpenAccess" SubType="CC BY">
                <SimplePara>This article is published under license to BioMed Central Ltd. <Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution License which permits any use, distribution, and reproduction in any medium, provided the original author(s) and the source are credited.</SimplePara>
              </License>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>N.</GivenName>
                  <GivenName>T.</GivenName>
                  <GivenName>N.</GivenName>
                  <FamilyName>Langa</FamilyName>
                </AuthorName>
              </Author>
              <Author CorrespondingAffiliationID="Aff2" AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>A.</GivenName>
                  <GivenName>A.</GivenName>
                  <FamilyName>Adeleke</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>adeadeleke@oauife.edu.ng</Email>
                </Contact>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>P.</GivenName>
                  <FamilyName>Mendonidis</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>C.</GivenName>
                  <GivenName>K.</GivenName>
                  <FamilyName>Thubakgale</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.442351.5</OrgID>
                <OrgDivision>Department of Metallurgical Engineering</OrgDivision>
                <OrgName>Vaal University of Technology</OrgName>
                <OrgAddress>
                  <City>Vanderbijlpark</City>
                  <Country Code="ZA">South Africa</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.10824.3f</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000121839444</OrgID>
                <OrgDivision>Department of Materials Science and Engineering</OrgDivision>
                <OrgName>Obafemi Awolowo University</OrgName>
                <OrgAddress>
                  <City>Ile-Ife</City>
                  <Country Code="NG">Nigeria</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <Abstract xml:lang="en" OutputMedium="All" Language="En" ID="Abs1">
              <Heading>Abstract</Heading>
              <Para>The dosage of a collector is an important factor that determines the efficiency of a froth flotation process. The representative sample of the carbonatitic Palabora copper ore ground 45 % passing 75 µm was froth-floated with sodium isobutyl xanthate (SIBX) collector at 60, 70, 80, 90 and 100 g/t dosages. The concentrates and tails were dried and analysed. The results obtained showed that the highest recovery of 85.18 % with a grade of 12.61 % was obtained at the 80 g/t dosage. It was further observed that the copper grade of the concentrate generally decreased with increasing recovery. The particle size distribution analysis of the tailings showed that the froth flotation was most efficient in the fine particle size range 38–75 µm.</Para>
            </Abstract>
            <KeywordGroup xml:lang="en" OutputMedium="All" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Copper ore</Keyword>
              <Keyword>Collector</Keyword>
              <Keyword>Dosage</Keyword>
              <Keyword>Recovery</Keyword>
              <Keyword>Grade</Keyword>
              <Keyword>Tailings</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Section1 Type="Introduction" ID="Sec1">
              <Heading>Introduction</Heading>
              <Para>The Palabora mine carbonatitic complex is the largest producer of copper in South Africa. The mine is also a major source of vermiculite, baddeleyite, nickel sulphate, uranium and phosphates. The carbonatite complex is the only one known in the world that contains sufficient copper as sulphides to qualify as an economic deposit. It is a magnetite–copper deposit hosted within a pyroxenite–carbonatite complex with minor constituent of gold [<CitationRef CitationID="CR3">3</CitationRef>, <CitationRef CitationID="CR4">4</CitationRef>, <CitationRef CitationID="CR6">6</CitationRef>].</Para>
              <Para>Flotation is an essential and resourceful technique in the mineral processing industry. The technique is based on the differences in the surface properties of different minerals, which cause them to have affinity either for water or for air bubbles. The flotation method works due to the action of reagents such as collectors, frothers and depressants. Collectors are organic compounds which render selected minerals water-repellent by adsorption of molecules or ions on to the mineral surface, reducing the stability of the hydrated layer separating the mineral surface from the air bubble to such a level that attachment of the particle to the bubble can be made on contact. Collectors make the minerals hydrophobic, thus allowing them to attach to the air bubbles and rise to the surface, frothers create froth on the pulp surface, which is easily recovered and the depressants cause the naturally floating gangue to become hydrophilic and thus remain in the cell as tailings [<CitationRef CitationID="CR8">8</CitationRef>].</Para>
              <Para>Standard copper sulphide flotation collector reagents are sulphur-based thiol class collectors, which can be grouped into the main xanthate, dithiophosphate, thionocarbamate and thiocarbamate families [<CitationRef CitationID="CR8">8</CitationRef>]. The amount of reagent required for a particular application is influenced by factors like variations in particular size, mass of particle, quantity of mineral, and the character of the host rock. Xanthates are ionising anionic sulfhydryl collectors that are powerful and selective in the flotation of sulphide minerals [<CitationRef CitationID="CR8">8</CitationRef>]. They adsorb on to the sulphide mineral surface and form insoluble metal xanthates which are very hydrophobic. The four common xanthate collectors are sodium isobutyl, sodium ethyl, sodium normal propyl and potassium amyl xanthate [<CitationRef CitationID="CR7">7</CitationRef>].</Para>
              <Para>In this research, the optimal dosage of SIBX collector for the froth flotation of Palabora ore was determined to ensure the efficient froth flotation of the ore with this collector.</Para>
            </Section1>
            <Section1 Type="MaterialsAndMethods" ID="Sec2">
              <Heading>Materials and methods</Heading>
              <Section2 Type="MaterialsAndMethods" ID="Sec3">
                <Heading>Materials</Heading>
                <Section3 ID="Sec4">
                  <Heading>Sample collection</Heading>
                  <Para>About 150 kg of Palabora copper ore was collected from the Palabora Mining Company. From the bulk sample, about 20 kg was taken for this study.</Para>
                </Section3>
                <Section3 ID="Sec5">
                  <Heading>Sample preparation</Heading>
                  <Para>The ore received from Palabora Mining Company comprised various lumps of rocks to fine material. The bulk sample was crushed in a jaw crusher and various size fractions were obtained ranging from −2 to +4 mm. The various size fractions were screened using the 2 and 4 mm sieves. Three size fractions &gt;4 mm, &lt;4 mm &gt;2 mm and &lt;2 mm were obtained. A representative mixture was then obtained from these fractions by coning and quartering.</Para>
                </Section3>
                <Section3 ID="Sec6">
                  <Heading>The reagents</Heading>
                  <Para>The reagents used were SIBX collector and Dow froth frother B supplied by Senmin.</Para>
                </Section3>
              </Section2>
              <Section2 Type="MaterialsAndMethods" ID="Sec7">
                <Heading>Methods</Heading>
                <Section3 ID="Sec8">
                  <Heading>Froth flotation</Heading>
                  <Para>A milling curve for the Palabora copper ore was first derived. The ball mill was turned so that it was facing vertically upward. The required number of steel balls was poured inside the mill and about 1.5 kg of the sample was delivered into the mill. The mill was closed and the timer was set on the mill panel. The sample was ground for 10 min and the power was switched off. The grinding balls were removed and the ground ore was then emptied into a bucket. The milled sample was wet screened with a 75 µm sieve. The wet screened sample residue was dried and weighed to determine the percentage that passed the 75 µm sieve. The procedure described was repeated but at 10, 20, 30, 40, 50 and 60 min grinding times. The tests were carried out in duplicates. Using the data obtained, the milling curve was constructed.</Para>
                  <Para>The Senmin sodium isobutyl xanthate (SIBX) collector was prepared at 6 % strength. About 60 g SIBX powder was weighed in a 1 L volumetric flask. Distilled water was added to make up to 1,000 ml and the flask was well shaken to ensure the dissolution of the powder. About 1.5 kg of the sample milled 45 % passing 75 µm was poured into a 4.5 L Denver cell to make a slurry density of 1.3 kg/L. All the milled ore was washed into the bucket. The cell and its contents were weighed and its mass was recorded. The flotation cell was then placed in the base of the Denver flotation machine. The flotation impeller was lowered into the slurry in the flotation cell. A clean concentrate collection pan was placed under the overflow lip of the cell. The flotation machine was switched on and the agitator was set at the speed of 1,000 rpm. The reagents-collector (SIBX) and frother (Dowfroth) were added with conditioning time of 2 min. The air valve was then opened. The froth was scraped towards the overflow every 15 s in 3, 7 and 10 min froth scraping time. The slurry level was adjusted and kept constant by adding 500 ml of water to the flotation cell every 5 min. The air valve was then closed and the agitator stopped. The Denver machine was electrically isolated. The three concentrates and the tail residue were taken to the oven for drying at 105 °C. The experiments were done in duplicates.</Para>
                </Section3>
                <Section3 ID="Sec9">
                  <Heading>X-ray fluorescence analysis</Heading>
                  <Para>The sample for the XRF analysis was first prepared. Two and a half spatula of boric acid and two spatula of the powder sample were mixed in a beaker. The mixture was poured into the mounting press and moulded at pressure above 400 bars. The pellet obtained was labelled. The sample was then subjected to X-ray fluorescence analysis according to standard [<CitationRef CitationID="CR1">1</CitationRef>].</Para>
                </Section3>
              </Section2>
            </Section1>
            <Section1 ID="Sec10">
              <Heading>Results and discussion</Heading>
              <Para>The screen distribution analysis for the mill feed in aperture ranges <Emphasis Type="Italic">A</Emphasis> (&lt;38), <Emphasis Type="Italic">B</Emphasis> (−63 + 38), <Emphasis Type="Italic">C</Emphasis> (−75 + 63), <Emphasis Type="Italic">D</Emphasis> (−125 + 75), <Emphasis Type="Italic">E</Emphasis> (−150 + 125), <Emphasis Type="Italic">F</Emphasis> (−212 + 150), <Emphasis Type="Italic">G</Emphasis> (−300 + 212), <Emphasis Type="Italic">H</Emphasis> (−425 + 300 μm) is presented in Fig. <InternalRef RefID="Fig1">1</InternalRef>. It is observed that the percentage passing 75 μm in the feed is very low, that is, 9.33 %. The results indicate that further grinding is required to obtain the set flotation target of 40 % passing 75 microns. It has been found that flotation occurs most efficiently in the particle size range 38–106 μm. The consideration of the entire size distribution curve is necessary as a mill may produce the required % &lt;75 μm and still produce a large amount of coarse material [<CitationRef CitationID="CR2">2</CitationRef>].<Figure ID="Fig1" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 1</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Particle size distribution (PSD) of the mill feed</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig1_HTML.gif"/>
                  </MediaObject>
                </Figure></Para>
              <Para>Figure <InternalRef RefID="Fig2">2</InternalRef> shows the milling curve obtained for the Palabora copper ore. The results obtained showed that the time required to obtain the grind of 45 % passing 75 microns was 20 min. A critical step in grinding is ensuring that the final particles from grinding are fine enough for efficient flotation. Coarser particles must be isolated and returned for further grinding. Since grinding requires considerable of input electrical energy, it is necessary to avoid over grinding [<CitationRef CitationID="CR2">2</CitationRef>].<Figure ID="Fig2" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 2</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Milling curve for Palabora copper ore</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig2_HTML.gif"/>
                  </MediaObject>
                </Figure></Para>
              <Para>Figure <InternalRef RefID="Fig3">3</InternalRef> shows copper recoveries obtained for each of the SIBX dosages tested. It was observed that there was an increase in recovery from 60 to 80 g/t dosage followed by a decrease at 90 g/t. The initial trend of increasing recovery at increasing SIBX dosage can be attributed to more sulphide being floated by the increased amount of collector added; with the highest recovery obtained at the SIBX dosage of 80 g/t. The results obtained thus suggest the 80 g/t dosage as the most efficient dosage for SIBX in floating the Palabora copper ore ground 45 % passing 75 µm.<Figure ID="Fig3" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 3</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Effects of SIBX dosage on copper recovery (R)</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="BlackWhite" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig3_HTML.gif"/>
                  </MediaObject>
                </Figure></Para>
              <Para>Figure <InternalRef RefID="Fig4">4</InternalRef> shows how recovery increases with time during the batch flotation tests. It was observed that the highest recoveries in 5 and 10 min froth scraping were obtained at 70 g/t SIBX dosage and the highest at 20 min at 80 g/t dosage. The results obtained suggest the 70 and 80 g/t dosages as the one that promoted the most efficient flotation. Figure <InternalRef RefID="Fig5">5</InternalRef> shows the recovery–grade curve which follows the generic inverse proportion trend. The decrease in copper grade with increase in collector dosage agrees with the observation of Mpongo and Siame [<CitationRef CitationID="CR5">5</CitationRef>] on the flotation of copper sulphide from the Nchanga sulphide-oxide copper ore. The results obtained show that increasing the collector dosage does not cause a corresponding increase in the copper grade of the concentrate obtained. It was observed that an increase in the sodium isopropyl xanthate collector up to 30 g/t leads to an increase in copper grade and then a decrease in grade at 50 and almost no increase at 80 g/t. As expected, the recovery of copper to the concentrate was observed to be inversely proportional to the grade value. This behaviour was said to be probably due to the fact that an excessive concentration of the collector has an adverse effect on the recovery of the valuable minerals due to the development of multi-layers on the mineral particles, which reduce the proportion of hydrocarbon radicals oriented into the bulk solution. The hydrophobicity of the mineral particles is thus reduced and hence, their floatability [<CitationRef CitationID="CR8">8</CitationRef>].<Figure ID="Fig4" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 4</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Recovery: time curve for Palabora copper ore</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig4_HTML.gif"/>
                  </MediaObject>
                </Figure><Figure ID="Fig5" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 5</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Recovery: grade curve for Palabora copper ore</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig5_HTML.gif"/>
                  </MediaObject>
                </Figure></Para>
              <Para>Figure <InternalRef RefID="Fig6">6</InternalRef> shows the particle size distribution of the tailings which shows that a significant amount of the various size fractions were floated during flotation. The percentage passing 425 microns has decreased from 81.33 % in the PSD of the mill feed to 53.97 % in the tailings indicating that the ball mill sufficiently liberated the copper minerals to produce the required sizes for froth flotation. Flotation occurs most efficiently in the range 38–106 μm this is emphasised by the reduced percentage passing these size fractions in the particle size distribution of the tailings indicating efficient milling and flotation.<Figure ID="Fig6" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Fig. 6</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>Particle size distribution of the Palabora copper ore tailings</SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="LinedrawHalftone" Rendition="HTML" Format="GIF" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs40090-014-0025-5/MediaObjects/40090_2014_25_Fig6_HTML.gif"/>
                  </MediaObject>
                </Figure></Para>
            </Section1>
            <Section1 Type="Conclusion" ID="Sec11">
              <Heading>Conclusions</Heading>
              <Para>The Palabora copper ore ground 45 % passing 75 µm was successfully froth-floated with SIBX collector at varying dosages. The results obtained showed that the highest recovery of copper into the concentrate was obtained at 80 g/t dosage. It was further observed that the concentrate grade decreased with increasing recovery in accordance with the established generic trend between grade and recovery. The particle distribution analysis of the tailings confirmed that the froth flotation occurred most efficiently in the fine particle size range 38–106 μm.</Para>
            </Section1>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibUnstructured>ASTM International E60-11 (2013) Analytical chemistry standards. Standard practice for analysis of metals, ores and related materials by spectrophotometry. <ExternalRef>
                    <RefSource>http://www.astm.org/Standards/analytical-chemistry-standards.html</RefSource>
                    <RefTarget TargetType="URL" Address="http://www.astm.org/Standards/analytical-chemistry-standards.html"/>
                  </ExternalRef>. Accessed 31 Jan 2014</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>WG</Initials>
                    <FamilyName>Davenport</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>King</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Schlesinger</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>AK</Initials>
                    <FamilyName>Biswas</FamilyName>
                  </BibAuthorName>
                  <Year>2000</Year>
                  <BookTitle>Extractive metallurgy of copper</BookTitle>
                  <EditionNumber>4</EditionNumber>
                  <PublisherName>Elsevier Science Ltd</PublisherName>
                  <PublisherLocation>Oxford</PublisherLocation>
                </BibBook>
                <BibUnstructured>Davenport WG, King M, Schlesinger M, Biswas AK (2000) Extractive metallurgy of copper, 4th edn. Elsevier Science Ltd, Oxford</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibUnstructured>Groves DI, Vielreicher NM (2001) The Phalabowra (Palabora) carbonatite-hosted magnetite–copper sulphide deposit, South Africa: an end-member of the iron-oxide copper–gold-rare earth element deposit group? Miner Depos 36:189–194. Springer. <ExternalRef>
                    <RefSource>http://link.springer.com/article/10.1007/s001260050298#page-2</RefSource>
                    <RefTarget TargetType="URL" Address="http://link.springer.com/article/10.1007/s001260050298#page-2"/>
                  </ExternalRef>. Accessed 16 July 2014</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibUnstructured>Heinrich EWM (1970) The Palabora carbonatitic complex; a unique copper deposit. Canad Mineral 10:585–598. <ExternalRef>
                    <RefSource>http://rruff.info/doclib/cm/vol10/CM10_585.pdf</RefSource>
                    <RefTarget TargetType="URL" Address="http://rruff.info/doclib/cm/vol10/CM10_585.pdf"/>
                  </ExternalRef>. Accessed 16 July 2014</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <CitationNumber>5.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>MK</Initials>
                    <FamilyName>Mpongo</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>E</Initials>
                    <FamilyName>Siame</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <ArticleTitle xml:lang="en" Language="En">Effect of collector, frother and depressant addition on the copper recovery and concentrate grade of the Nchanga underground scavenger circuit of Konkola copper mine-Zambia</ArticleTitle>
                  <JournalTitle>Afr J Sci Technol (AJST) Sci Eng Ser</JournalTitle>
                  <VolumeID>7</VolumeID>
                  <IssueID>1</IssueID>
                  <FirstPage>8</FirstPage>
                  <LastPage>11</LastPage>
                </BibArticle>
                <BibUnstructured>Mpongo MK, Siame E (2006) Effect of collector, frother and depressant addition on the copper recovery and concentrate grade of the Nchanga underground scavenger circuit of Konkola copper mine-Zambia. Afr J Sci Technol (AJST) Sci Eng Ser 7(1):8–11</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <CitationNumber>6.</CitationNumber>
                <BibUnstructured>Sharaky AM (2011) Mineral resources and exploration in Africa. <ExternalRef>
                    <RefSource>http://african.cu.edu.eg/Dr_Abbas/Papers/Minerals_2011.pdf</RefSource>
                    <RefTarget TargetType="URL" Address="http://african.cu.edu.eg/Dr_Abbas/Papers/Minerals_2011.pdf"/>
                  </ExternalRef>. Accessed 16 July 2014</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <CitationNumber>7.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Wiese</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Harris</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Bradshaw</FamilyName>
                  </BibAuthorName>
                  <Year>2005</Year>
                  <ArticleTitle xml:lang="en" Language="En">Investigation of the role and interactions of a dithiophosphate collector in the flotation of sulphides from the Merensky reef</ArticleTitle>
                  <JournalTitle>Miner Eng</JournalTitle>
                  <VolumeID>18</VolumeID>
                  <FirstPage>791</FirstPage>
                  <LastPage>800</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2MXmt1aqur4%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/j.mineng.2005.01.032</Handle>
                  </Occurrence>
                  <BibComments>1:CAS:528:DC%2BD2MXmt1aqur4%3D, 10.1016/j.mineng.2005.01.032</BibComments>
                </BibArticle>
                <BibUnstructured>Wiese J, Harris P, Bradshaw D (2005) Investigation of the role and interactions of a dithiophosphate collector in the flotation of sulphides from the Merensky reef. Miner Eng 18:791–800</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <CitationNumber>8.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>BA</Initials>
                    <FamilyName>Wills</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <BookTitle>Mineral processing technology</BookTitle>
                  <EditionNumber>7</EditionNumber>
                  <PublisherName>Pergamon Press</PublisherName>
                  <PublisherLocation>Oxford</PublisherLocation>
                </BibBook>
                <BibUnstructured>Wills BA (2006) Mineral processing technology, 7th edn. Pergamon Press, Oxford</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>IJIC-D-14-00025.2</EditorialManuscriptNumber>
            </JobSheet></Publisher>


