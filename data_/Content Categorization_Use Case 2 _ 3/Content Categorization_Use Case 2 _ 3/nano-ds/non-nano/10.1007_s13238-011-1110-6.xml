<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-011-1110-6">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>1110</ArticleID>
            <ArticleDOI>10.1007/s13238-011-1110-6</ArticleDOI>
            <ArticleSequenceNumber>5</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" Language="En">Innovator of <Emphasis Type="Italic">in vitro</Emphasis> virus culture—Dr. Chen-Hsiang Huang</ArticleTitle>
            <ArticleCategory>Recollection</ArticleCategory>
            <ArticleFirstPage>782</ArticleFirstPage>
            <ArticleLastPage>783</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2011</Year>
                <Month>11</Month>
                <Day>5</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2011</Year>
                <Month>11</Month>
                <Day>6</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Higher Education Press and Springer-Verlag Berlin Heidelberg</CopyrightHolderName>
              <CopyrightYear>2011</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="Regular">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Guangsheng</GivenName>
                  <FamilyName>Cheng</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Institute of Microbiology</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Measle</Keyword>
              <Keyword>Measle Virus</Keyword>
              <Keyword>Poliomyelitis</Keyword>
              <Keyword>Virus Culture</Keyword>
              <Keyword>Measle Vaccine</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="PDF">
            <page id="pg01">Protein Cell 2011, 2(10): 782–783 DOI 10.1007/s13238-011-1110-6 Protein &amp; Cell RECOLLECTION Innovator of in vitro virus culture— Dr. Chen-Hsiang Huang Guangsheng Cheng Institute of Microbiology, Chinese Academy of Sciences, Beijing 100101, China Received January 1, 2011 Accepted February 2, 2011 In 1954, American scientists John Franklin Enders, Thomas Huckle Weller and Frederick Chapman Robbins were awarded the Nobel Prize in Medicine and Physiology due to their contribution to the development of the technique to grow polio virus in cultures of human tissues. Their technique later became a powerful tool for the prevention of poliomyelitis in children. At the Nobel lecture on December 11, 1954, when they talked about how they developed this technique, the three scientists mentioned that the methods had been used in the past by other workers, in particular Dr. Chen-Hsiang Huang (Fig. 1). Dr. Chen-Hsiang Huang had been studying virology at the Rockefeller Institute for Medical Research since 1941. In 1943, he published an original research article “Further studies on the titration and neutralization of the western strain of equine encephalomyelitis virus in tissue culture” in Journal of Experimental Medicine. In the paper, he reported on his successful establishment of a new technique to culture viruses, thus making a breakthrough in virology. Prior to his studies, viruses had to be examined by injection into animals and observation of the incidence or mortality of infected- animals. Dr. Chen-Hsiang Huang’s method is to digest animal tissues into a single layer of cells, and then inoculate the virus in the cultured cells. If there is a virus infection and proliferation, pathological changes of the inoculated cells can be observed under a light microscope. This technique brought virus culture from animal level to cellular level. In their Nobel lecture, Enders, Weller and Robbins said that in the past, observation of cellular pathological changes caused by the poliovirus was very troublesome and often took more than two weeks. They tested a number of methods to improve it, and ﬁnally developed the technique with Dr. Huang’s method. Later on, many laboratories around the world applied this technique in pathogenic virus studies and virus isolation, especially in the virus etiology of some little-known diseases at that time. For his innovative achievement, Chen-Hsiang Huang was later considered to have made important contributions to modern virology. Dr. Chen-Hsiang Huang was born in a Christian family in Kulangsu, Xiamen, China and grew up determined to study medicine. He graduated from Peking Union Medical College in 1934, and was engaged in virology research in the United States from 1941 to 1943. Before he returned to his motherland in December 1943, he unreservedly published his ﬁndings in his paper, so that other virologists could avoid some detours in seeking the truth. After the founding of new China, he continued devoting himself to virology studies in Beijing. In 1980, he was elected an academician of Chinese Academy of Sciences. He was also a member of American Society for Experimental Biology and Medicine, an honorary member of Infectious Diseases Society of America, and a member on the Advisory Committee of the International Comparative Virology Organization. In the 1950s, he served as a member of the editorial board of the journal Issues of Virology published by the former Soviet Union and other Eastern European countries, as well as the editorial boards of the American journals International Journal of Virology and Reviews of Infectious Diseases. Figure 1. Dr. Chen-Hsiang Huang 782 © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011</page>
            <page id="pg02">Innovator of in vitro virus culture—Dr. Chen-Hsiang Huang Protein &amp; Cell Figure 2. The Key to the City of Denton, Texas that was awarded to Chen-Hsiang Huang in 1983. In over 50 years of his academic life, Dr. Chen-Hsiang Huang conducted extensive research on the theory of medical virology and viral diseases. On account of his contribution to the systematic study of encephalitis, he won the National Science Conference Prize in 1978 and, after he passed away, the Science and Technology Award of Ministry of Health in 1988. He also studied the pathogenicity and immunity of measles virus, as well as the measles virus hemagglutinin, measles vaccine adjuvants, vaccine produc- tion techniques, etc. In his later years, he began to focus on viral immunity studies, and published research articles such as “The effects of passive immunity on active immunity by live virus.” He was the ﬁrst one in China to apply viral interference into medical virology research. He presented a vision of cancer immu- notherapy through virus, in which virus was used to infect tumor cells, not only to directly kill the tumor cells, but also to change the antigenicity of the cell membranes of the tumor cells, so that they can be recognized and targeted by the patient’s immune system. After years of exploration, this project had made encouraging progress. He also studied non- pathogenic viruses that others paid little attention to at that time, as well as the insect-vector viral diseases. Under his guidance, a lot of exploratory work were carried out on virus- mediated immunotherapy of tumor. After he was diagnosed with leukemia, he tested the immunotherapy on himself, making a ﬁnal contribution to the research. Dr. Chen-Hsiang Huang had a high academic status internationally. In 1983, when he led a delegation of the Chinese Society for Microbiology to the United States to attend the 13 th International Congress of Microbiology, he was awarded the honorary citizenship and the “Key to the City” by Denton, Texas (Fig. 2). In 1986, he met Prof. Joseph Louis Melnick, American virologist and the founder of the International Committee on Taxonomy of Viruses, during Melnick’s visit in Beijing, and introduced him to the Chinese community of virologists (Fig. 3). “One can never surpass others if always taking the old paths. Innovation is the key in exploring truth. This is the principle that I’ve been following all my life.” said Dr. Chen- Hsiang Huang to summarize his academic life. Figure 3. Joseph Louis Melnick and Chen-Hsiang Huang in Beijing (1986). © Higher Education Press and Springer-Verlag Berlin Heidelberg 2011 783</page>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


