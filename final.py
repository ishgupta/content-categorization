# -*- coding: utf-8 -*-
"""
Created on Sat Sep  1 18:42:40 2018

@author: isgupta
"""

"""
    look for "review"
"""
# In[]
# import packages

from content_categorization import fetch_data_from_concept_net, extract_data, extract_sentences
from pathlib import Path
from itertools import chain

# In[]
# prepare vocabulary

vocab_terms = ['human', 'animal', 'consent', 'disclosure']  # review- to be enriched- PTR
relations = ['Synonym']

vocabulary = fetch_data_from_concept_net(vocab_terms, relations)

# read data:
data_dir = Path("data")
data = extract_data(data_dir)

# extract relevant sentences
sentences = {}
matched_words = {}

# review
search_terms = list(chain(*list(map(lambda x : list(chain(*list(vocabulary.values())[x].values())), range(0, len(vocabulary))))))

for doc, content in data.items():
    sentences[doc] = extract_sentences(content, search_terms, remove_stop_words = True)
    
    matched_words[doc] = set(chain(*[x[1] for x in sentences[doc]]))
    sentences[doc] = [x[0] for x in sentences[doc]]
    

# prepare feature data    
features = {}
for doc in list(data.keys()):
    features[doc] = {}
    features[doc]['sentences'] = sentences[doc]
    # review headings
    features[doc]['headings'] = None
    features[doc] = matched_words[doc]
    # review animal authority
    features[doc]['authority'] = None
    
    # review
    features[doc]['label'] = 'animal' if set(matched_words[doc]).intersection(vocabulary['animal']) != () else None
    features[doc]['label'] = 'human' if set(matched_words[doc]).intersection(set(vocabulary['human'])) != () else None
    features[doc]['label'] = 'both' if set(matched_words[doc]).intersection(set(vocabulary['human'])).intersection(set(vocabulary['animal'])) else None
    
    
    
    
    

    