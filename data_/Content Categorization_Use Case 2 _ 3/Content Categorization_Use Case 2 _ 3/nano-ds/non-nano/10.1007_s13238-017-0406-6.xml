<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <JournalOnlineFirst>
      <Article OutputMedium="All" ID="s13238-017-0406-6">
        <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
          <ArticleID>406</ArticleID>
          <ArticleDOI>10.1007/s13238-017-0406-6</ArticleDOI>
          <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Investigation on natural resources and species conservation of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>, the famous medicinal fungus endemic to the Tibetan Plateau</ArticleTitle>
          <ArticleCategory>Recollection</ArticleCategory>
          <ArticleFirstPage>1</ArticleFirstPage>
          <ArticleLastPage>3</ArticleLastPage>
          <ArticleHistory>
            <RegistrationDate>
              <Year>2017</Year>
              <Month>3</Month>
              <Day>24</Day>
            </RegistrationDate>
            <OnlineDate>
              <Year>2017</Year>
              <Month>4</Month>
              <Day>10</Day>
            </OnlineDate>
          </ArticleHistory>
          <ArticleCopyright>
            <CopyrightHolderName>The Author(s)</CopyrightHolderName>
            <CopyrightYear>2017</CopyrightYear>
            <License Version="4.0" Type="OpenAccess" SubType="CC BY">
              <SimplePara>
                        <Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution 4.0 International License (<ExternalRef>
                  <RefSource>http://creativecommons.org/licenses/by/4.0/</RefSource>
                  <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/4.0/"/>
                </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided you give appropriate credit to the original author(s) and the source, provide a link to the Creative Commons license, and indicate if changes were made.</SimplePara>
            </License>
          </ArticleCopyright>
          <ArticleGrants Type="OpenChoice">
            <MetadataGrant Grant="OpenAccess"/>
            <AbstractGrant Grant="OpenAccess"/>
            <BodyPDFGrant Grant="OpenAccess"/>
            <BodyHTMLGrant Grant="OpenAccess"/>
            <BibliographyGrant Grant="OpenAccess"/>
            <ESMGrant Grant="OpenAccess"/>
          </ArticleGrants>
        </ArticleInfo>
        <ArticleHeader>
          <AuthorGroup>
            <Author ID="Au1" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Wenjing</GivenName>
                <FamilyName>Wang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au2" AffiliationIDS="Aff1 Aff2">
              <AuthorName DisplayOrder="Western">
                <GivenName>Ke</GivenName>
                <FamilyName>Wang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au3" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Xiaoliang</GivenName>
                <FamilyName>Wang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au4" AffiliationIDS="Aff3">
              <AuthorName DisplayOrder="Western">
                <GivenName>Ruiheng</GivenName>
                <FamilyName>Yang</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au5" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Yi</GivenName>
                <FamilyName>Li</FamilyName>
              </AuthorName>
            </Author>
            <Author ID="Au6" CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
              <AuthorName DisplayOrder="Western">
                <GivenName>Yijian</GivenName>
                <FamilyName>Yao</FamilyName>
              </AuthorName>
              <Contact>
                <Email>yaoyj@im.ac.cn</Email>
              </Contact>
            </Author>
            <Affiliation ID="Aff1">
              <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
              <OrgDivision>Institute of Microbiology</OrgDivision>
              <OrgName>Chinese Academy of Sciences</OrgName>
              <OrgAddress>
                <City>Beijing</City>
                <Postcode>100101</Postcode>
                <Country Code="CN">China</Country>
              </OrgAddress>
            </Affiliation>
            <Affiliation ID="Aff2">
              <OrgID Type="GRID" Level="Institution">grid.410726.6</OrgID>
              <OrgName>University of Chinese Academy of Sciences</OrgName>
              <OrgAddress>
                <City>Beijing</City>
                <Postcode>100049</Postcode>
                <Country Code="CN">China</Country>
              </OrgAddress>
            </Affiliation>
            <Affiliation ID="Aff3">
              <OrgID Type="GRID" Level="Institution">grid.419073.8</OrgID>
              <OrgDivision>Institute of Edible Fungi</OrgDivision>
              <OrgName>Shanghai Academy of Agricultural Sciences</OrgName>
              <OrgAddress>
                <City>Shanghai</City>
                <Postcode>201403</Postcode>
                <Country Code="CN">China</Country>
              </OrgAddress>
            </Affiliation>
          </AuthorGroup>
          <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
            <Heading>Keywords</Heading>
            <Keyword>Tibetan Plateau</Keyword>
            <Keyword>County Level</Keyword>
            <Keyword>Alpine Meadow</Keyword>
            <Keyword>Host Insect</Keyword>
            <Keyword>Distribution Site</Keyword>
          </KeywordGroup>
        </ArticleHeader>
        <Body Type="XML">
          <Para ID="Par1">
                  <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis> (Berk.) G.H. Sung, J.M. Sung, Hywel-Jones &amp; Spatafora, an entomogenous fungus endemic to the Tibetan Plateau, is one of the best known medicinal fungi in China and around the world. It has been documented in literature for hundreds of years and officially classified as a drug in the Chinese Pharmacopeia (Committee of Pharmacopeia, Chinese Ministry of Health, <CitationRef CitationID="CR1">2005</CitationRef>). The whole fungus, including a stroma emerged above the ground from an underground sclerotium covered by the exoskeleton of a moth larva (Fig. <InternalRef RefID="Fig1">1</InternalRef>), is harvested for medicinal uses, usually as tonic to strengthen the body system and to regain energy after a serious illness (Pegler et al., <CitationRef CitationID="CR5">1994</CitationRef>) but now for many other purposes.<Figure ID="Fig1" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 1</CaptionNumber>
                <CaptionContent>
                  <SimplePara>
                    <Emphasis Type="BoldItalic">Ophiocordyceps sinensis</Emphasis>
                    <Emphasis Type="Bold">in natural habitat (left) and freshly collected after cleaning (right)</Emphasis>
                  </SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO1">
                <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-017-0406-6/MediaObjects/13238_2017_406_Fig1_HTML.jpg"/>
              </MediaObject>
            </Figure>
               </Para>
          <Para ID="Par2">
                  <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis> occurs in alpine meadow and shrub habitats from an altitude of 3,000 m up to the snow-line (Li et al., <CitationRef CitationID="CR4">2011</CitationRef>). Since <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> is now sold at a price much higher than the gold, thousands and thousands of collectors are crowding onto the Tibetan Plateau to plunder this valuable resource during late spring and summer every year (Fig. <InternalRef RefID="Fig2">2</InternalRef>). Due to its strict host-specificity, confined geographic distribution, climate changes and over exploitation by human, <Emphasis Type="Italic">O. sinensis</Emphasis> has suffered from a sharp decrease in natural production in recent decades. The annual production was recorded up to 100 tons in 1950s and ranged from 50–80 tons in 1960s but sharply decreased to 5–15 tons in 1990s (Li, <CitationRef CitationID="CR3">1998</CitationRef>). In 1999, the fungus was listed as an endangered species under the second class of state protection (State Forestry Administration and Ministry of Agriculture, <CitationRef CitationID="CR7">1999</CitationRef>).<Figure ID="Fig2" Float="Yes" Category="Standard">
              <Caption xml:lang="en" Language="En">
                <CaptionNumber>Figure 2</CaptionNumber>
                <CaptionContent>
                  <SimplePara>
                    <Emphasis Type="Bold">Collectors in Golog Tibetan Autonomous Prefecture, Qinghai Province, China, photoed on June 4, 2014</Emphasis>
                  </SimplePara>
                </CaptionContent>
              </Caption>
              <MediaObject ID="MO2">
                <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-017-0406-6/MediaObjects/13238_2017_406_Fig2_HTML.jpg"/>
              </MediaObject>
            </Figure>
               </Para>
          <Para ID="Par3">Investigation on the resource of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> in China started from 1952, mainly focusing on its natural distribution. Since 2000, the information on distribution of the fungus became widely available online. Most of the materials online were short articles with a list of localities, often at county level or above, without any analysis. There were not many reports based on fieldwork to determine the distribution range of the species. Till 2011, the distribution of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> at the county level was clarified based on extensive fieldwork during the years 2000–2010 by the research group from the Institute of Microbiology, Chinese Academy of Sciences, on specimens preserved in HMAS (Fangrium, Institute of Microbiology, Chinese Academy of Sciences) and HKAS (Herbarium of Cryptograms, Kunming Institute of Botany, Chinese Academy of Sciences) and on examination of literature of more than 3600 publications. A total of 203 localities at county level for <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> were recorded and analyzed. Among them, 106 were considered as confirmed distribution sites, 65 as possible distribution sites, 29 as excluded distribution sites, and three as suspicious distribution sites for the fungus (Li et al., <CitationRef CitationID="CR4">2011</CitationRef>). In addition, the host insect species of the fungus was also summarized based upon an extensive literature survey. A total of 91 insect names spanning 13 genera of Hepialidae were gathered. Fifty-seven species were considered as recognizable potential host insects of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis>, whilst eight as indeterminate hosts and 26 as non-hosts (Wang and Yao, <CitationRef CitationID="CR8">2011</CitationRef>).</Para>
          <Para ID="Par4">Genetic divergence in <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> has been observed using different molecular markers and the southern populations were found more diverse than that of northern populations (Jiao, <CitationRef CitationID="CR2">2010</CitationRef>). It is also speculated that <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> spreads from a center of origin (the Nyingchi District) to southern regions and subsequently to northern areas (Zhang et al., <CitationRef CitationID="CR10">2009</CitationRef>). Further, the correlation of genetic distances between <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> and its host insects indicated that they coevolved through the history (Quan et al., <CitationRef CitationID="CR6">2014</CitationRef>).</Para>
          <Para ID="Par5">As one of the most valued living resources for local economy on the Tibetan Plateau and adjacent regions, further efforts should be devoted to learning more about the species conservation and resource management for this special fungus. So far, annual harvest, resource reservation, and quality comparison of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> in different geographic regions are less studied. Meanwhile, the natural distribution and the annual yield per unit area for this species are both dwindling based on feedbacks from local households, harvesters, and modeling predictions. The decline may result from the overexploitation, overgrazing and climate change, but there is no doubt that the changes in its distribution range and yield should be monitored by long-term field observation and statistical analyses.</Para>
          <Para ID="Par6">Assessments of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> on its distribution and sustainable situation have been reported from Western Sichuan, Qinghai, and Northern Yunnan Provinces in China and from some areas in Nepal. The data were generally collected from households, harvesters and respondents by interviews, group discussions, informal communications, and personal field observations. The contribution to livelihoods and community development and the population status of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> were documented. However, there are no systematic management plans and national efforts to sustain these precious fungal resources. The extensive survey and the long-term field observation are important for successful management to protect natural resources of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> and to facilitate the improvement of the adaptive capacity to survive in the changing environment as revealed recently by Yan et al., (<CitationRef CitationID="CR9">2017</CitationRef>).</Para>
          <Para ID="Par7">The extensive survey and long-term field observation for <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> can be structured to target at: (i) to organize the survey of the species occurrence through administrative division with the support from the government from prefecture level down to every country, township and village, marking the production area to each mountain, finding the dates of emerging and terminating of the stroma, calculating annual yields based on real harvest; (ii) to determine the site for long-term field observation based on the habitat, fungal genetic background, transportation and living facilities at the location, to conduct the long-term observation with climate, soil and environment factors and also physical, chemical and biological characters, e.g., temperature, humidity, precipitation, organic matter, soil texture, pH, vegetation types, biomass, community structure, animals, soil microbes, and so on, and to monitor the occurrence of the fungus and its host insects, as well as the harvest grazing and human activities; (iii) to establish special natural reserves for the fungus and to carry out the management for sustainable uses of the fungal resources, including local laws and government regulations, time and methods of harvesting, measures for improvement of grassland for the fungus and its host insects, etc.; (iv) to construct the comprehensive database for recording and management of the fungus, its host insects, and the grassland for its survival and reproduction.</Para>
          <Para ID="Par8">Along with market demands, <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> is largely consumed in its dried form as a stroma emerged from a sclerotium which is covered by the exoskeleton of caterpillar larva. The maturity of the stroma is considered very important to the quality of the product. The immature specimens with stromata protruded from intact and solid sclerotium are much more welcomed than those with well grown or mature stromata because the nutritional reserves within stromata have been exhausted. To maintain the medicinal value, the stromata and sclerotia of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> are harvested before they start to produce and release the spores for reproduction. It is an urgent need to manage the natural resources of <Emphasis Type="Italic">O</Emphasis>. <Emphasis Type="Italic">sinensis</Emphasis> which have limited natural production but are so extensively harvested by human overexploitation.</Para>
        </Body>
        <BodyRef TargetType="OnlinePDF"/>
        <ArticleBackmatter>
          <Bibliography ID="Bib1">
            <Heading>References</Heading>
            <Citation ID="CR1">
              <BibUnstructured>Committee of Pharmacopeia, Chinese Ministry of Health (2005) Chinese Pharmacopeia, Part one. 2005 ed. Chemical Industry Press, Beijing, China. (国家药典委员会编. (2005) 中华人民共和国药典 (2005年版一部). 北京: 化学工业出版社)</BibUnstructured>
            </Citation>
            <Citation ID="CR2">
              <BibUnstructured>Jiao L (2010) Phylogeographic Study on <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>. Beijing: Thesis Submitted for the Doctoral Degree, Graduate School of Chinese Academy of Sciences. (焦磊. (2010) 冬虫夏草系统发育地理学研究. 博士学位论文, 中国科学院研究生院)</BibUnstructured>
            </Citation>
            <Citation ID="CR3">
              <BibUnstructured>Li HK (1998) Research progress on resources of <Emphasis Type="Italic">Cordyceps</Emphasis> species. World Agriculture 225: 35. (李宏科. (1998) 虫草菌资源研究进展. 世界农业 225: 35)</BibUnstructured>
            </Citation>
            <Citation ID="CR4">
              <BibArticle>
                <BibAuthorName>
                  <Initials>Y</Initials>
                  <FamilyName>Li</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>XL</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>L</Initials>
                  <FamilyName>Jiao</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>Y</Initials>
                  <FamilyName>Jiang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>H</Initials>
                  <FamilyName>Li</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>SP</Initials>
                  <FamilyName>Jiang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>N</Initials>
                  <FamilyName>Lhosumtseiring</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>SZ</Initials>
                  <FamilyName>Fu</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>CH</Initials>
                  <FamilyName>Dong</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>Y</Initials>
                  <FamilyName>Zhan</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Yao</FamilyName>
                </BibAuthorName>
                <Year>2011</Year>
                <ArticleTitle xml:lang="en" Language="En">A survey of the geographic distribution of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>
                        </ArticleTitle>
                <JournalTitle>J Microbiol</JournalTitle>
                <VolumeID>49</VolumeID>
                <FirstPage>913</FirstPage>
                <LastPage>919</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1007/s12275-011-1193-z</Handle>
                </Occurrence>
                <Occurrence Type="PID">
                  <Handle>22203553</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Li Y, Wang XL, Jiao L, Jiang Y, Li H, Jiang SP, Lhosumtseiring N, Fu SZ, Dong CH, Zhan Y, Yao YJ (2011) A survey of the geographic distribution of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>. J Microbiol 49:913–919</BibUnstructured>
            </Citation>
            <Citation ID="CR5">
              <BibArticle>
                <BibAuthorName>
                  <Initials>DN</Initials>
                  <FamilyName>Pegler</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Yao</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>Y</Initials>
                  <FamilyName>Li</FamilyName>
                </BibAuthorName>
                <Year>1994</Year>
                <ArticleTitle xml:lang="en" Language="En">The Chinese ‘caterpillar fungus’</ArticleTitle>
                <JournalTitle>Mycologist</JournalTitle>
                <VolumeID>8</VolumeID>
                <FirstPage>3</FirstPage>
                <LastPage>5</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1016/S0269-915X(09)80670-8</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Pegler DN, Yao YJ, Li Y (1994) The Chinese ‘caterpillar fungus’. Mycologist 8:3–5</BibUnstructured>
            </Citation>
            <Citation ID="CR6">
              <BibArticle>
                <BibAuthorName>
                  <Initials>QM</Initials>
                  <FamilyName>Quan</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>QX</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>XL</Initials>
                  <FamilyName>Zhou</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>S</Initials>
                  <FamilyName>Li</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>XL</Initials>
                  <FamilyName>Yang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YG</Initials>
                  <FamilyName>Zhu</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>Z</Initials>
                  <FamilyName>Cheng</FamilyName>
                </BibAuthorName>
                <Year>2014</Year>
                <ArticleTitle xml:lang="en" Language="En">Comparative phylogenetic relationships and genetic structure of the caterpillar fungus <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis> and its host insects inferred from multiple gene sequences</ArticleTitle>
                <JournalTitle>J Microbiol</JournalTitle>
                <VolumeID>52</VolumeID>
                <FirstPage>99</FirstPage>
                <LastPage>105</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1007/s12275-014-3391-y</Handle>
                </Occurrence>
                <Occurrence Type="PID">
                  <Handle>24500473</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Quan QM, Wang QX, Zhou XL, Li S, Yang XL, Zhu YG, Cheng Z (2014) Comparative phylogenetic relationships and genetic structure of the caterpillar fungus <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis> and its host insects inferred from multiple gene sequences. J Microbiol 52:99–105</BibUnstructured>
            </Citation>
            <Citation ID="CR7">
              <BibUnstructured>State Forestry Administration and Ministry of Agriculture, Order No. 4 (1999) The list of the wild plants under the state emphasized protection. Available at <ExternalRef>
                  <RefSource>http://www.gov.cn/gongbao/content/2000/content_60072.htm</RefSource>
                  <RefTarget TargetType="URL" Address="http://www.gov.cn/gongbao/content/2000/content_60072.htm"/>
                </ExternalRef>. Accessed 17 June 2016 (<Emphasis Type="Bold">in Chinese</Emphasis>)</BibUnstructured>
            </Citation>
            <Citation ID="CR8">
              <BibArticle>
                <BibAuthorName>
                  <Initials>XL</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Yao</FamilyName>
                </BibAuthorName>
                <Year>2011</Year>
                <ArticleTitle xml:lang="en" Language="En">Host insect species of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>: a review</ArticleTitle>
                <JournalTitle>ZooKeys</JournalTitle>
                <VolumeID>127</VolumeID>
                <FirstPage>43</FirstPage>
                <LastPage>59</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.3897/zookeys.127.928</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Wang XL, Yao YJ (2011) Host insect species of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>: a review. ZooKeys 127:43–59</BibUnstructured>
            </Citation>
            <Citation ID="CR9">
              <BibArticle>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Yan</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>Y</Initials>
                  <FamilyName>Li</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>WJ</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>JS</Initials>
                  <FamilyName>He</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>RH</Initials>
                  <FamilyName>Yang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>HJ</Initials>
                  <FamilyName>Wu</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>XL</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>L</Initials>
                  <FamilyName>Jiao</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>ZY</Initials>
                  <FamilyName>Tang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Yao</FamilyName>
                </BibAuthorName>
                <Year>2017</Year>
                <ArticleTitle xml:lang="en" Language="En">Range shifts in response to climate change of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>, a fungus endemic to the Tibetan Plateau</ArticleTitle>
                <JournalTitle>Biol Conserv</JournalTitle>
                <VolumeID>206</VolumeID>
                <FirstPage>143</FirstPage>
                <LastPage>150</LastPage>
                <Occurrence Type="DOI">
                  <Handle>10.1016/j.biocon.2016.12.023</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Yan YJ, Li Y, Wang WJ, He JS, Yang RH, Wu HJ, Wang XL, Jiao L, Tang ZY, Yao YJ (2017) Range shifts in response to climate change of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>, a fungus endemic to the Tibetan Plateau. Biol Conserv 206:143–150</BibUnstructured>
            </Citation>
            <Citation ID="CR10">
              <BibArticle>
                <BibAuthorName>
                  <Initials>YJ</Initials>
                  <FamilyName>Zhang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>LL</Initials>
                  <FamilyName>Xu</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>S</Initials>
                  <FamilyName>Zhang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>XZ</Initials>
                  <FamilyName>Liu</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>ZQ</Initials>
                  <FamilyName>An</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>M</Initials>
                  <FamilyName>Wang</FamilyName>
                </BibAuthorName>
                <BibAuthorName>
                  <Initials>YL</Initials>
                  <FamilyName>Guo</FamilyName>
                </BibAuthorName>
                <Year>2009</Year>
                <ArticleTitle xml:lang="en" Language="En">Genetic diversity of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>, a medicinal fungus endemic to the Tibetan Plateau: Implications for its evolution and conservation</ArticleTitle>
                <JournalTitle>BMC Evol Biol</JournalTitle>
                <VolumeID>9</VolumeID>
                <FirstPage>1</FirstPage>
                <LastPage>12</LastPage>
                <Occurrence Type="COI">
                  <Handle>1:CAS:528:DC%2BD1MXkvFGht70%3D</Handle>
                </Occurrence>
                <Occurrence Type="DOI">
                  <Handle>10.1186/1471-2148-9-1</Handle>
                </Occurrence>
              </BibArticle>
              <BibUnstructured>Zhang YJ, Xu LL, Zhang S, Liu XZ, An ZQ, Wang M, Guo YL (2009) Genetic diversity of <Emphasis Type="Italic">Ophiocordyceps sinensis</Emphasis>, a medicinal fungus endemic to the Tibetan Plateau: Implications for its evolution and conservation. BMC Evol Biol 9:1–12</BibUnstructured>
            </Citation>
          </Bibliography>
        </ArticleBackmatter>
      </Article>
    </JournalOnlineFirst>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>17912.zip</EditorialManuscriptNumber>
            </JobSheet></Publisher>


