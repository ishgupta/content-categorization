# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 16:36:29 2018

@author: ishgupta
"""
"""
pip install docx2txt pandas numpy scipy sklearn nltk pytest-runner ordered_set pymongo requests flask flask_cors json ggplot csv
"""
# In[]
from pathlib import Path
from nltk import sent_tokenize, word_tokenize
import docx2txt
import pandas as pd
from itertools import chain
from copy import deepcopy
from term_extraction import extract_important_features_using_tf_idf, extract_phrases, filter_nouns, fetch_data_from_concept_net, filter_nouns_v1, singularize
import logging
import textract
import spacy
from nltk.stem import WordNetLemmatizer
import json

# In[]
def fetch_vocabulary(vocab_terms):
    relations_to_extract = ["IsA", "RelatedTo", "Synonym"]
    vocabulary = fetch_data_from_concept_net(vocab_terms, relations_to_extract, use_proxy =True)
    
    return vocabulary

# In[]

def extract_terms(data_dir):
    #data_dir = Path("D:/Data Sciences/Upswing Quest/Content_Categorization/Content Categorization_Use Case 1/data")
    files = list(data_dir.iterdir())
    
    important_phrases_from_adjacency_list = {}
    noun_terms = {}
    important_terms_from_idf = {}
    data = {}
    
    for file in files:
        file = Path(file)
        if file.is_file():
            print("\n\n\nreading file: {0}".format(file))
            tmp_data = None
            if file.suffix == '.docx':
                tmp_data = docx2txt.process(file)
            elif file.suffix == '.doc':
                txt_file_name = file.name
                txt_file_name = 'data/txt/' + str(txt_file_name.replace(file.suffix, '.txt'))
                Path(txt_file_name).touch()
                cmd = r'C:\antiword_v2\antiword\antiword.exe -f ' + data_dir.name + '/' + file.name + ' > ' + txt_file_name
                subprocess.Popen(cmd, shell=True)
                tmp_file = 'data/txt/' + file.name.replace(file.suffix, '.txt')
                
                txt_file = open(tmp_file, 'r')
                tmp_data = txt_file.read()
            else:
                print("\n{0} is a {1} file".format(file, file.suffix))
                
            if tmp_data is not None:
                data[file.name] = tmp_data
                # Use Case 3 (find important keywords)
                important_terms_from_idf[file.name] = extract_important_features_using_tf_idf(data[file.name], ngrams= 1, term_count = 20, consolidate = True, count_vectorizer = True, term_freq_threshold = 5)
                # Use case 4 (find n-grams)
                important_phrases_from_adjacency_list[file.name] = extract_phrases(data[file.name], requested_terms= 60, term_freq_threshold= 2, spell_correction=False, consolidated = True)                
                # Use Case 2 (find important nouns)
                # noun_terms[file.name] = list(set(filter_nouns(list(set(chain(*[list(chain(*important_terms_from_idf[file.name].values())), important_phrases_from_adjacency_list[file.name]]))))))
                noun_terms[file.name] = list(set(filter_nouns_v1(list(set(chain(*[list(chain(*important_terms_from_idf[file.name].values())), important_phrases_from_adjacency_list[file.name]])))).values()))
                
                print("\n\n\n\n doc : {0}, \n\nnouns : {1}, \n\nimportant_terms from adjacency: {2}, \
                      \n\ntf-idf : {3} \n\n".format(file.name, noun_terms[file.name], important_phrases_from_adjacency_list[file.name],  
                      important_terms_from_idf[file.name]))
                
    return important_phrases_from_adjacency_list, noun_terms, important_terms_from_idf, data

# In[]
def extract_documents_containing_relevant_data(noun_terms, relations_for_nouns, vocabulary, use_proxy = False):
    nouns_data = {}
    match_set = {}
    
    for doc, nouns in noun_terms.items():
        nouns_data[doc] = fetch_data_from_concept_net([x.replace(' ', '_') for x in nouns], relations_for_nouns, use_proxy = False)
        tmp_consolidated_nouns_in_document = list(nouns_data[doc].values())
        tmp_consolidated_nouns_in_document = set(chain(*map(lambda x : set(chain(*tmp_consolidated_nouns_in_document[x].values())), range(0, len(tmp_consolidated_nouns_in_document) -1))))
        
        for item in vocabulary:
            tmp_matches = []
            tmp_matches = tmp_consolidated_nouns_in_document.intersection(set(chain(*vocabulary[item].values())))
            
            # if len(tmp_matches) > 0:
            if doc not in match_set:
                match_set[doc] = {}
            match_set[doc][item] = tmp_matches
    
    print("documents matched : {0} ".format(match_set.keys()))
    return match_set

# In[]
def extract_sentences(doc, search_terms):
    found_sentences = []
    doc = doc.split('\n')
    sentences = []
    search_terms = set(search_terms)
    lemma = WordNetLemmatizer()
    
    for para in doc:
        sentences.extend(sent_tokenize(para))
    
    #found_sentences.extend([sent for sent in sentences if len(set(search_terms).intersection(set(word_tokenize(sent).extend([singularize(x) for x in word_tokenize(sent)])))) > 0])
    for sentence in sentences:
        result = word_tokenize(sentence)
        result.extend([lemma.lemmatize(x) for x in word_tokenize(sentence)])
        result = len(set(result).intersection(search_terms))
        if result:
            found_sentences.append(sentence)
    return found_sentences

# In[]
    
def validate_sentences(sentences, search_terms, vocabulary, pos, words_to_have = (), words_to_ignore = ()):
    nlp = spacy.load('en')
    consolidated_vocabulary = set(chain(*list(chain(*[list(vocabulary[item].values()) for item in list(vocabulary.keys())]))))
    
    consolidated_vocabulary = consolidated_vocabulary.difference(words_to_ignore)
    consolidated_vocabulary = consolidated_vocabulary.union(words_to_have)
    
    sentences_to_remove = []
    
    for sentence in sentences:
        tmp = nlp(sentence)
        tokens = []
        for token in tmp:
            if token.text not in search_terms and token.pos_ == pos and (token.lemma_ in consolidated_vocabulary or token.text in consolidated_vocabulary):
                tokens.append((token.text, token.pos_))
            
            """ print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}".format(
                token.text,
                token.idx,
                token.lemma_,
                token.is_punct,
                token.is_space,
                token.shape_,
                token.pos_,
                token.tag_
            ))
            """
            
        if len(tokens) == 0:
            sentences_to_remove.append(sentence)
    
    return list(set(sentences).difference(set(sentences_to_remove)))
      


# In[]
def process_data(data_dir):
    logging.info('process whole data')
    # find Entities and keywords for the respective documents
    #data_dir = Path("D:/Data Sciences/Upswing Quest/Content_Categorization/Content Categorization_Use Case 1/data")
    phrases, nouns, keywords, data = extract_terms(data_dir)
    
    #dic_dir = Path("D:/Data Sciences/Upswing Quest/Content_Categorization/Content Categorization_Use Case 1/dictionary")
    dic_dir = Path("dictionary")
    dictionaries = list(dic_dir.iterdir())
    
    medical_corpus = set()
    tmp_data = None
    
    for dictionary in dictionaries:
        tmp_data = pd.read_fwf(Path(dictionary), header=None)
        medical_corpus = medical_corpus.union(set(tmp_data[0]))
         
    for doc, noun_terms in nouns.items():
        nouns[doc] = set.intersection(set(noun_terms), medical_corpus)
        
    # find documents which do not have the statements on disclosure or consent
    
    vocab_terms = ["human", "animal"]
    
    vocabulary = fetch_data_from_concept_net(vocab_terms, ['IsA', 'RelatedTo', 'Synonym'], False)
    
    # remove common terms from animal vocabulary
    for category, words in vocabulary['animal'].items():
        vocabulary['animal'][category] = list(set(words).difference(set(vocabulary['human'][category])))
    
    
    relations_for_nouns = ['IsA']
    match_set = extract_documents_containing_relevant_data(nouns, relations_for_nouns, vocabulary)
    
    # filter documents which contain (and not contain) info about species
    documents_with_info_on_species = list(set(x for x, y in match_set.items() for y1, y2 in y.items() if len(y2) != 0))
    documents_with_no_info_on_species = list(set(x for x, y in match_set.items() for y1, y2 in y.items() if len(y2) == 0))
    
    documents_with_info_on_species = {}
    documents_with_no_info_on_species = {}
    for species in vocab_terms:
        documents_with_info_on_species[species] = [doc for doc in list(match_set.keys()) if len(match_set[doc][species]) > 0]
        documents_with_no_info_on_species[species] = [doc for doc in list(match_set.keys()) if len(match_set[doc][species]) == 0]
    
    # find sentences in documents related to a species
    species_related_sentences = {}
    species_validation_vocab = fetch_data_from_concept_net(['experiment', 'trial', 'test', 'tested', 'study', 'examination', 'diagnosis'], ['IsA', 'Synonym'], False)
    
    for species, docs in documents_with_info_on_species.items():
        for doc in docs:
            if doc not in list(species_related_sentences.keys()):
                species_related_sentences[doc] = {}
            sentence_vocab = vocabulary[species]['Synonym'] + vocabulary[species]['IsA']
            species_related_sentences[doc][species] = extract_sentences(data[doc], sentence_vocab)
            # filter sentences which relate to trials or experiments
            species_related_sentences[doc][species] = validate_sentences(species_related_sentences[doc][species], search_terms = sentence_vocab, vocabulary = species_validation_vocab, pos = 'VERB')
    
    for species in vocab_terms:
        documents_with_info_on_species[species].extend([doc for doc, data in species_related_sentences.items() if species in species_related_sentences[doc] and len(list(chain(*species_related_sentences[doc][species]))) > 0])
        
    # prepare vocabulary for synonyms of consent and disclosure
    vocabulary_for_words_to_find = fetch_data_from_concept_net(['consent', 'disclosure', 'approve', 'Ethic', 'ethical'], ['Synonym'], use_proxy = False)
    
    related_sentences_in_docs = {}
    # build search terms consolidated for the synonyms of consent and disclosure
    search_terms = list(chain(*list(map(lambda x : list(chain(*list(vocabulary_for_words_to_find.values())[x].values())), range(0, len(vocabulary_for_words_to_find))))))
    
    faulty_documents = {}
    # find sentences (in the documents with info on species) which contain any synonym of disclosure or consent
    for species in vocab_terms:
        if species not in related_sentences_in_docs:
            related_sentences_in_docs[species] = {}
        for doc in documents_with_info_on_species[species]:
            if doc not in related_sentences_in_docs['human']:
                related_sentences_in_docs[species][doc] = extract_sentences(data[doc], search_terms)
            # words_to_ignore = set(['point'])
            # words_to_have = set(['participant', 'patient', 'subject'])
            # related_sentences_in_docs[species][doc] = validate_sentences(related_sentences_in_docs[doc], search_terms, vocabulary, 'NOUN', words_to_have, words_to_ignore)
            
        # filter documents which do not contain disclosure or consent.

        faulty_documents[species] = [k for k,v in related_sentences_in_docs[species].items() if len(v) == 0]
              
    print("faulty documents: {0}".format(faulty_documents))
    
    
    # find common topics amongst documents
    nouns_to_check = set(chain(*list(nouns.values())))
    doc_groups = {}
    
    for tmp_noun in nouns_to_check:
        for doc, noun_terms in nouns.items():
            if tmp_noun in noun_terms:
                if tmp_noun not in doc_groups:
                    doc_groups[tmp_noun] = []
                doc_groups[tmp_noun].append(doc)
    
    multi_doc_groups = deepcopy(doc_groups)
    for term, docs in multi_doc_groups.items():
        new_term_set = set([k for k,v in multi_doc_groups.items() if v == docs])
        if ", ".join(new_term_set) not in doc_groups:
            doc_groups[", ".join(new_term_set)] = docs
    
    doc_groups = dict(filter(lambda x : len(x[1]) > 1, doc_groups.items()))

    write_output(faulty_documents, related_sentences_in_docs, documents_with_info_on_species, documents_with_no_info_on_species)
    
    return nouns, doc_groups, faulty_documents, keywords, match_set, related_sentences_in_docs, documents_with_info_on_species, documents_with_no_info_on_species

# In[]
    
def write_output(faulty_documents, related_sentences_in_docs, documents_with_info_on_species, documents_with_no_info_on_species):
    with open('output/documents_with_info_on_species.json', 'w') as fp:
        json.dump(documents_with_info_on_species, fp)

    with open('output/related_sentences_in_docs.json', 'w') as fp:
        json.dump(related_sentences_in_docs, fp)

    with open('output/documents_with_no_info_on_species.json', 'w') as fp:
        json.dump(documents_with_no_info_on_species, fp)

    with open('output/faulty_documents.json', 'w') as fp:
        json.dump(faulty_documents, fp)
    
    return        
# In[]

data_dir = Path("data")
animal_training_data_dir = Path("data/training/Animal_training")
human_training_data_dir = Path("data/training/Human_Training")
test_dir = Path("data/test/Use case 1")

#test_data_dir = 
# process_data(data_dir)     
#nouns, doc_groups, faulty_documents, keywords, match_set, related_sentences_in_docs, documents_with_info_on_species, documents_with_no_info_on_species = process_data(test_dir)


# In[]
"""
logging.basicConfig(filename = "logs/nlp_utilities.log", filemode='a', format = "%(asctime)s:%(msecs)03d %(levelname)s %(filename)s:%(lineno)d %(message)s"
	, datefmt = "%Y-%m-%d %H:%M:%S", level = 'logging.DEBUG')
"""