<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-016-0304-3">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>304</ArticleID>
            <ArticleDOI>10.1007/s13238-016-0304-3</ArticleDOI>
            <ArticleSequenceNumber>1</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Beyond the classroom lecture: Liang Wang’s personal war on tuberculosis in China</ArticleTitle>
            <ArticleCategory>Recollection</ArticleCategory>
            <ArticleFirstPage>697</ArticleFirstPage>
            <ArticleLastPage>698</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2016</Year>
                <Month>7</Month>
                <Day>25</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2016</Year>
                <Month>8</Month>
                <Day>2</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>The Author(s)</CopyrightHolderName>
              <CopyrightYear>2016</CopyrightYear>
              <License Version="4.0" Type="OpenAccess" SubType="CC BY">
                <SimplePara>
                           <Emphasis Type="Bold">Open Access</Emphasis>This article is distributed under the terms of the Creative Commons Attribution 4.0 International License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/4.0/</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/4.0/"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided you give appropriate credit to the original author(s) and the source, provide a link to the Creative Commons license, and indicate if changes were made.</SimplePara>
              </License>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author ID="Au1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Ming</GivenName>
                  <FamilyName>Li</FamilyName>
                </AuthorName>
              </Author>
              <Author ID="Au2" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Xiaomei</GivenName>
                  <FamilyName>Hu</FamilyName>
                </AuthorName>
              </Author>
              <Author ID="Au3" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Fuquan</GivenName>
                  <FamilyName>Hu</FamilyName>
                </AuthorName>
              </Author>
              <Author ID="Au4" CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Xiancai</GivenName>
                  <FamilyName>Rao</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>raoxiancai@126.com</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.410570.7</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000417606682</OrgID>
                <OrgDivision>Department of Microbiology, College of Basic Medical Sciences</OrgDivision>
                <OrgName>Third Military Medical University</OrgName>
                <OrgAddress>
                  <City>Chongqing</City>
                  <Postcode>400038</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Tuberculosis</Keyword>
              <Keyword>Monosodium Glutamate</Keyword>
              <Keyword>Prevalent Infectious Disease</Keyword>
              <Keyword>National Vaccination Program</Keyword>
              <Keyword>Sichuan Region</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para ID="Par1">Tuberculosis, historically one of the deadliest and most prevalent infectious diseases, is caused by the bacterium <Emphasis Type="Italic">Mycobacterium tuberculosis</Emphasis>. It is estimated that one-third of the world’s population is infected by this agent (Zumla et al., <CitationRef CitationID="CR4">2014</CitationRef>), and tuberculosis is therefore a major topic for medical students. <Emphasis Type="Italic">M. tuberculosis</Emphasis> is a small aerobic bacillus, with a remarkably high lipid-content cell wall that plays a critical role in its pathogenicity. Although the efficacy of the Bacillus Calmette-Guérin (BCG) vaccine for preventing tuberculosis continues to be debated (Villarreal-Ramos, <CitationRef CitationID="CR2">2009</CitationRef>), live attenuated BCG has been the only approved vaccine against <Emphasis Type="Italic">M. tuberculosis</Emphasis> infection since its introduction in 1921, and is still administered routinely around the world, particularly in developing countries. In China, BCG has been included in the national vaccination program for newborns and children under 15 since the mid-1970s. After decades of intensive effort, remarkable progress has been made in the fight against tuberculosis in China. This great achievement was made possible by Liang Wang, a physician who pioneered the introduction of the BCG vaccine to China in the early 1930s and spearheaded China’s fight against tuberculosis (Yan, <CitationRef CitationID="CR3">2003</CitationRef>; Dai et al., <CitationRef CitationID="CR1">2014</CitationRef>).</Para>
            <Para ID="Par2">Although his accomplishments are frequently discussed in the medical school classroom, most students are unfamiliar with Liang Wang’s personal story and the significant challenges he faced in his battle against tuberculosis. Liang Wang was born on May 5, 1891 in Chengdu in the Sichuan Province. He lost his father at very young age. After graduating from the Hanoi Medical School in Vietnam in 1913, he entered private practice in the Yunnan and Sichuan regions of China. In the early 20th century, tuberculosis was the leading cause of death. Because there was no known cure, the severe health threat posed by tuberculosis was considered to be as serious as that posed by cancer today. Wang’s older brother and younger sister both died of tuberculosis. As a young doctor, Liang Wang determined to devote himself completely to the fight against tuberculosis.</Para>
            <Para ID="Par3">In 1924, Albert Calmette and Camille Guérin from the Pasteur Institute in France published their research results on the BCG vaccine, demonstrating its efficacy in the prevention of tuberculosis. One year later, Liang Wang learned about BCG and began fundraising to support a trip to France. With the help of France’s foreign ministry, Wang was able to visit the Pasteur Institute in 1931, where he began his research career under the personal guidance of Guérin. Wang’s modesty and eagerness to learn were quickly appreciated. During his two years at the Pasteur, Wang completed four research papers, three of which focused on BCG and the culture of <Emphasis Type="Italic">M. tuberculosis</Emphasis>. In the summer 1933, Liang Wang returned to China with the BCG seed strain and lab equipment. He established a microbiology laboratory while working as a physician in a private hospital in Chongqing. He committed much of his spare time to BCG culture preparation and developed the first batch of BCG vaccine in China. Beginning October 1933 through August 1935, 248 infants were inoculated with the BCG vaccine manufactured by Liang Wang. No adverse reactions were observed. Crucially, BCG vaccination conferred significant protection against the development of tuberculosis, and also offered some degree of immunity against other common infectious diseases.</Para>
            <Para ID="Par4">However, just as Liang Wang was planning to expand and promote BCG vaccination, the Japanese invaded China in 1937, beginning the Anti-Japanese War, also known as the Second Sino-Japanese War. Wang’s lab was forced to close, and the development of the vaccination project halted for more than a decade until the Japanese were defeated and the People’s Republic of China was established.</Para>
            <Para ID="Par5">The new Department of Health, opened by the Southwest Military Administrative Committee in Chongqing, provided Liang Wang with the opportunity continuing his work. He was invited to participate in the first national health working conference in 1950, and was authorized by the central government to found the Southwest Chinese BCG Production and Research Institute in Chongqing under his leadership. Beginning August 1951, the health ministry held three training classes to promote the use of Wang’s BCG vaccine, and BCG vaccination quickly became popularized. In 1956, the Southwest BCG Production and Research Institute was incorporated into the Chengdu Institute of Biological Products (Fig. <InternalRef RefID="Fig1">1</InternalRef>), where Liang Wang devoted the later stages of his professional career to BCG research and manufacturing.<Figure ID="Fig1" Float="Yes" Category="Standard">
                <Caption xml:lang="en" Language="En">
                  <CaptionNumber>Figure 1</CaptionNumber>
                  <CaptionContent>
                    <SimplePara>The Chengdu Institute of Biological Products in its early days</SimplePara>
                  </CaptionContent>
                </Caption>
                <MediaObject ID="MO1">
                  <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1007%2Fs13238-016-0304-3/MediaObjects/13238_2016_304_Fig1_HTML.jpg"/>
                </MediaObject>
              </Figure>
                  </Para>
            <Para ID="Par6">For the next 30 years, Wang insisted on the priority of clinical application and the principle of combining theory with practice. By focusing on three specific areas of research, he contributed significantly to the refinement of BCG-based immunization strategies to control tuberculosis in China.</Para>
            <Para ID="Par7">First, Wang emphasized the improvement of BCG vaccine quality. Wang optimized the chemical composition of BCG by decreasing the content of monosodium glutamate from 8% to 6% and glycerol from 6% to 5%, making it more suitable for live attenuated BCG. He also determined the most effective concentration of sucrose (8%) for the protection of lyophilized BCG preparations by using the optimum seeking method. To determine the shelf life for the vaccine, Wang measured the loss in potency for 10 different batches of BCG vaccine after storage at 4°C for 6 weeks, and confirmed that 26% of bacteria remained viable, a level sufficient to induce the body’s immune response.</Para>
            <Para ID="Par8">Second, Wang began a process to isolate a superior BCG strain for vaccine production. Due to different passage methods and culture conditions, the BCG vaccine used in different countries varied. In collaboration with others, both locally and nationally, Liang Wang performed a two-year-long BCG breeding program in which he compared a large number of domestic and international BCG strains and established efficient detection protocols. His achievements provided the experimental basis for the selection of the Shanghai D2 strain from Denmark as the foundation for BCG vaccine production.</Para>
            <Para ID="Par9">Third, Wang actively investigated the immunologic mechanisms underlying BCG efficacy. In an analysis of various blood constituents in BCG-immunized animals, he observed that the number of leukocytes increased significantly 3 days after vaccination, peaked on day 5, and returned to normal on day 12. Lymphocytes were the most markedly increased cell type. Through many experiments, he determined that the cytophagic capacity of reticuloendothelial cells and phagocytes were remarkably enhanced in BCG-vaccinated animals, and serum immunoglobulins were also significantly elevated. Wang also noted that the protection conferred by BCG vaccination persists even after allergies have disappeared in immunized animals. His work demonstrated that BCG induces not only cell-mediated immunity but also humoral immunity, consistent with the non-specific immunity observed in infants enrolled in Wang’s first vaccination project in 1935. Furthermore, Wang also found that BCG-immunized animals exhibited an enhanced ability to resist infection caused by <Emphasis Type="Italic">Staphylococcus aureus</Emphasis> and other species from the genus <Emphasis Type="Italic">Salmonella</Emphasis> and <Emphasis Type="Italic">Streptococcus</Emphasis>. This remarkable discovery provided the foundation for the clinical application of BCG and its active ingredients as immunological treatments for cancer and other diseases, and defined the future direction for development of a series of therapeutic BCG-associated products.</Para>
            <Para ID="Par10">With his strong work ethic and rigorous attitude toward research, Liang Wang devoted himself wholeheartedly to his anti-tuberculosis career. He was invariably courteous, amiable, and easy to approach, and paid close attention to the cultivation of young scientists and physicians. He remained active as a writer and lecturer even when nearly 90 years old. In 1983, an academic symposium was organized by the Chengdu Institute of Biological Products to celebrate the 50th anniversary of Wang’s professional career and his 92th birthday.</Para>
            <Para ID="Par11">On August 31, 1985, Liang Wang died at the age of 94 in his hometown of Chengdu.
</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <BibBook>
                  <BibAuthorName>
                    <Initials>Z</Initials>
                    <FamilyName>Dai</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Xiao</FamilyName>
                  </BibAuthorName>
                  <Year>2013</Year>
                  <BookTitle>A history of tuberculosis</BookTitle>
                  <PublisherName>People’s Medical Publishing</PublisherName>
                  <PublisherLocation>Shelton</PublisherLocation>
                </BibBook>
                <BibUnstructured>Dai Z, Xiao D (2013) A history of tuberculosis. People’s Medical Publishing, Shelton</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>B</Initials>
                    <FamilyName>Villarreal-Ramos</FamilyName>
                  </BibAuthorName>
                  <Year>2009</Year>
                  <ArticleTitle xml:lang="en" Language="En">Towards improved understanding of protective mechanisms induced by the BCG vaccine</ArticleTitle>
                  <JournalTitle>Expert Rev Vaccines</JournalTitle>
                  <VolumeID>8</VolumeID>
                  <FirstPage>1531</FirstPage>
                  <LastPage>1534</LastPage>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD1MXhtleisbvP</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1586/erv.09.109</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>19863244</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Villarreal-Ramos B (2009) Towards improved understanding of protective mechanisms induced by the BCG vaccine. Expert Rev Vaccines 8:1531–1534</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>Z</Initials>
                    <FamilyName>Yan</FamilyName>
                  </BibAuthorName>
                  <Year>2003</Year>
                  <ArticleTitle xml:lang="en" Language="En">The founder of China’s BCG career-Liang Wang (1891–1985)</ArticleTitle>
                  <JournalTitle>Chin J Microbiol Immunol</JournalTitle>
                  <VolumeID>1</VolumeID>
                  <FirstPage>1</FirstPage>
                  <LastPage>2</LastPage>
                </BibArticle>
                <BibUnstructured>Yan Z (2003) The founder of China’s BCG career-Liang Wang (1891–1985). Chin J Microbiol Immunol 1:1–2</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <BibArticle>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Zumla</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>George</FamilyName>
                  </BibAuthorName>
                  <Etal/>
                  <Year>2015</Year>
                  <ArticleTitle xml:lang="en" Language="En">The WHO 2014 global tuberculosis report-further to go</ArticleTitle>
                  <JournalTitle>Lancet Glob Health</JournalTitle>
                  <VolumeID>3</VolumeID>
                  <FirstPage>e10</FirstPage>
                  <LastPage>e12</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/S2214-109X(14)70361-4</Handle>
                  </Occurrence>
                  <Occurrence Type="PID">
                    <Handle>25539957</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Zumla A, George A et al (2015) The WHO 2014 global tuberculosis report-further to go. Lancet Glob Health 3:e10–e12</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
<JobSheet xmlns="http://www.springer.com/app/jobsheet">
              <EditorialManuscriptNumber>16915.zip</EditorialManuscriptNumber>
            </JobSheet></Publisher>


