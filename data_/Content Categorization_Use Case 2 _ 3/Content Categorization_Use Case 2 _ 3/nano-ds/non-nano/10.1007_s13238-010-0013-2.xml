<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="s13238-010-0013-2">
          <ArticleInfo xml:lang="en" TocLevels="0" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="OriginalPaper">
            <ArticleID>13</ArticleID>
            <ArticleDOI>10.1007/s13238-010-0013-2</ArticleDOI>
            <ArticleSequenceNumber>3</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" Language="En">Protein science research in China</ArticleTitle>
            <ArticleCategory>Perspective</ArticleCategory>
            <ArticleFirstPage>4</ArticleFirstPage>
            <ArticleLastPage>5</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2010</Year>
                <Month>2</Month>
                <Day>4</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2010</Year>
                <Month>2</Month>
                <Day>7</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Higher Education Press and Springer-Verlag Berlin Heidelberg</CopyrightHolderName>
              <CopyrightYear>2010</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="Regular">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Ming</GivenName>
                  <FamilyName>Sun</FamilyName>
                </AuthorName>
              </Author>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Rui-Ming</GivenName>
                  <FamilyName>Xu</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>rmxu@sun5.ibp.ac.cn</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.9227.e</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000119573309</OrgID>
                <OrgDivision>Institute of Biophysics</OrgDivision>
                <OrgName>Chinese Academy of Sciences</OrgName>
                <OrgAddress>
                  <City>Beijing</City>
                  <Postcode>100101</Postcode>
                  <Country Code="CN">China</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <Abstract xml:lang="en" Language="En" ID="Abs1">
              <Heading>Abstract</Heading>
              <Para>Proteins are major executors of life processes, carrying out essential and nonessential functions inside and outside of the cell, in species ranging from simple unicellular organisms to mammals. Thus, not surprisingly, studies of structure and function of proteins span the entire spectrum of molecular biology and modern biomedical sciences in general. Due to historical reasons, protein science research in China was isolated and limited in scope until late 1970s. In the last two decades, China has seen an outburst of research activities, government initiatives, and aggregation of human talents in protein science research. This article provides an overview of major initiatives in research funding, investment in infrastructures, and research forces for protein science research in China.</Para>
            </Abstract>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Protein Research</Keyword>
              <Keyword>Senior Scholar</Keyword>
              <Keyword>Technology Development Plan</Keyword>
              <Keyword>Government Research Institution</Keyword>
              <Keyword>Human Talent</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="PDF">
            <page id="pg01">Protein &amp; Cell 2010, 1(1): 4–5 DOI 10.1007/s13238-010-0013-2 Protein &amp; Cell PERSPECTIVE Protein science research in China Ming Sun, Rui-Ming Xu (✉) Institute of Biophysics, Chinese Academy of Sciences, Beijing 100101, China ✉ Correspondence: rmxu@sun5.ibp.ac.cn Proteins are major executors of life processes, carrying out essential and nonessential functions inside and outside of the cell, in species ranging from simple unicellular organisms to mammals. Thus, not surpris- ingly, studies of structure and function of proteins span the entire spectrum of molecular biology and modern biomedical sciences in general. Due to historical rea- sons, protein science research in China was isolated and limited in scope until late 1970s. In the last two decades, China has seen an outburst of research activities, government initiatives, and aggregation of human talents in protein science research. This article provides an overview of major initiatives in research funding, invest- ment in infrastructures, and research forces for protein science research in China. NATIONAL FUNDING MECHANISMS In 2006, the Protein Research Program (PRP) was launched as part of the four basic science research initiatives set forth in the “Mid- and Long-Term National Science and Technology Development Plan” by the Chinese central government. This program supports research projects in the following cate- gories: structure biology of macromolecular complexes, proteomics, proteins involved in major human diseases, development of new technique and methodology of protein research. A PRP grant supports a team of investigators in the way much like a Program Project grant funded by the National Institute of Health (NIH) in the US. To date, 37 projects have been funded, with a total expenditure in excess of $100 millions. By far, PRP is the only national program that speciﬁcally target protein science research. As a national funding source, the current budget for the program is rather limited. Each year, approximately 8–10 projects will be funded, with a budget per project in the range of $2.9–4.4 millions. At present, PRP is operating under the umbrella of National Basic Research Program of China, i. e., the 973 program, which in the last 12 years has spent over $1.6 billion supporting basic science research in all scientiﬁc disciplines, including the PRP projects. Separately, the 973 program also supports projects in broadly deﬁned biomedical sciences. For example, it has supported projects in membrane protein, non-coding RNA, and epigenetics research. The National High-tech R&amp;D Program, i. e., the 863 program, which has supported several large-scale projects in proteomics, structural genomics, and pharmaceutical proteins, may also support protein research projects with application and market potential. All of the above research programs are administrated by the Ministry Of Science and Technology of China (MOST). In addition, the National Natural Science Foundation of China (NSFC) funds smaller grants, in the range of approxi- mately $50,000–300,000 per grant, to individual investigators. Unlike the MOST grants, most NSFC grants are open to all areas of scientiﬁc inquiries, with the exception of Special Emphasis Programs, in which case the agency issues call for applications in speciﬁc areas. For example, NSFC initiated the program in Cell Programming and Reprogramming last year and funded 7 large projects and a number of smaller grants. INFRASTRUCTURE China has approved the establishment of two national facilities for protein research, one to be built in Shanghai and the other in Beijing. The Shanghai facility will be located next to the Shanghai Synchrotron, which houses a beamline for protein X-ray crystallography and has begun operation since May, 2008. The facility will be equipped with three new beamlines for X-ray crystallography at the nearby synchrotron and an assortment of equipments for structural biology and proteo- mics. The Beijing facility will also serve the need of research communities in structural biology, proteomics and protein functional analyses. Both facilities have an approximate budget of $80 millions and are expected to be in operation © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010</page>
            <page id="pg02">Protein science research in China Protein &amp; Cell within next couple of years. At present, Chinese Academy of Sciences (CAS) is operating a similar facility located in the Institute of Biophysics in Beijing. CAS has invested more than $50 millions for the acquisition of capital equipments of protein research at the site, and the facility is open to the research community national wide. ORGANIZATIONS Apart from conventional research entities in universities and government research institutions, a unique research organi- zation in China is the State Key Laboratory system. Top research units in representative areas are selected and given the designation by the MOST, which provides an operation budget of $1.5 million per year for each State Key Laboratory. Two of earliest established State Key Laboratories with strong emphasis on protein research are State Key Laboratories of Biomacromolecules and Molecular Biology, located within Institute of Biophysics and Shanghai Institute of Biological Sciences, respectively. At present, the missions of more than a dozen of State Key Laboratories national-wide are related to protein research. At the end of 2006, MOST announced the establishment of the National Laboratory of Protein Science (NLPS), which will be housed in Institute of Biophysics. A national laboratory is considered the highest-caliber research institution in a given research area, which is determined by national strategic interest. The NLPS is the ﬁrst national lab for biological science research in China. It is expected that the NLPS, like national laboratories in other disciplines, will have better support, both ﬁnancially and policy wise, that will serve as a magnet for attracting best talents in protein science research. An interesting case is the National Institute of Biological Sciences (NIBS), located in a northwest suburb of Beijing and headed by the renowned biochemist Dr. Xiaodong Wang of University of Texas Southwestern Medical Center. The NIBS differs from universities and CAS Institutes in both admini- strative and funding channels. It receives a single large budget from the government, and the principal investigators are relieved from the burden of applying for external grants, and they also enjoy a better salary and beneﬁts package compared to their peers at universities and government research institutions. These factors have contributed the success of NIBS in recruiting a pool of outstanding young scientists. RESEARCH FORCE Thirty years ago, China began to send scholars and students to the West for training and education. To date, hundreds of thousands of scholars and students have studied and worked in the US and Europe. With the sustained economic growth and drastically improved research environment and condition in the last decades, many scholars who remained abroad after their study are returning to China. At major research universities and CAS institutes, one will ﬁnd that a vast majority of the faculties and principal investigators in biological sciences have had oversea experiences one way or another. Several ministry-level “talents” plans, which offer relatively attractive start-up and beneﬁts packages, should be credited for the present-day success of attracting expatriate scientists. The 100 Talent Program of CAS started in 1994, and has attracted approximately 1600 young scientists to CAS, of whom about 1200 are recruited from overseas. The Ministry of Education’s “Changjiang Scholar” program started in 1998 and have attracted more than 1400 scholars to universities nation-wide. Many established investigators and young rising stars of protein science research in China beneﬁted greatly from these “talent” plans. Most of the scholars returned to China through the aforementioned plans were new PhDs, postdoctoral fellows or junior faculties abroad. Starting at the end of last year, the central government initiated a plan to recruit senior faculties to China. The plan, also known as the “Thousand people plan”, aims at attracting approximately 1000 senior scholars, high- level managers, or entrepreneurs to China over a period of 5–10 years. Approximately half of the recruits are academics, and among the more than 300 recruits announced over 10% are scholars with expertise in protein science and biomedical sciences in general. Some familiar names in the list include Dr. Xiaodong Wang of UT Southwestern, who will return to China to head NIBS full time, and Dr. Yigong Shi, a structural biologist from Princeton University, who returned to Tsinghua University in Beijing last year to become the dean of School of Life Sciences. It is anticipated that the senior scholars, together with the existing research force in the country, will greatly strengthen protein science research in China. PERSPECTIVE From the structure determination of light harvesting complex and key proteins of the SARS coronavirus and the inﬂuenza H5N1 virus, to the discovery of an important regulator of p53; and from the human liver proteomics project to high resolution imaging of neuronal proteins in nematodes, we have seen signiﬁcant accomplishment of protein science research in China in the last few years. However, as seen above, for a country of the size of China, the resources in support of protein research is still rather limited. The research force in this important area of biomedical research also pales in compar- ison to that in developed countries. As the economic and geopolitical inﬂuence of China increase, its competitive edge in science and technology has to follow in pace, and there is no exception in protein science research. With the commitment to promote protein science research as outlined in the “Mid- and Long-Term National Science and Technology Development Plan”, and the increased emphasis on human talents in science and technology, we have every reason to believe that the spring of protein science research in China is upon us. © Higher Education Press and Springer-Verlag Berlin Heidelberg 2010 5</page>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


