# -*- coding: utf-8 -*-
"""
Created on Sat Sep  1 18:35:14 2018

@author: isgupta
"""
# In[]
""" 
    import packages
"""
from pymongo import MongoClient
import requests
import copy
from nltk import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from pathlib import Path
import docx2txt
from ordered_set import OrderedSet
import textract

# In[]
def extract_data(data_dir):
    data = {}
    files = list(data_dir.iterdir())
    for file in files:
        file = Path(file)
        if file.is_file():
            print("\n\n\nreading file: {0}".format(file))
            tmp_data = None
            if file.suffix == '.docx':
                tmp_data = docx2txt.process(file)
            elif file.suffix == '.doc':
                new_file = 'data/txt/' + str(file.name.replace(file.suffix, '.txt'))
                Path(new_file).touch()
                cmd = r'C:\antiword_v2\antiword\antiword.exe -f ' + data_dir.name + '/' + file.name + ' > ' + new_file
                subprocess.Popen(cmd, shell=True)
                tmp_file = 'data/txt/' + file.name.replace(file.suffix, '.txt')
                tmp_data = textract.process(tmp_file)
            else:
                print("\n{0} is a {1} file".format(file, file.suffix))
                
            if tmp_data is not None:
                data[file.name] = tmp_data
    return data

# In[]

def extract_sentences(doc, search_terms, remove_stop_words = False):
    found_sentences = []
    sentences = sent_tokenize(doc)
    stop_words = set(stopwords.words('english'))
        
    if remove_stop_words:
        found_sentences.extend([(" ".join(OrderedSet(sent.split()).difference(stop_words)), set(search_terms).intersection(set(word_tokenize(sent)))) for sent in sentences if len(set(search_terms).intersection(set(word_tokenize(sent)))) > 0])
    else:
        found_sentences.extend([(sent, set(search_terms).intersection(set(word_tokenize(sent)))) for sent in sentences if len(set(search_terms).intersection(set(word_tokenize(sent)))) > 0])
    
    return found_sentences

# In[]
# build vocabulary of any terms
def fetch_data_from_concept_net(data, relations, use_proxy = True):
    db_name = 'test'
    db_collection = 'concept_net'
    db_address = "localhost:27017"
    db = MongoClient(db_address).get_database(db_name).get_collection(db_collection)
    
    concept_net_uri = "http://api.conceptnet.io"
    concept_net_concept_notation = 'c'
    concept_net_language = 'en'
    concept_net_primary_identifier = '@id'
    
    proxy_ss = "http://10.135.0.29:8080"
    
    if isinstance(data, str):
        data = data.split()
    
    # construct URLs for all words in the requested data
    concept_net_request_iris = dict(map(lambda x : (x, concept_net_uri + '/' + concept_net_concept_notation + '/' + concept_net_language + '/' + x + '?offset=0&limit=1000'), data))
    concept_net_responses = {}

    # fetch and collate response for each word requested in the data
    for word, concept_net_request_iri in concept_net_request_iris.items():
        # check the node in local database:
        concept_net_response = db.find({concept_net_primary_identifier : '/' + concept_net_concept_notation + '/' + concept_net_language + '/' + word})
        
        if concept_net_response.count() == 0:
            print("requesting data for", concept_net_request_iri)
            if use_proxy:
                concept_net_response = requests.get(concept_net_request_iri, proxies = {'http' : proxy_ss}).json()
            else:
                concept_net_response = requests.get(concept_net_request_iri).json()
                
            if concept_net_response is not None:
                tmp_response = copy.deepcopy(concept_net_response)
                while 'view' in list(tmp_response.keys()): 
                    if 'nextPage' in list(tmp_response['view'].keys()):
                        if use_proxy:
                            tmp_response = requests.get(concept_net_uri + tmp_response['view']['nextPage'], proxies = {'http' : proxy_ss}).json()
                        else:
                            tmp_response = requests.get(concept_net_uri + tmp_response['view']['nextPage']).json()
                        concept_net_response['edges'].extend(tmp_response['edges'])
                    else:
                        break
                tmp_response = None
                db.insert_one(concept_net_response)
                concept_net_responses[word] = concept_net_response
        else:
            print("found data for {0} locally".format(word))
            concept_net_responses[word] = concept_net_response.next()
            
    # filter and aggregate concept net data
    concept_net_data = {}
    for word, concept_net_response in concept_net_responses.items():
        for relation in relations:
            end_tags = [tmp['start']['label'] for tmp in concept_net_response['edges'] if tmp['@type'] == 'Edge' and 
                                                    tmp['rel']['label'] == relation and tmp['start']['language'] == concept_net_language and tmp['end']['language'] == concept_net_language]
            start_tags = [tmp['end']['label'] for tmp in concept_net_response['edges'] if tmp['@type'] == 'Edge' and 
                                                    tmp['rel']['label'] == relation and tmp['start']['language'] == concept_net_language and tmp['end']['language'] == concept_net_language]
            
            if len(start_tags) > 0 or len(end_tags) > 0:
                if word not in concept_net_data:
                    concept_net_data[word] = {}
                    
                if relation not in concept_net_data[word]:
                    concept_net_data[word][relation] = []
                    
                concept_net_data[word][relation].extend(start_tags)
                concept_net_data[word][relation].extend(end_tags)
                concept_net_data[word][relation] = list(set(concept_net_data[word][relation]))
                
    return concept_net_data
