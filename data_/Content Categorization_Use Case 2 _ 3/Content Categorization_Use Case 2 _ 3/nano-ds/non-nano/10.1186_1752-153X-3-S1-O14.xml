<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <IssueHeader>
          <EditorGroup>
            <Editor>
              <EditorName DisplayOrder="Western">
                <GivenName>Oellien</GivenName>
                <FamilyName>Frank</FamilyName>
              </EditorName>
            </Editor>
          </EditorGroup>
        </IssueHeader>
        <Article OutputMedium="All" ID="BMC1752-153X-3-S1-O14">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="BriefCommunication">
            <ArticleID>169</ArticleID>
            <ArticleDOI>10.1186/1752-153X-3-S1-O14</ArticleDOI>
            <ArticleCitationID>O14</ArticleCitationID>
            <ArticleSequenceNumber>29</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">The perfect fit? Balancing predictive power and computational complexity for an atomistic model as prerequisite for nano-scale simulations</ArticleTitle>
            <ArticleCategory>Oral presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Maaß et al; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2009</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>A</GivenName>
                  <FamilyName>Maaß</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>TJ</GivenName>
                  <FamilyName>Müller</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>L</GivenName>
                  <FamilyName>Nikitina</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>M</GivenName>
                  <FamilyName>Hülsmann</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgDivision>Fraunhofer Institute SCAI</OrgDivision>
                <OrgName>Schloss Birlinghoven</OrgName>
                <OrgAddress>
                  <Postcode>53754</Postcode>
                  <City>Sankt Augustin</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgAddress>
                  <City>Darmstadt</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Atomistic Model</Keyword>
              <Keyword>Coarse Graining</Keyword>
              <Keyword>Polymer Precursor</Keyword>
              <Keyword>Coarse Grained Model</Keyword>
              <Keyword>Term Objective</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>When aiming at quantitative predictions for materials that require huge system sizes in simulation – such as polymers – models at coarse-grained level are the natural choice. However, capturing the chemical identity of beads that are void of any individual structure resembling the original compound is a critical point for achieving meaningful predictions. As the coarse grained model inherits the features from an atomistic precursor, the latter needs to be most predictive. This may be achieved by calibrating the detailed model carefully to experimental data, thereby enhancing the atomistic model structure with most realistic behaviour [<CitationRef CitationID="CR1">1</CitationRef>].</Para>
            <Para>Diverse strategies like e.g. simplex optimization [<CitationRef CitationID="CR2">2</CitationRef>][<CitationRef CitationID="CR3">3</CitationRef>], interactive design parameter optimization [<CitationRef CitationID="CR4">4</CitationRef>][<CitationRef CitationID="CR5">5</CitationRef>], i.e. local optimization versus global search, have been applied to study the polymer precursor ethylene-epoxide and have been intensively investigated in order to identify a viable route to a perfectly tailored atomistic model. Obviously, each strategy has its profits and limitations, the bottom-line being that a final model needs to yield results that are not only accurate, but also to be robust with respect to transfer between independent program packages. For ethylene-oxide several competing models have been published [<CitationRef CitationID="CR1">1</CitationRef>, <CitationRef CitationID="CR6">6</CitationRef>][<CitationRef CitationID="CR7">7</CitationRef>][<CitationRef CitationID="CR8">8</CitationRef>][<CitationRef CitationID="CR9">9</CitationRef>][<CitationRef CitationID="CR10">10</CitationRef>], however not all are suited for a later coarse graining step. The present field report details newly created models, as well as the tested methods, thus it documents the progress with respect to the long term objective of accurate property predictions for nano-scale simulations.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>TJ</Initials>
                    <FamilyName>Müller</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Roy</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>W</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Maaß</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Reith</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <BookTitle>Fluid Phase Equilib</BookTitle>
                </BibBook>
                <BibUnstructured>Müller TJ, Roy S, Zhao W, Maaß A, Reith D: Fluid Phase Equilib. 2008.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Faller</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>H</Initials>
                    <FamilyName>Schmitz</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>O</Initials>
                    <FamilyName>Biermann</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>F</Initials>
                    <FamilyName>Müller-Plathe</FamilyName>
                  </BibAuthorName>
                  <Year>1999</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Comp Chem</JournalTitle>
                  <VolumeID>20</VolumeID>
                  <IssueID>10</IssueID>
                  <FirstPage>1009</FirstPage>
                  <BibArticleDOI>10.1002/(SICI)1096-987X(19990730)20:10&lt;1009::AID-JCC3&gt;3.0.CO;2-C</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK1MXktFCnsbw%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1002/(SICI)1096-987X(19990730)20:10&lt;1009::AID-JCC3&gt;3.0.CO;2-C</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Faller R, Schmitz H, Biermann O, Müller-Plathe F: J Comp Chem. 1999, 20 (10): 1009-10.1002/(SICI)1096-987X(19990730)20:10&lt;1009::AID-JCC3&gt;3.0.CO;2-C.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>F</Initials>
                    <FamilyName>Müller-Plathe</FamilyName>
                  </BibAuthorName>
                  <Year>1993</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Comput Phys Commun</JournalTitle>
                  <VolumeID>78</VolumeID>
                  <FirstPage>77</FirstPage>
                  <BibArticleDOI>10.1016/0010-4655(93)90144-2</BibArticleDOI>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/0010-4655(93)90144-2</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Müller-Plathe F: Comput Phys Commun. 1993, 78: 77-10.1016/0010-4655(93)90144-2.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>CA</Initials>
                    <FamilyName>Thole</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>L</Initials>
                    <FamilyName>Nikitina</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I</Initials>
                    <FamilyName>Nikitin</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Steffes-Iai</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>K</Initials>
                    <FamilyName>Roel</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Bruns</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <BookTitle>Fraunhofer Publica</BookTitle>
                </BibBook>
                <BibUnstructured>Thole CA, Nikitina L, Nikitin I, Steffes-Iai D, Roel K, Bruns J: Fraunhofer Publica. 2007</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <CitationNumber>5.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>D</Initials>
                    <FamilyName>Steffes-IAi</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>CA</Initials>
                    <FamilyName>Thole</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I</Initials>
                    <FamilyName>Nikitin</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>L</Initials>
                    <FamilyName>Nikitina</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <NoArticleTitle/>
                  <JournalTitle>ERCIM News</JournalTitle>
                  <VolumeID>73</VolumeID>
                  <FirstPage>29</FirstPage>
                </BibArticle>
                <BibUnstructured>Steffes-IAi D, Thole CA, Nikitin I, Nikitina L: ERCIM News. 2008, 73: 29-</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <CitationNumber>6.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>PA</Initials>
                    <FamilyName>Wielopolski</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>ER</Initials>
                    <FamilyName>Smith</FamilyName>
                  </BibAuthorName>
                  <Year>1985</Year>
                  <NoArticleTitle/>
                  <JournalTitle>Mol Physics</JournalTitle>
                  <VolumeID>54</VolumeID>
                  <IssueID>2</IssueID>
                  <FirstPage>467</FirstPage>
                  <BibArticleDOI>10.1080/00268978500100371</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaL2MXhslOnuro%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1080/00268978500100371</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Wielopolski PA, Smith ER: Mol Physics. 1985, 54 (2): 467-10.1080/00268978500100371.</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <CitationNumber>7.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>RD</Initials>
                    <FamilyName>Mountain</FamilyName>
                  </BibAuthorName>
                  <Year>2005</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Phys Chem B</JournalTitle>
                  <VolumeID>109</VolumeID>
                  <FirstPage>13352</FirstPage>
                  <BibArticleDOI>10.1021/jp051379k</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2MXltlGgu78%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/jp051379k</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Mountain RD: J Phys Chem B. 2005, 109: 13352-10.1021/jp051379k.</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <CitationNumber>8.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>X</Initials>
                    <FamilyName>Li</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>L</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Cheng</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>L</Initials>
                    <FamilyName>Liu</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>H</Initials>
                    <FamilyName>Sun</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <BookTitle>Fluid Phase Equilib</BookTitle>
                </BibBook>
                <BibUnstructured>Li X, Zhao L, Cheng T, Liu L, Sun H: Fluid Phase Equilib. 2008.</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <CitationNumber>9.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>JD</Initials>
                    <FamilyName>Olson</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>LC</Initials>
                    <FamilyName>Wilson</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <BookTitle>Fluid Phase Equilibria</BookTitle>
                </BibBook>
                <BibUnstructured>Olson JD, Wilson LC: Fluid Phase Equilibria. 2008.</BibUnstructured>
              </Citation>
              <Citation ID="CR10">
                <CitationNumber>10.</CitationNumber>
                <BibBook>
                  <BibAuthorName>
                    <Initials>B</Initials>
                    <FamilyName>Eckl</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Vrabec</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>H</Initials>
                    <FamilyName>Hasse</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <BookTitle>Fluid Phase Equilibria</BookTitle>
                </BibBook>
                <BibUnstructured>Eckl B, Vrabec J, Hasse H: Fluid Phase Equilibria. 2008.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


