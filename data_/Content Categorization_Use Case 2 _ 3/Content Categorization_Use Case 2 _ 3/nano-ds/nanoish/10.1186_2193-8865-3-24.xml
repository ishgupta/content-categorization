<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
      <dds:site>SPRCOM</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Regular">
        
        <Article OutputMedium="All" ID="BMC2193-8865-3-24">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="Yes" ArticleType="OriginalPaper">
            <ArticleID>15</ArticleID>
            <ArticleDOI>10.1186/2193-8865-3-24</ArticleDOI>
            <ArticleCitationID>24</ArticleCitationID>
            <ArticleSequenceNumber>15</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Synthesis of clay-CNTs nanocomposite</ArticleTitle>
            <ArticleCategory>Original</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>4</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2013</Year>
                <Month>2</Month>
                <Day>15</Day>
              </RegistrationDate>
              <Received>
                <Year>2013</Year>
                <Month>2</Month>
                <Day>15</Day>
              </Received>
              <Accepted>
                <Year>2013</Year>
                <Month>3</Month>
                <Day>10</Day>
              </Accepted>
              <OnlineDate>
                <Year>2013</Year>
                <Month>4</Month>
                <Day>27</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Sedaghat; licensee Springer.</CopyrightHolderName>
              <CopyrightYear>2013</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd. This is an Open Access article distributed under the terms of the Creative Commons Attribution License (<ExternalRef>
                    <RefSource>http://creativecommons.org/licenses/by/2.0</RefSource>
                    <RefTarget TargetType="URL" Address="http://creativecommons.org/licenses/by/2.0"/>
                  </ExternalRef>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>40097</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>1</IssueIDStart>
              <IssueIDEnd>1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Sajjad</GivenName>
                  <FamilyName>Sedaghat</FamilyName>
                </AuthorName>
                <Contact>
                  <Email>sajjadsedaghat@yahoo.com</Email>
                </Contact>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.411463.5</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000107062472</OrgID>
                <OrgDivision>Department of Chemistry</OrgDivision>
                <OrgName>Islamic Azad University, Shahr-eQods Branch</OrgName>
                <OrgAddress>
                  <City>Tehran</City>
                  <Country Code="IR">Iran</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <Abstract xml:lang="en" OutputMedium="All" Language="En" ID="Abs1">
              <Heading>Abstract</Heading>
              <Para>Nanocomposite of clay-carbon nanotubes (CNTs) is prepared by using carboxyl-modified multiwalled carbon nanotubes and amino-functionalized organophilic montmorillonite (clay). The nanocomposite is characterized by scanning electron microscopy to evaluate the morphology. The findings show uniform dispersion of CNTs.</Para>
            </Abstract>
            <KeywordGroup xml:lang="en" OutputMedium="All" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Carbon nanotubes</Keyword>
              <Keyword>Montmorillonite</Keyword>
              <Keyword>Carboxyl-modified multiwalled carbon nanotubes</Keyword>
              <Keyword>Organo clay</Keyword>
            </KeywordGroup>
            <ArticleNote Type="ESMHint">
              <Heading>Electronic supplementary material</Heading>
              <SimplePara>The online version of this article (doi:<ExternalRef>
                  <RefSource>10.1186/2193-8865-3-24</RefSource>
                  <RefTarget TargetType="DOI" Address="10.1186/2193-8865-3-24"/>
                </ExternalRef>) contains supplementary material, which is available to authorized users.</SimplePara>
            </ArticleNote>
          </ArticleHeader>
          <Body Type="XML">
            <Section1 ID="Sec1">
              <Heading>Background</Heading>
              <Para>Carbon nanotubes (CNTs) were first discovered by Iijimain1991 [<CitationRef CitationID="CR1">1</CitationRef>]. Since then, CNTs have been the focus of considerable research because of their physicochemical properties. The first observed CNTs were multiwalled carbon nanotubes (MWCNTs) (see Figure <InternalRef RefID="Fig1">1</InternalRef>a), consisting of up to several tens of graphitic shells with adjacent shell separation of 0.34 nm, diameters of 1 nm, and large length/diameter ratio. MWCNTs can be considered as elongated fullerene [<CitationRef CitationID="CR2">2</CitationRef>]. A few months after Iijima’s discovery, Ebbesen and Ajayan published their work on the bulk synthesis of MWCNTs by arcing graphite electrodes in inert atmospheres at optimum current and pressure conditions. However, it is necessary to note that the first images of CNTs were obtained by Oberlin et al. by pyrolysis of benzene and ferrocene at 1,000°C in the mid-1970s. In 1993, smaller-diameter, single-walled carbon nanotubes (SWCNTs) (see Figure <InternalRef RefID="Fig1">1</InternalRef>b) were independently discovered by Iijima and Ichihashi and Bethune et al. using arc-discharge method and metal (iron and cobalt) as catalysts. CNTs have been termed ‘materials of the twenty-first century’ due to their unique properties such as functional mechanical, thermal, electrical, and optoelectronic properties which depend on the atomic arrangement (how the sheets of graphite are ‘rolled’), the diameter and length of the tubes, the morphology, or nanostructure [<CitationRef CitationID="CR3">3</CitationRef>, <CitationRef CitationID="CR4">4</CitationRef>].<Figure ID="Fig1" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Figure 1</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>
                        <Emphasis Type="Bold">Structure presentations of (a) MWCNT and (b) SWCNT.</Emphasis>
                      </SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_Article_15_Fig1_HTML.jpg"/>
                  </MediaObject>
                </Figure></Para>
              <Para>The adsorption-related applications of CNTs to solve environmental pollution problems have received considerable attention in recent years. CNTs are relatively new adsorbents and hold interesting positions in carbon-based adsorptive materials for many reasons [<CitationRef CitationID="CR5">5</CitationRef>].</Para>
              <Para>The surface functional group density, rather than total surface area, becomes the primary determinant of inorganic pollutant adsorption capacity. The activation of CNTs plays an important role in enhancing the maximum adsorption capacity because of the modification in the surface morphology and surface functional groups. Activation of CNTs under oxidizing conditions with chemicals such as HNO<Subscript>3</Subscript>, KMnO<Subscript>4</Subscript>, H<Subscript>2</Subscript>O<Subscript>2</Subscript>, NaClO, H<Subscript>2</Subscript>SO<Subscript>4</Subscript>, KOH, and NaOH have been widely reported [<CitationRef CitationID="CR6">6</CitationRef>]. After activation, the metallic impurities, amorphous carbon, and catalyst support materials are removed, and the surface characteristics are altered due to the introduction of new functional groups as shown in Figure <InternalRef RefID="Fig2">2</InternalRef>.<Figure ID="Fig2" Float="Yes" Category="Standard">
                  <Caption xml:lang="en" Language="En">
                    <CaptionNumber>Figure 2</CaptionNumber>
                    <CaptionContent>
                      <SimplePara>
                        <Emphasis Type="Bold">Schematic of the major mechanism for sorption of divalent metal ions onto CNT surfaces.</Emphasis>
                      </SimplePara>
                    </CaptionContent>
                  </Caption>
                  <MediaObject>
                    <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_Article_15_Fig2_HTML.jpg"/>
                  </MediaObject>
                </Figure></Para>
              <Para>Nanotechnology is being used or considered in many applications targeted to provide cleaner and more efficient energy supplies and uses. While many of these applications may not affect energy transmission directly, each has the potential to reduce the need for the electricity, petroleum distillate fuel, or natural gas [<CitationRef CitationID="CR7">7</CitationRef>].</Para>
              <Para>Clay minerals may be divided into four major groups, mainly in terms of the variation in the layered structure. The kaolinite group has three members, including kaolinite, dickite, and nacrite, each with a formula of Al<Subscript>2</Subscript>Si<Subscript>2</Subscript>O<Subscript>5</Subscript>(OH)<Subscript>4</Subscript>. The same formula indicates that the members of this group are polymorphs, meaning that they have same formula but different structures. Each member is composed of silicate sheets (Si<Subscript>2</Subscript>O<Subscript>5</Subscript>) bonded to aluminum oxide/hydroxide layers Al<Subscript>2</Subscript>(OH)<Subscript>4</Subscript> the two types of layers are tightly bonded. These clays are used as fillers in ceramics, paint, rubber, paper, plastics and as plasticizer in molding sands. Montmorillonite, talc, pyrophyllite, saponite, and nontronite are a few members of the larger smectite clay group. The general formula for the chemical structure of this group is (Ca,Na,H)(Al,Mg,Fe,Zn)<Subscript>2</Subscript>(Si,Al)<Subscript>4O</Subscript>10(OH)<Subscript>2</Subscript>XH<Subscript>2</Subscript>O. The important difference among the members of this group is seen in the chemical characteristics. The layer structure contains silicate layers sandwiching an aluminum oxide/hydroxide layer (Al<Subscript>2</Subscript>(OH)<Subscript>4</Subscript>) [<CitationRef CitationID="CR8">8</CitationRef>].</Para>
              <Para>One of the members, talc, has traditional presence in facial powder. ‘Nanoclay’ is the term generally used when referring to a clay mineral with a phyllosilicate or sheet structure with a thickness of the order of 1 nm and surfaces of perhaps 50 150 nm in one dimension. The mineral base can be natural or synthetic and is hydrophilic. The clay surfaces can be modified with specific methods to render them organophilic and therefore compatible with organic polymers adsorption of clay minerals can occur in two forms, selective and non-selective [<CitationRef CitationID="CR9">9</CitationRef>]. Selective sorption is also named as specific or non-exchangeable sorption, while non-selective sorption is named as exchangeable sorption. In the process of non-selective adsorption, clay minerals adsorb ions through exchangeable cations and electrostatically bound counter ions in the diffusion layer which is formed because of the surface charge. Such ions are electrostatically bound to the surface and the surface charge is a factor controlling the sorption density. Selectively adsorbed ions are assumed to form inner sphere complexes with surface hydroxide sites that is, they are primarily bound to the surface of clay minerals by chemical bond. This bond is highly dependent upon the structure of the surface and its interaction with a specific ion and, hence, exhibits substantially different behavior from electrostatic bonding. In recent decades, clays such as montmorillonite, vermiculite, kaolinite, and mica were reported as inexpensive products in a large diversity of industrial processes, pharmaceutical industry, cosmetics, organic synthesis, and environmental remediation [<CitationRef CitationID="CR10">10</CitationRef>]. In this paper we have used amino-functionalized clay and carboxyl-functionalized MWCNTs to synthesize the nanocomposite.</Para>
            </Section1>
            <Section1 ID="Sec2">
              <Heading>Results and discussion</Heading>
              <Section2 ID="Sec3">
                <Heading>The SEM analysis of nanocomposite</Heading>
                <Para>The failure surface of the MMT, OMMT, and clay-CNTs nanocomposite e film was observed via SEM. The SEM photographs were taken with a magnification 625,1,250, and ×2,500 at room temperature. The study of the SEM images shows the differences between the MMT, OMMT, and clay-CNTs nanocomposite the reference sample (Figures <InternalRef RefID="Fig3">3</InternalRef>, <InternalRef RefID="Fig4">4</InternalRef>, and <InternalRef RefID="Fig5">5</InternalRef>).<Figure ID="Fig3" Float="Yes" Category="Standard">
                    <Caption xml:lang="en" Language="En">
                      <CaptionNumber>Figure 3</CaptionNumber>
                      <CaptionContent>
                        <SimplePara>
                          <Emphasis Type="Bold">SEM image of montmorillonite.</Emphasis>
                        </SimplePara>
                      </CaptionContent>
                    </Caption>
                    <MediaObject>
                      <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_Article_15_Fig3_HTML.jpg"/>
                    </MediaObject>
                  </Figure><Figure ID="Fig4" Float="Yes" Category="Standard">
                    <Caption xml:lang="en" Language="En">
                      <CaptionNumber>Figure 4</CaptionNumber>
                      <CaptionContent>
                        <SimplePara>
                          <Emphasis Type="Bold">SEM image of modified montmorillonite.</Emphasis>
                        </SimplePara>
                      </CaptionContent>
                    </Caption>
                    <MediaObject>
                      <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_Article_15_Fig4_HTML.jpg"/>
                    </MediaObject>
                  </Figure><Figure ID="Fig5" Float="Yes" Category="Standard">
                    <Caption xml:lang="en" Language="En">
                      <CaptionNumber>Figure 5</CaptionNumber>
                      <CaptionContent>
                        <SimplePara>
                          <Emphasis Type="Bold">SEM image of clay-CNTs nanocomposite.</Emphasis>
                        </SimplePara>
                      </CaptionContent>
                    </Caption>
                    <MediaObject>
                      <ImageObject Type="Halftone" Rendition="HTML" Format="JPEG" Color="Color" FileRef="https://static-content.springer.com/image/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_Article_15_Fig5_HTML.jpg"/>
                    </MediaObject>
                  </Figure></Para>
              </Section2>
            </Section1>
            <Section1 ID="Sec4">
              <Heading>Conclusion</Heading>
              <Para>Because of a hydrophilic nature of the MMT, the organo-modification of the MMT is an important step in the preparation of MMT/HMDA composite. The organo-modification could change the hydrophilicity of Na-MMT to be more hydrophobic to generate a microchemical environment appropriate to the intercalation of MMT. Furthermore, the increase in the layer-to-layer spacing in the MMT is also desired. Primary aliphatic amines such as hexamethylenediamine and quaternary ammonium salts have been the most effective modifying reagents in the preparation of MMT/HMDA composite. Findings from the SEM images show that OMMT and clay-CNTs nanocomposite have uniform disperse and unique morphology. The clay-CNTs nanocomposite is favorable for many applications such as nanosponges for oil split remediation.</Para>
            </Section1>
            <Section1 ID="Sec5">
              <Heading>Methods</Heading>
              <Section2 ID="Sec6">
                <Heading>Materials</Heading>
                <Para>Montmorillonite (clay) was purchased from Fluka (Fluka Chemical Corporation, St. Gallen, Switzerland) and carboxyl-functionalized MWCNTs were purchased from China. In all experiment, double distilled water was used.</Para>
              </Section2>
              <Section2 ID="Sec7">
                <Heading>Synthesis of organic montmorillonite</Heading>
                <Para>A 1.0 g of MMT was added in 40 mL of distilled water then mixed and heated to 80°C for 15.0 min. In a solution of 2.5 mM HCL, 0.04 g of hexamethylenediamine was added and mixed, and then the two abovementioned solutions were mixed together overnight at 80°C. The precipitate was removed by filtration and washed with distilled water, then dried in an oven at 60°C overnight.</Para>
              </Section2>
              <Section2 ID="Sec8">
                <Heading>Synthesis of clay-CNTs nanocomposite</Heading>
                <Para>A 0.1 g of the as-synthesized organoclay is mixed in 30 mL of DMF and sonicated for 40 min. Then 0.1 g of carboxyl-functionalized MWCNTs is added and heated to 90°C and refluxed for 24 h. The precipitate is washed with distilled water and dried in an oven.</Para>
              </Section2>
              <Section2 ID="Sec9">
                <Heading>Characterization</Heading>
                <Para>The as-synthesized nanocomposite has been characterized by scanning electron microscopy (SEM) using a XL-30 Philips model scanning electron microscope (Royal Philips Electronics, Amsterdam, The Netherlands).</Para>
              </Section2>
            </Section1>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <ArticleNote Type="Misc">
              <Heading>Competing interest</Heading>
              <SimplePara>The author declares no competing interest.</SimplePara>
            </ArticleNote>
            <Acknowledgments>
              <Heading>Acknowledgment</Heading>
              <SimplePara>The author would be like to thank Islamic Azad University, Shahr-e Qods Branch for the kindly support.</SimplePara>
            </Acknowledgments>
            <Appendix OutputMedium="Online" ID="App1">
              <Section1 ID="Sec10">
                <Heading>Authors’ original submitted files for images</Heading>
                <Para>Below are the links to the authors’ original submitted files for images.<MediaObject ID="MOESM1">
                    <DataObject FileSize="3275728" FileRef="https://static-content.springer.com/esm/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_15_MOESM1_ESM.tiff">
                      <Caption xml:lang="en" Language="En">
                        <CaptionContent>
                          <SimplePara>Authors’ original file for figure 1</SimplePara>
                        </CaptionContent>
                      </Caption>
                    </DataObject>
                  </MediaObject><MediaObject ID="MOESM2">
                    <DataObject FileSize="712788" FileRef="https://static-content.springer.com/esm/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_15_MOESM2_ESM.tiff">
                      <Caption xml:lang="en" Language="En">
                        <CaptionContent>
                          <SimplePara>Authors’ original file for figure 2</SimplePara>
                        </CaptionContent>
                      </Caption>
                    </DataObject>
                  </MediaObject><MediaObject ID="MOESM3">
                    <DataObject FileSize="1096900" FileRef="https://static-content.springer.com/esm/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_15_MOESM3_ESM.tiff">
                      <Caption xml:lang="en" Language="En">
                        <CaptionContent>
                          <SimplePara>Authors’ original file for figure 3</SimplePara>
                        </CaptionContent>
                      </Caption>
                    </DataObject>
                  </MediaObject><MediaObject ID="MOESM4">
                    <DataObject FileSize="1015696" FileRef="https://static-content.springer.com/esm/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_15_MOESM4_ESM.tiff">
                      <Caption xml:lang="en" Language="En">
                        <CaptionContent>
                          <SimplePara>Authors’ original file for figure 4</SimplePara>
                        </CaptionContent>
                      </Caption>
                    </DataObject>
                  </MediaObject><MediaObject ID="MOESM5">
                    <DataObject FileSize="807628" FileRef="https://static-content.springer.com/esm/art%3A10.1186%2F2193-8865-3-24/MediaObjects/40097_2013_15_MOESM5_ESM.tiff">
                      <Caption xml:lang="en" Language="En">
                        <CaptionContent>
                          <SimplePara>Authors’ original file for figure 5</SimplePara>
                        </CaptionContent>
                      </Caption>
                    </DataObject>
                  </MediaObject></Para>
              </Section1>
            </Appendix>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Iijima</FamilyName>
                  </BibAuthorName>
                  <Year>1991</Year>
                  <ArticleTitle xml:lang="en" Language="En">Helical microtubules of graphitic carbon</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>354</VolumeID>
                  <FirstPage>56</FirstPage>
                  <LastPage>58</LastPage>
                  <BibArticleDOI>10.1038/354056a0</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK38Xmt1Ojtg%3D%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/354056a0</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Iijima S: <Emphasis Type="Bold">Helical microtubules of graphitic carbon.</Emphasis> <Emphasis Type="Italic">Nature</Emphasis> 1991, <Emphasis Type="Bold">354:</Emphasis> 56–58. 10.1038/354056a0</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Endo</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>HW</Initials>
                    <FamilyName>Kroto</FamilyName>
                  </BibAuthorName>
                  <Year>1992</Year>
                  <ArticleTitle xml:lang="en" Language="En">Formation of carbon nanofibers</ArticleTitle>
                  <JournalTitle>J. Phys. Chem</JournalTitle>
                  <VolumeID>96</VolumeID>
                  <FirstPage>6941</FirstPage>
                  <LastPage>6944</LastPage>
                  <BibArticleDOI>10.1021/j100196a017</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK38XkvFGqsL8%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/j100196a017</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Endo M, Kroto HW: <Emphasis Type="Bold">Formation of carbon nanofibers.</Emphasis> <Emphasis Type="Italic">J. Phys. Chem.</Emphasis> 1992, <Emphasis Type="Bold">96:</Emphasis> 6941–6944. 10.1021/j100196a017</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>TW</Initials>
                    <FamilyName>Ebbesen</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>PM</Initials>
                    <FamilyName>Ajayan</FamilyName>
                  </BibAuthorName>
                  <Year>1992</Year>
                  <ArticleTitle xml:lang="en" Language="En">Large scale synthesis of carbon nanotubes</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>358</VolumeID>
                  <FirstPage>220</FirstPage>
                  <LastPage>222</LastPage>
                  <BibArticleDOI>10.1038/358220a0</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK38Xls1akt7w%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/358220a0</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Ebbesen TW, Ajayan PM: <Emphasis Type="Bold">Large scale synthesis of carbon nanotubes.</Emphasis> <Emphasis Type="Italic">Nature</Emphasis> 1992, <Emphasis Type="Bold">358:</Emphasis> 220–222. 10.1038/358220a0</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Oberlin</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Endo</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Koyama</FamilyName>
                  </BibAuthorName>
                  <Year>1976</Year>
                  <ArticleTitle xml:lang="en" Language="En">Filamentous growth of carbon through benzene decomposition</ArticleTitle>
                  <JournalTitle>J. Cryst. Growth</JournalTitle>
                  <VolumeID>32</VolumeID>
                  <FirstPage>335</FirstPage>
                  <LastPage>349</LastPage>
                  <BibArticleDOI>10.1016/0022-0248(76)90115-9</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaE28XhsFyisr8%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/0022-0248(76)90115-9</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Oberlin A, Endo M, Koyama T: <Emphasis Type="Bold">Filamentous growth of carbon through benzene decomposition.</Emphasis> <Emphasis Type="Italic">J. Cryst. Growth</Emphasis> 1976, <Emphasis Type="Bold">32:</Emphasis> 335–349. 10.1016/0022-0248(76)90115-9</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <CitationNumber>5.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Iijima</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Ichihashi</FamilyName>
                  </BibAuthorName>
                  <Year>1993</Year>
                  <ArticleTitle xml:lang="en" Language="En">Single-shell carbon nanotubes of 1-nm diameter</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>363</VolumeID>
                  <FirstPage>603</FirstPage>
                  <LastPage>605</LastPage>
                  <BibArticleDOI>10.1038/363603a0</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK3sXltVOrs7o%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/363603a0</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Iijima S, Ichihashi T: <Emphasis Type="Bold">Single-shell carbon nanotubes of 1-nm diameter.</Emphasis> <Emphasis Type="Italic">Nature</Emphasis> 1993, <Emphasis Type="Bold">363:</Emphasis> 603–605. 10.1038/363603a0</BibUnstructured>
              </Citation>
              <Citation ID="CR6">
                <CitationNumber>6.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>DS</Initials>
                    <FamilyName>Bethune</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>CH</Initials>
                    <FamilyName>Kiang</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>MS</Initials>
                    <FamilyName>deVries</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>G</Initials>
                    <FamilyName>Gorman</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Savoy</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>J</Initials>
                    <FamilyName>Vazquez</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Beyers</FamilyName>
                  </BibAuthorName>
                  <Year>1993</Year>
                  <ArticleTitle xml:lang="en" Language="En">Cobalt catalyzed growth of carbon nanotubes with single atomic-layer walls</ArticleTitle>
                  <JournalTitle>Nature</JournalTitle>
                  <VolumeID>363</VolumeID>
                  <FirstPage>605</FirstPage>
                  <LastPage>607</LastPage>
                  <BibArticleDOI>10.1038/363605a0</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK3sXltVOrs7s%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1038/363605a0</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Bethune DS, Kiang CH, deVries MS, Gorman G, Savoy R, Vazquez J, Beyers R: <Emphasis Type="Bold">Cobalt catalyzed growth of carbon nanotubes with single atomic-layer walls.</Emphasis> <Emphasis Type="Italic">Nature</Emphasis> 1993, <Emphasis Type="Bold">363:</Emphasis> 605–607. 10.1038/363605a0</BibUnstructured>
              </Citation>
              <Citation ID="CR7">
                <CitationNumber>7.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>DA</Initials>
                    <FamilyName>Britz</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>AN</Initials>
                    <FamilyName>Khlobystov</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <ArticleTitle xml:lang="en" Language="En">Noncovalent interactions of molecules with single walled carbon nanotubes</ArticleTitle>
                  <JournalTitle>Chem. Soc. Rev</JournalTitle>
                  <VolumeID>35</VolumeID>
                  <FirstPage>637</FirstPage>
                  <LastPage>659</LastPage>
                  <BibArticleDOI>10.1039/b507451g</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD28Xmt1Glsr4%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1039/b507451g</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Britz DA, Khlobystov AN: <Emphasis Type="Bold">Noncovalent interactions of molecules with single walled carbon nanotubes.</Emphasis> <Emphasis Type="Italic">Chem. Soc. Rev.</Emphasis> 2006, <Emphasis Type="Bold">35:</Emphasis> 637–659. 10.1039/b507451g</BibUnstructured>
              </Citation>
              <Citation ID="CR8">
                <CitationNumber>8.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Avouris</FamilyName>
                  </BibAuthorName>
                  <Year>2002</Year>
                  <ArticleTitle xml:lang="en" Language="En">Molecular electronics with carbon nanotubes</ArticleTitle>
                  <JournalTitle>Acc. Chem. Res</JournalTitle>
                  <VolumeID>35</VolumeID>
                  <FirstPage>1026</FirstPage>
                  <LastPage>1034</LastPage>
                  <BibArticleDOI>10.1021/ar010152e</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD38Xls1GgurY%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ar010152e</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Avouris P: <Emphasis Type="Bold">Molecular electronics with carbon nanotubes.</Emphasis> <Emphasis Type="Italic">Acc. Chem. Res.</Emphasis> 2002, <Emphasis Type="Bold">35:</Emphasis> 1026–1034. 10.1021/ar010152e</BibUnstructured>
              </Citation>
              <Citation ID="CR9">
                <CitationNumber>9.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>ET</Initials>
                    <FamilyName>Thostenson</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>ZF</Initials>
                    <FamilyName>Ren</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>TW</Initials>
                    <FamilyName>Chou</FamilyName>
                  </BibAuthorName>
                  <Year>2001</Year>
                  <ArticleTitle xml:lang="en" Language="En">Advances in the science and technology of carbon nanotubes and their composites: a review</ArticleTitle>
                  <JournalTitle>Compos. Sci. Technol</JournalTitle>
                  <VolumeID>61</VolumeID>
                  <FirstPage>1899</FirstPage>
                  <LastPage>1912</LastPage>
                  <BibArticleDOI>10.1016/S0266-3538(01)00094-X</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD3MXnt1Wisbg%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1016/S0266-3538(01)00094-X</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Thostenson ET, Ren ZF, Chou TW: <Emphasis Type="Bold">Advances in the science and technology of carbon nanotubes and their composites: a review.</Emphasis> <Emphasis Type="Italic">Compos. Sci. Technol.</Emphasis> 2001, <Emphasis Type="Bold">61:</Emphasis> 1899–1912. 10.1016/S0266-3538(01)00094-X</BibUnstructured>
              </Citation>
              <Citation ID="CR10">
                <CitationNumber>10.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>YL</Initials>
                    <FamilyName>Zhao</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>JF</Initials>
                    <FamilyName>Stoddart</FamilyName>
                  </BibAuthorName>
                  <Year>2009</Year>
                  <ArticleTitle xml:lang="en" Language="En">Noncovalent functionalization of single-walled carbon nanotubes</ArticleTitle>
                  <JournalTitle>Acc. Chem. Res</JournalTitle>
                  <VolumeID>421</VolumeID>
                  <FirstPage>1161</FirstPage>
                  <LastPage>1171</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ar900056z</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Zhao YL, Stoddart JF: <Emphasis Type="Bold">Noncovalent functionalization of single-walled carbon nanotubes.</Emphasis> <Emphasis Type="Italic">Acc. Chem. Res.</Emphasis> 2009, <Emphasis Type="Bold">421:</Emphasis> 1161–1171.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


