<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <IssueHeader>
          <EditorGroup>
            <Editor>
              <EditorName DisplayOrder="Western">
                <GivenName>Oellien</GivenName>
                <FamilyName>Frank</FamilyName>
              </EditorName>
            </Editor>
          </EditorGroup>
        </IssueHeader>
        <Article OutputMedium="All" ID="BMC1752-153X-3-S1-O3">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="BriefCommunication">
            <ArticleID>147</ArticleID>
            <ArticleDOI>10.1186/1752-153X-3-S1-O3</ArticleDOI>
            <ArticleCitationID>O3</ArticleCitationID>
            <ArticleSequenceNumber>7</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">New open drug activity data at EBI</ArticleTitle>
            <ArticleCategory>Oral presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Steinbeck et al; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2009</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>C</GivenName>
                  <FamilyName>Steinbeck</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>B</GivenName>
                  <FamilyName>Al-Lazikani</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>H</GivenName>
                  <FamilyName>Hermjakob</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>J</GivenName>
                  <FamilyName>Overington</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>J</GivenName>
                  <FamilyName>Thornton</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.225360.0</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000097097726</OrgID>
                <OrgName>European Bioinformatics Institute</OrgName>
                <OrgAddress>
                  <State>Hinxton</State>
                  <City>Cambridge</City>
                  <Postcode>CB10 1SD</Postcode>
                  <Country>UK</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Public Domain</Keyword>
              <Keyword>Team Leader</Keyword>
              <Keyword>Target Class</Keyword>
              <Keyword>Chemistry Literature</Keyword>
              <Keyword>Informatics System</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>The Wellcome Trust has awarded £4.7 million to the European Bioinformatics Institute (EBI) to support the transfer of a large collection of information on the properties and activities of drugs and a large set of drug-like small molecules from BioFocus DPI, part of the publicly listed company Galapagos to the public domain.</Para>
            <Para>The databases will be incorporated into EBI&apos;s collection of open-access data resources for biomedical research and will be maintained by a newly established team at the EBI. These data lie at the heart of translating information from the human genome into successful new drugs in the clinic.</Para>
            <Para>The databases to be brought into the public domain include DrugStore™ (database of known drugs), StARLITe™ (database of known compounds and their effects), Strudle™ (binding site drugability), and Kinase SARfari™ and GPCR SARfari™ (informatics systems for the most widely used target classes in drug discovery). The main database, StARLITe, on Drug-Target interactions alone has hundreds of thousands of interaction data points manually curated from the medicinal chemistry literature.</Para>
            <Para>A new team leader will be appointed to support the new resource and my group will provide the chemoinformatics expertise to move the underlying analysis software into the open source world. The Chemistry Development Kit (CDK) will play a major role both in freeing the QSAR code as well as in providing (sub-) structure and similarity searching to the database.</Para>
            <Para>The transfer will empower academia to participate in the first stages of drug discovery for all therapeutic areas, including major diseases of the developing world. In future it could also result in improved prediction of drug side-effects and spark all kinds of new academic research directions.</Para>
            <Para>This talk will outline the resources to be brought into the public domain, discuss the integration into EBI resources and give a view on our future plans of developments.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


