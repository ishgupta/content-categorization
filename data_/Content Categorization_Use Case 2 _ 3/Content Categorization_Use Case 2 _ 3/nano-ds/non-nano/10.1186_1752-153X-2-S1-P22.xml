<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <Article OutputMedium="All" ID="BMC1752-153X-2-S1-P22">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="Abstract">
            <ArticleID>97</ArticleID>
            <ArticleDOI>10.1186/1752-153X-2-S1-P22</ArticleDOI>
            <ArticleCitationID>P22</ArticleCitationID>
            <ArticleSequenceNumber>39</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">Workflow-based identification of bioisosteric replacements for molecular scaffolds</ArticleTitle>
            <ArticleCategory>Poster presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2008</Year>
                <Month>03</Month>
                <Day>26</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2008</Year>
                <Month>03</Month>
                <Day>26</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Höhfeld et al.</CopyrightHolderName>
              <CopyrightYear>2008</CopyrightYear>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>2</VolumeIDStart>
              <VolumeIDEnd>2</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1 Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Kerstin</GivenName>
                  <FamilyName>Höhfeld</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Andreas</GivenName>
                  <FamilyName>Teckentrup</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff2">
                <AuthorName DisplayOrder="Western">
                  <GivenName>Timothy</GivenName>
                  <FamilyName>Clark</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgID Type="GRID" Level="Institution">grid.420061.1</OrgID>
                <OrgName>Boehringer Ingelheim Pharma GmbH &amp; Co. KG</OrgName>
                <OrgAddress>
                  <City>Biberach (Riss)</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
              <Affiliation ID="Aff2">
                <OrgID Type="GRID" Level="Institution">grid.5330.5</OrgID>
                <OrgID Type="ISNI" Level="Institution">0000000121073311</OrgID>
                <OrgName>Friedrich-Alexander-Universität Erlangen-Nürnberg, Computer-Chemie-Centrum and Interdisciplinary Center for Molecular Materials</OrgName>
                <OrgAddress>
                  <City>Erlangen</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Electronic Property</Keyword>
              <Keyword>Electrostatic Potential</Keyword>
              <Keyword>Geometric Constraint</Keyword>
              <Keyword>Molecular Surface</Keyword>
              <Keyword>Lead Structure</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>Scaffolds are defined as the core structure of a molecule connecting different substituents or functional groups. Defining bioisosteric alternative scaffolds for lead structures is an important task in drug design to improve e.g. the activity, bioavailability or selectivity of the structures concerned [<CitationRef CitationID="CR1">1</CitationRef>]. In this context, bioisosteric replacement means changing the scaffold structure while retaining important biological properties of the molecule, e.g. binding affinity or solubility.</Para>
            <Para>The general aim of changing the compound class can be extended by simultaneously retaining the orientation of a set of substituents. Computational methods that propose new scaffolds and consider the constraints mentioned already exist [<CitationRef CitationID="CR2">2</CitationRef>–<CitationRef CitationID="CR4">4</CitationRef>]. For example, a few geometrical approaches are implemented, that also consider e.g. H-bond-donor-acceptor properties (Recore) or interaction energy values of a molecule to different virtual probes (SHOP).</Para>
            <Para>A workflow is presented that identifies proposals for alternative scaffolds with an intensive consideration of bioisosteric replacements. It is assumed, that electronic characteristics of the molecular surface, like the electrostatic potential, represent the behavior of a molecule, especially the binding situation, in the active site. Therefore, a geometric scaffold search is combined with the investigation of semiempirically calculated molecular electronic characteristics.</Para>
            <Para>For the search, a query structure must be determined that consists of geometric constraints consisting of the bonds connecting the query substituents to the core of the molecule and the angles between them. The workflow performs a search for alternative scaffold structures in the molecules of the database. It searches for scaffolds with bonds that are geometrically similar to the defined query bonds and that therefore can conserve the orientation of the query substituents.</Para>
            <Para>New molecules are constructed by connecting the scaffolds identified with the query substituents. In a subsequent module, the influence of the new scaffold on the electronic properties of the substituents is investigated as well as the influence of the substituents on the scaffold. For this reason, local electronic surface properties are calculated with the program ParaSurf [<CitationRef CitationID="CR5">5</CitationRef>] for the molecules constructed.</Para>
            <Para>On this poster, retrospective examples are presented, for which the workflow retrieves the expected scaffolds with similar electronic property patterns.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>G</Initials>
                    <FamilyName>Schneider</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Schneider</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>S</Initials>
                    <FamilyName>Renner</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <NoArticleTitle/>
                  <JournalTitle>QSAR Comb Sci</JournalTitle>
                  <VolumeID>38</VolumeID>
                  <FirstPage>1162</FirstPage>
                  <LastPage>1171</LastPage>
                </BibArticle>
                <BibUnstructured>Schneider G, Schneider P, Renner S: QSAR Comb Sci. 2007, 38: 1162-1171.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>G</Initials>
                    <FamilyName>Lauri</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>PA</Initials>
                    <FamilyName>Bartlett</FamilyName>
                  </BibAuthorName>
                  <Year>1994</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Comput Aided Mol Des</JournalTitle>
                  <VolumeID>8</VolumeID>
                  <FirstPage>51</FirstPage>
                  <LastPage>61</LastPage>
                  <BibArticleDOI>10.1007/BF00124349</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DyaK2cXksFKls7w%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/BF00124349</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Lauri G, Bartlett PA: J Comput Aided Mol Des. 1994, 8: 51-61. 10.1007/BF00124349.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>P</Initials>
                    <FamilyName>Maass</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Schulz-Gasch</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Stahl</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>M</Initials>
                    <FamilyName>Rarey</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Chem Inf Model</JournalTitle>
                  <VolumeID>47</VolumeID>
                  <FirstPage>390</FirstPage>
                  <LastPage>399</LastPage>
                  <BibArticleDOI>10.1021/ci060094h</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2sXhvVWjtbk%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/ci060094h</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Maass P, Schulz-Gasch T, Stahl M, Rarey M: J Chem Inf Model. 2007, 47: 390-399. 10.1021/ci060094h.</BibUnstructured>
              </Citation>
              <Citation ID="CR4">
                <CitationNumber>4.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>R</Initials>
                    <FamilyName>Bergmann</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>A</Initials>
                    <FamilyName>Linusson</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>I</Initials>
                    <FamilyName>Zamora</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <NoArticleTitle/>
                  <JournalTitle>J Med Chem</JournalTitle>
                  <VolumeID>50</VolumeID>
                  <FirstPage>2708</FirstPage>
                  <LastPage>2117</LastPage>
                  <BibArticleDOI>10.1021/jm061259g</BibArticleDOI>
                  <Occurrence Type="COI">
                    <Handle>1:CAS:528:DC%2BD2sXltVKqtrg%3D</Handle>
                  </Occurrence>
                  <Occurrence Type="DOI">
                    <Handle>10.1021/jm061259g</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Bergmann R, Linusson A, Zamora I: J Med Chem. 2007, 50: 2708-2117. 10.1021/jm061259g.</BibUnstructured>
              </Citation>
              <Citation ID="CR5">
                <CitationNumber>5.</CitationNumber>
                <BibUnstructured>ParaSurf&apos;06, Cepos InSilico Ltd., Ryde, UK. 2006, (<ExternalRef>
                    <RefSource>http://www.ceposinsilico.com</RefSource>
                    <RefTarget TargetType="URL" Address="http://www.ceposinsilico.com"/>
                  </ExternalRef>)</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


