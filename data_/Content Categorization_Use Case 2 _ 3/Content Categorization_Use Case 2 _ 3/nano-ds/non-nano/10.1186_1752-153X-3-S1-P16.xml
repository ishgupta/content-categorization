<?xml version="1.0" encoding="UTF-8"?>


<Publisher>
  <dds:export xmlns:dds="http://www.springer.com/dds2casper">
    <dds:flags/>
    <dds:sites>
      <dds:site>BMC</dds:site>
      <dds:site>RD</dds:site>
    </dds:sites>
    <dds:projects/>
  </dds:export>
  
  <Journal OutputMedium="All">
    
    <Volume OutputMedium="All">
      
      <Issue OutputMedium="All" IssueType="Supplement">
        
        <IssueHeader>
          <EditorGroup>
            <Editor>
              <EditorName DisplayOrder="Western">
                <GivenName>Oellien</GivenName>
                <FamilyName>Frank</FamilyName>
              </EditorName>
            </Editor>
          </EditorGroup>
        </IssueHeader>
        <Article OutputMedium="All" ID="BMC1752-153X-3-S1-P16">
          <ArticleInfo xml:lang="en" TocLevels="0" OutputMedium="All" NumberingStyle="Unnumbered" Language="En" ContainsESM="No" ArticleType="Abstract">
            <ArticleID>172</ArticleID>
            <ArticleDOI>10.1186/1752-153X-3-S1-P16</ArticleDOI>
            <ArticleCitationID>P16</ArticleCitationID>
            <ArticleSequenceNumber>32</ArticleSequenceNumber>
            <ArticleTitle xml:lang="en" OutputMedium="All" Language="En">New and improved features of the docking software PLANTS</ArticleTitle>
            <ArticleCategory>Poster presentation</ArticleCategory>
            <ArticleFirstPage>1</ArticleFirstPage>
            <ArticleLastPage>1</ArticleLastPage>
            <ArticleHistory>
              <RegistrationDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </RegistrationDate>
              <OnlineDate>
                <Year>2009</Year>
                <Month>6</Month>
                <Day>05</Day>
              </OnlineDate>
            </ArticleHistory>
            <ArticleCopyright>
              <CopyrightHolderName>Exner et al; licensee BioMed Central Ltd.</CopyrightHolderName>
              <CopyrightYear>2009</CopyrightYear>
              <CopyrightComment>
                <SimplePara>This article is published under license to BioMed Central Ltd.</SimplePara>
              </CopyrightComment>
            </ArticleCopyright>
            <ArticleGrants Type="OpenChoice">
              <MetadataGrant Grant="OpenAccess"/>
              <AbstractGrant Grant="OpenAccess"/>
              <BodyPDFGrant Grant="OpenAccess"/>
              <BodyHTMLGrant Grant="OpenAccess"/>
              <BibliographyGrant Grant="OpenAccess"/>
              <ESMGrant Grant="OpenAccess"/>
            </ArticleGrants>
            <ArticleContext>
              <JournalID>13065</JournalID>
              <VolumeIDStart>3</VolumeIDStart>
              <VolumeIDEnd>3</VolumeIDEnd>
              <IssueIDStart>S1</IssueIDStart>
              <IssueIDEnd>S1</IssueIDEnd>
            </ArticleContext>
          </ArticleInfo>
          <ArticleHeader>
            <AuthorGroup>
              <Author CorrespondingAffiliationID="Aff1" AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>TE</GivenName>
                  <FamilyName>Exner</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>O</GivenName>
                  <FamilyName>Korb</FamilyName>
                </AuthorName>
              </Author>
              <Author AffiliationIDS="Aff1">
                <AuthorName DisplayOrder="Western">
                  <GivenName>T</GivenName>
                  <FamilyName>ten Brink</FamilyName>
                </AuthorName>
              </Author>
              <Affiliation ID="Aff1">
                <OrgDivision>Fachbereich Chemie</OrgDivision>
                <OrgName>Universität Konstantz</OrgName>
                <OrgAddress>
                  <Postcode>D-78457</Postcode>
                  <City>Konstanz</City>
                  <Country>Germany</Country>
                </OrgAddress>
              </Affiliation>
            </AuthorGroup>
            <KeywordGroup xml:lang="en" Source="AutomaticallyGenerated" Language="En">
              <Heading>Keywords</Heading>
              <Keyword>Experimental Data</Keyword>
              <Keyword>Water Molecule</Keyword>
              <Keyword>REcognition System</Keyword>
              <Keyword>Late Development</Keyword>
              <Keyword>Additional Degree</Keyword>
            </KeywordGroup>
          </ArticleHeader>
          <Body Type="XML">
            <Para>We will summarize the latest developments for our protein-ligand docking software PLANTS (Protein-Ligand ANT System) [<CitationRef CitationID="CR1">1</CitationRef>][<CitationRef CitationID="CR2">2</CitationRef>][<CitationRef CitationID="CR3">3</CitationRef>]. This will include the pre-processing of the ligands and the protein performed by the program SPORES (Structure PrOtonation and REcognition System), parameterization of a new scoring function, as well as the inclusion of additional degrees of freedom into the docking process like flexible side chains and essential water molecules. The use of constraints from experimental data for improving the docking poses will be presented. Finally, an outlook towards the possible usage of the PLANTS approach for the flexible alignment of multiple ligands will be given.</Para>
          </Body>
          <BodyRef TargetType="OnlinePDF"/>
          <ArticleBackmatter>
            <Bibliography ID="Bib1">
              <Heading>References</Heading>
              <Citation ID="CR1">
                <CitationNumber>1.</CitationNumber>
                <BibChapter>
                  <BibAuthorName>
                    <Initials>O</Initials>
                    <FamilyName>Korb</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Stützle</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>TE</Initials>
                    <FamilyName>Exner</FamilyName>
                  </BibAuthorName>
                  <Year>2008</Year>
                  <ChapterTitle xml:lang="en" Language="En">Empirical Scoring Functions for Advanced Protein-Ligand Docking with PLANTS</ChapterTitle>
                  <BookTitle>J Comp Chem</BookTitle>
                </BibChapter>
                <BibUnstructured>Korb O, Stützle T, Exner TE: Empirical Scoring Functions for Advanced Protein-Ligand Docking with PLANTS. J Comp Chem. 2008.</BibUnstructured>
              </Citation>
              <Citation ID="CR2">
                <CitationNumber>2.</CitationNumber>
                <BibArticle>
                  <BibAuthorName>
                    <Initials>O</Initials>
                    <FamilyName>Korb</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Stützle</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>TE</Initials>
                    <FamilyName>Exner</FamilyName>
                  </BibAuthorName>
                  <Year>2007</Year>
                  <ArticleTitle xml:lang="en" Language="En">An ant colony optimization approach to flexible protein-ligand docking</ArticleTitle>
                  <JournalTitle>Swarm Intell</JournalTitle>
                  <VolumeID>1</VolumeID>
                  <FirstPage>115</FirstPage>
                  <LastPage>134</LastPage>
                  <Occurrence Type="DOI">
                    <Handle>10.1007/s11721-007-0006-9</Handle>
                  </Occurrence>
                </BibArticle>
                <BibUnstructured>Korb O, Stützle T, Exner TE: An ant colony optimization approach to flexible protein-ligand docking. Swarm Intell. 2007, 1: 115-134.</BibUnstructured>
              </Citation>
              <Citation ID="CR3">
                <CitationNumber>3.</CitationNumber>
                <BibChapter>
                  <BibAuthorName>
                    <Initials>O</Initials>
                    <FamilyName>Korb</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>T</Initials>
                    <FamilyName>Stützle</FamilyName>
                  </BibAuthorName>
                  <BibAuthorName>
                    <Initials>TE</Initials>
                    <FamilyName>Exner</FamilyName>
                  </BibAuthorName>
                  <Year>2006</Year>
                  <ChapterTitle xml:lang="en" Language="En">PLANTS: Application of Ant Colony Optimization to Structure-Based Drug Design</ChapterTitle>
                  <BookTitle>Lecture Notes in Computer Science 4150</BookTitle>
                  <FirstPage>247</FirstPage>
                  <LastPage>258</LastPage>
                </BibChapter>
                <BibUnstructured>Korb O, Stützle T, Exner TE: PLANTS: Application of Ant Colony Optimization to Structure-Based Drug Design. Lecture Notes in Computer Science 4150. 2006, 247-258.</BibUnstructured>
              </Citation>
            </Bibliography>
          </ArticleBackmatter>
        </Article>
      </Issue>
    </Volume>
  </Journal>
</Publisher>


