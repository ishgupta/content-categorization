# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 11:30:58 2018

@author: ishgupta

extract important terms from the data

"""

# In[]
from nltk import sent_tokenize, word_tokenize, pos_tag, FreqDist, bigrams
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import string
from itertools import chain
import copy
import numpy as np
from autocorrect import spell
import re
from ordered_set import OrderedSet
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import requests
from pymongo import MongoClient
import logging
import csv
# In[]

def extract_phrases_v1(corpus, requested_terms = None, term_freq_threshold = 1, spell_correction = False, consolidated = False):
    # lower
    # lemmatization
    
    if consolidated :
        corpus = sent_tokenize(corpus)
    
    bigram_docs = extract_bigrams(corpus, spell_correction)
    
    # consolidate bigrams of the coprus, I did not dissolve everything since individual document bigrams are 
    # also needed in further processing
    corpus_bigram = list(chain(*bigram_docs))
    
    # freq of all corpus
    corpus_freq = dict(FreqDist(corpus_bigram).items())
    corpus_bigram = None
    
    # stitch related phrases in the corpus together
    corpus_freq = stitch_related_phrases(bigram_docs, corpus_freq, term_freq_threshold)
    bigram_docs = None
    
    # sort the corpus in descending order of the frequency
    corpus_freq = dict(filter(lambda x : x[1] > term_freq_threshold, list(corpus_freq.items())))
    corpus_freq = sorted(corpus_freq.items(), key = lambda x : x[0][1], reverse = True)
    corpus_freq = dict(corpus_freq)
    
    # initialize stop words
    stop_words = stopwords.words('english')
    stop_words.extend(" ".join(stop_words).title().split())
    stop_words.extend(string.punctuation)
    # stop_words = set(stop_words)
    
    # keys of the frequency distribution of corpus contains the found phrases
    phrases = list(corpus_freq)
    corpus_freq = None
    
    translator = str.maketrans(' ', ' ', string.punctuation + string.digits + '±')
    
    # remove stop words
    print("strip of stopwords using {0}".format(stop_words))
    phrases = list(map(lambda x : " ".join(OrderedSet(strip_surrounding_stopwords(x, stop_words))).translate(translator).strip(), phrases))
        
    # stop words with initials capital were not removed for their possibility of coming up in any abbreviation.
    stop_words = (" ".join(stop_words).title().split())
    phrases = list(set([x.translate(translator) for x in phrases if x not in stop_words and len(x) > 1]))
    
    if '' in phrases:
        phrases.remove('')
    
    # filter the requested number of phrases
    if requested_terms is not None and requested_terms < len(phrases):
        phrases = phrases[:requested_terms]
    
    return phrases
# In[]
    
"""
    extract_phrases -> extract_word_phrases
    requested_terms -> number_of_terms
    corpus -> data
    spell_correction -> "removed"
    consolidated -> "removed"
    bigram_docs -> list_of_bigram_docs
    corpus_bigram -> bigrams_for_corpus
"""
# the corpus is of documents, and not just the sentences
def extract_phrases(corpus, requested_terms = None, term_freq_threshold = 1, spell_correction = False, consolidated = False, tf_idf = False):
    # lower
    # lemmatization
    logging.debug("extracting phrases for the data: {0}".format(corpus))
    if consolidated :
        logging.info("phrase extraction requested to be consolidated")
        if isinstance(corpus, str):
           corpus = sent_tokenize(corpus)
        
    
    bigram_docs = extract_bigrams(corpus, spell_correction)
    logging.debug("bigrams as extracted: {0}".format(bigram_docs))
    # consolidate bigrams of the coprus, I did not dissolve everything since individual document bigrams are 
    # also needed in further processing
    corpus_bigram = list(chain(*bigram_docs))
    
    # freq of all corpus
    if tf_idf:
        term_freq_threshold = 0
        tf = TfidfVectorizer(analyzer='word', min_df = term_freq_threshold, ngram_range = (1, 1), stop_words = 'english')
        tmp_corpus_bigram = [x[0] + ' ' + x[1] for x in corpus_bigram]
        tfidf_matrix =  tf.fit_transform(tmp_corpus_bigram)
        dense = tfidf_matrix.todense()
        sum_mat = np.sum(dense, axis=1).tolist()
        corpus_freq = dict((corpus_bigram[index], sum_mat[index][0]) for index in range(0, len(sum_mat)))
        tmp_corpus_bigram = None
    else:
        corpus_freq = dict(FreqDist(corpus_bigram).items())
    
    corpus_bigram = None
    
    # stitch related phrases in the corpus together
    corpus_freq = stitch_related_phrases(bigram_docs, corpus_freq, term_freq_threshold)
    
    bigram_docs = None
    
    # sort the corpus in descending order of the frequency
    if tf_idf:
        corpus_freq = dict(filter(lambda x : x[1] > 1.0, list(corpus_freq.items())))
    else:
        corpus_freq = dict(filter(lambda x : x[1] > term_freq_threshold, list(corpus_freq.items())))
            
    if tf_idf:
        corpus_freq = sorted(corpus_freq.items(), key = lambda x : x[1], reverse = True)
    else:
        corpus_freq = sorted(corpus_freq.items(), key = lambda x : x[0][1], reverse = True)
        
    corpus_freq = dict(corpus_freq)
    print(str(corpus_freq))
    # initialize stop words
    stop_words = stopwords.words('english')
    stop_words.extend(" ".join(stop_words).title().split())
    stop_words.extend(string.punctuation)
    # stop_words = set(stop_words)
    
    # keys of the frequency distribution of corpus contains the found phrases
    phrases = list(corpus_freq)
    corpus_freq = None
    
    translator = str.maketrans(' ', ' ', string.digits + '±')
    range_utf_8_chars = [8192, 8304]
    utf_8_special_chars_table = dict((char, None) for char in range(range_utf8_chars[0], range_utf8_chars[1] + 1))
    translator.update(utf_8_special_chars_table)
    
    """
    # remove stop words
    phrases = list(map(lambda x : " ".join(OrderedSet(x).difference(stop_words)).translate(translator).strip(), phrases))
    
    # stop words with initials capital were not removed for their possibility of coming up in any abbreviation.
    stop_words = (" ".join(stop_words).title().split())
    phrases = list(set([x.translate(translator) for x in phrases if x not in stop_words and len(x) > 1]))
    """
    
    # remove stopwords, punctuation and digits, and join the words into phrases
    phrases = list(set(map(lambda x : str(strip_surrounding_stopwords(" ".join(x), stop_words).translate(translator).strip()), phrases)))
    #phrases = list(map(lambda x : " ".join(OrderedSet(strip_surrounding_stopwords(x, stop_words))).translate(translator).strip(), phrases))
    
    if '' in phrases:
        phrases.remove('')
    
    phrases = remove_urls(phrases)
    
    # filter the requested number of phrases
    if requested_terms is not None and requested_terms < len(phrases):
        phrases = phrases[:requested_terms]
    
    return phrases

# In[]
def extract_important_features_using_tf_idf_v1(data, term_count = 20, ngrams = 1, consolidate = False):
    tf = TfidfVectorizer(analyzer='word', min_df = 0, ngram_range = (1, ngrams), stop_words = 'english')
    
    if isinstance(data, list) and len(data) > 0 and consolidate:
        data = [" ".join(data)]
    elif not isinstance(data, list) and len(data) > 0 and not consolidate:
        data = sent_tokenize(data)
    elif not isinstance(data, list) and len(data) > 0 and consolidate:
        data = [data]
    
    top_features = {}
    # print("processing {0} documents".format(len(data)))
    
    if len(data) > 0:
        tfidf_matrix =  tf.fit_transform(data)
        feature_names = np.array(tf.get_feature_names())
        #print(feature_names)
        dense = tfidf_matrix.todense()
        phrase_scores = {}
        
        # find phrases which have score > 0
        for doc_id in range(0, len(dense)):
            tmp = dense[doc_id].tolist()[0]
            phrase_scores[doc_id] = [pair for pair in zip(range(0, len(tmp)), tmp) if pair[1] > 0]
            
            # sort the scores in descending order
            sorted_phrase_scores = sorted(phrase_scores[doc_id], key=lambda t: t[1] * -1)
            
            top_features[doc_id] = list(set(feature_names[list(map(lambda x : x[0], sorted_phrase_scores[:term_count]))]))
    
    
    return top_features
    
# In[]
def extract_important_features_using_tf_idf(data, term_count = 20, ngrams = 1, consolidate = False, count_vectorizer = False, term_freq_threshold = 5):
    if count_vectorizer:
        tf = CountVectorizer(analyzer='word', min_df = 0, ngram_range = (1, ngrams), stop_words = 'english')
    else:
        tf = TfidfVectorizer(analyzer='word', min_df = 0, ngram_range = (1, ngrams), stop_words = 'english')
    
    if isinstance(data, list) and len(data) > 0 and consolidate:
        data = [" ".join(data)]
    elif not isinstance(data, list) and len(data) > 0 and not consolidate:
        data = sent_tokenize(data)
    elif not isinstance(data, list) and len(data) > 0 and consolidate:
        data = [data]
    
    top_features = {}
    # print("processing {0} documents".format(len(data)))
    
    if len(data) > 0:
        tfidf_matrix =  tf.fit_transform(data)
        feature_names = np.array(tf.get_feature_names())
        #print(feature_names)
        dense = tfidf_matrix.todense()    
        
        if not consolidate:
            phrase_scores = {}
            # find phrases which have score > 0
            for doc_id in range(0, len(dense)):
                tmp = dense[doc_id].tolist()[0]
                if count_vectorizer:
                    phrase_scores[doc_id] = [pair for pair in zip(range(0, len(tmp)), tmp) if pair[1] > term_freq_threshold]
                else:                
                    phrase_scores[doc_id] = [pair for pair in zip(range(0, len(tmp)), tmp) if pair[1] > 0]
                
                # sort the scores in descending order
                sorted_phrase_scores = sorted(phrase_scores[doc_id], key=lambda t: t[1] * -1)            
                top_features[doc_id] = list(set(feature_names[list(map(lambda x : x[0], sorted_phrase_scores))]))
        else:
            sum_mat = np.sum(dense, axis=0).tolist()[0]
            feature_score = [(x, y) for x,y in zip(feature_names, sum_mat)]
            feature_score = sorted(feature_score, key = lambda x : x[1], reverse = True)
            top_features['0'] = list(set(dict(np.array(feature_score)).keys()))
       
    translator = str.maketrans('', '', string.digits)
    
    for doc, features in top_features.items():
        top_features[doc] = OrderedSet(x.translate(translator).strip() for x in features)
        
        if '' in top_features[doc]:
            top_features[doc].remove('')
        
        if term_count is not None:
            top_features[doc] = np.array(top_features[doc])[:term_count]
        else:
            top_features[doc] = np.array(top_features[doc])
            
    return top_features

# In[]
# no change
# corpus is a list of documents
def stitch_related_phrases(bigram_docs, corpus_freq, term_freq_threshold = 1):
    bigrams_to_be_removed = []
    num_of_bigrams_dissolved = 0
    
    for bigram_content in bigram_docs:
        previous_bigram = ()
        for bigram in bigram_content:
            new_bigram = None
            # print(previous_bigram, bigram)
            
            if previous_bigram != () and corpus_freq[bigram] > term_freq_threshold and corpus_freq[bigram] == corpus_freq[previous_bigram]:
                
                #print("{0} #{1} == {2} #{3}".format(previous_bigram, corpus_freq[previous_bigram], bigram, corpus_freq[bigram]))
                new_bigram = (*previous_bigram, bigram[1])
                corpus_freq[new_bigram] = corpus_freq[bigram]
                #print("new bigram: {0} added".format(new_bigram))
                bigrams_to_be_removed.append(bigram)
                bigrams_to_be_removed.append(previous_bigram)
                
                num_of_bigrams_dissolved += 2
                
            if new_bigram is not None:
                previous_bigram = new_bigram
            else:
                previous_bigram = bigram

    for tmp_bigram in bigrams_to_be_removed:
        if tmp_bigram in corpus_freq:
            corpus_freq.pop(tmp_bigram)
    
    
    return corpus_freq

# In[]
    
def filter_nouns_v1(words):
    nltk_noun_identifiers = [ "NN", "NNS", "NNP", "NNPS" ]
    words = word_tokenize(words) if not isinstance(words, list) else words
    
    nouns = {}
    
    for word in words:
        noun = None
        if word is not None and 'NA' not in word:
            annotated_word = pos_tag(word.split())
            tmp = OrderedSet(map(lambda x : x[0] if x[1] in nltk_noun_identifiers else 'NA', annotated_word))
            noun = tmp if len(tmp) == len(annotated_word) else None
        
        if noun is not None:
            if 'NA' in noun:
                noun.remove('NA')
            
            noun = " ".join(noun)
            
            if noun != '':
                nouns[word] = noun
    
    
    return nouns

# In[]
    

"""
    filter nouns from the terms and phrases
"""
def filter_nouns(phrases):
    noun_phrases = []
    
    for phrase in phrases:
        tmp_phrase = phrase.split()
        
        if len(tmp_phrase) > 1:
            tmp_phrase = [singularize(x) for x in tmp_phrase]
        
        tagged_phrase = pos_tag(tmp_phrase)

        noun_phrases.append(phrase) if len([term for term, tag in tagged_phrase if tag.startswith('NN')]) == len(tagged_phrase) else 'NA'

    noun_phrases = list(set(noun_phrases))

    return sorted(noun_phrases)

# In[]
# no change    
def extract_bigrams(list_of_docs, spell_correction = False):
    list_of_bigram_docs = []
    
    for doc in list_of_docs:
        if doc is not None and len(doc) > 0:
            if not isinstance(doc, list):
                #print("tokenize: {0} for bigrams".format(doc))
                doc = word_tokenize(doc)
                if spell_correction:
                    doc = list(map(lambda x : spell(re.compile(r"(.)\1{2,}").sub(r"\1\1", x)), doc))
                
            list_of_bigram_docs.append(list(bigrams(doc)))
    
    return list_of_bigram_docs

# In[]
# decommissioned
def aggregate_bigram_corpus(list_of_bigram_docs):
    text_bigrams = None
    
    if len(list_of_bigram_docs) > 0:
        text_bigrams = copy.deepcopy(list_of_bigram_docs[0])
        for bigram_doc in list_of_bigram_docs:
            text_bigrams.extend(bigram_doc)
    
    return text_bigrams

# In[]
def singularize(word):
    lemma = WordNetLemmatizer()

    tagged_word = pos_tag(word.split())
    tag = tagged_word[0][1]
    
    tmp_pos = 'n' if tag.startswith('NN') else 'v' if tag.startswith('VB') else 'a'
    new_word = lemma.lemmatize(word, pos = tmp_pos)

    return new_word
# In[]

def singularize_phrase(phrase):
    lemma= WordNetLemmatizer()
    phrase = phrase.split()
    new_phrase = []

    pos = pos_tag(phrase)

    for word, tag in pos:
        tmp_pos = 'n' if tag.startswith('NN') else 'v' if tag.startswith('VB') else 'a'

        new_word = lemma.lemmatize(word, pos = tmp_pos)
        new_phrase.append(new_word)
    return " ".join(new_phrase).strip()

# In[]
def strip_surrounding_stopwords(x, stop_words):
    #translator = str.maketrans('', '', string.punctuation + string.digits)
    suffix = False
    prefix = False
    word_received = x
    
    # x = x.translate(translator).strip()
    #print("strip '{0}' of stopwords".format(x))
    
    if len(x) > 0:
        logging.debug("phrase : '{0}'".format(x))
        
        if isinstance(x, str):
            x = OrderedSet(x.split())
        
        
        if x[0] in stop_words:
            prefix = True
        
        # check if the phrase is of length > 1 and contains stop_words at last position
        if len(x) > 1 and x[len(x) - 1] in stop_words:
           suffix = True
        
        if prefix:
            x.remove(x[0])
            if suffix:
                x.pop()
        elif suffix:
            x.pop()
        
        #print("phrase updated to: {0}".format(x))
        if len(x) > 0 and (x[0] in stop_words or x[len(x) - 1] in stop_words):
            x = OrderedSet(strip_surrounding_stopwords(" ".join(x), stop_words).split())
    else:
        return ''
    
    # print("'{0}' ==> {1}".format(word_received, x))
    
    return " ".join(x).strip()


# In[]
def read_csv(file_name):
    data = []
    with open(file_name, encoding='utf-8') as data_file:
        reader = csv.DictReader(data_file, dialect='excel-tab')
        for row in reader:
            data.append(row)
    return data    

# In[]
def extract_data(data_dir):
    data = {}
    files = list(data_dir.iterdir())
    for file in files:
        file = Path(file)
        if file.is_file():
            print("\n\n\nreading file: {0}".format(file))
            tmp_data = None
            if file.suffix == '.docx':
                tmp_data = docx2txt.process(file)
            elif file.suffix == '.doc':
                new_file = 'data/txt/' + str(file.name.replace(file.suffix, '.txt'))
                Path(new_file).touch()
                cmd = r'C:\antiword_v2\antiword\antiword.exe -f ' + data_dir.name + '/' + file.name + ' > ' + new_file
                subprocess.Popen(cmd, shell=True)
                tmp_file = 'data/txt/' + file.name.replace(file.suffix, '.txt')
                tmp_data = textract.process(tmp_file)
            elif file.suffix == '.csv' or file.suffix == '.tsv':
                tmp_data = read_csv(file)
            else:
                print("\n{0} is a {1} file".format(file, file.suffix))
                
            if tmp_data is not None:
                data[file.name] = tmp_data
    return data
# In[]
def remove_urls(list_of_words):
    url_reg_expression_format = r"(\w+[./:?&%]+\w+[./:?&%]*){1,}"
    list_of_words = list(filter(lambda x : re.search(url_reg_expression_format, str(x)) is None, list_of_words))

    return list_of_words
    
# In[]
    
def remove_urls(list_of_words):
    url_reg_expression_format = r"(\w+[./:?&%]+\w+[./:?&%]*){1,}"
    list_of_words = list(filter(lambda x : re.search(url_reg_expression_format, str(x)) is None, list_of_words))

    return list_of_words
# In[]
    
def fetch_data_from_concept_net(data, relations, use_proxy = True):
    db_name = 'test'
    db_collection = 'concept_net'
    db_address = "localhost:27017"
    db = MongoClient(db_address).get_database(db_name).get_collection(db_collection)
    
    concept_net_uri = "http://api.conceptnet.io"
    concept_net_concept_notation = 'c'
    concept_net_language = 'en'
    concept_net_primary_identifier = '@id'
    
    proxy_ss = "http://10.135.0.29:8080"
    
    if isinstance(data, str):
        data = data.split()
    
    # construct URLs for all words in the requested data
    concept_net_request_iris = dict(map(lambda x : (x, concept_net_uri + '/' + concept_net_concept_notation + '/' + concept_net_language + '/' + x + '?offset=0&limit=1000'), data))
    concept_net_responses = {}

    # fetch and collate response for each word requested in the data
    for word, concept_net_request_iri in concept_net_request_iris.items():
        # check the node in local database:
        concept_net_response = db.find({concept_net_primary_identifier : '/' + concept_net_concept_notation + '/' + concept_net_language + '/' + word})
        
        if concept_net_response.count() == 0:
            print("requesting data for", concept_net_request_iri)
            if use_proxy:
                concept_net_response = requests.get(concept_net_request_iri, proxies = {'http' : proxy_ss}).json()
            else:
                concept_net_response = requests.get(concept_net_request_iri).json()
                
            if concept_net_response is not None:
                tmp_response = copy.deepcopy(concept_net_response)
                while 'view' in list(tmp_response.keys()): 
                    if 'nextPage' in list(tmp_response['view'].keys()):
                        if use_proxy:
                            tmp_response = requests.get(concept_net_uri + tmp_response['view']['nextPage'], proxies = {'http' : proxy_ss}).json()
                        else:
                            tmp_response = requests.get(concept_net_uri + tmp_response['view']['nextPage']).json()
                        concept_net_response['edges'].extend(tmp_response['edges'])
                    else:
                        break
                tmp_response = None
                db.insert_one(concept_net_response)
                concept_net_responses[word] = concept_net_response
        else:
            print("found data for {0} locally".format(word))
            concept_net_responses[word] = concept_net_response.next()
            
    # filter and aggregate concept net data
    concept_net_data = {}
    for word, concept_net_response in concept_net_responses.items():
        for relation in relations:
            end_tags = [tmp['start']['label'] for tmp in concept_net_response['edges'] if tmp['@type'] == 'Edge' and 
                                                    tmp['rel']['label'] == relation and tmp['start']['language'] == concept_net_language and tmp['end']['language'] == concept_net_language]
            start_tags = [tmp['end']['label'] for tmp in concept_net_response['edges'] if tmp['@type'] == 'Edge' and 
                                                    tmp['rel']['label'] == relation and tmp['start']['language'] == concept_net_language and tmp['end']['language'] == concept_net_language]
            
            if len(start_tags) > 0 or len(end_tags) > 0:
                if word not in concept_net_data:
                    concept_net_data[word] = {}
                    
                if relation not in concept_net_data[word]:
                    concept_net_data[word][relation] = []
                    
                concept_net_data[word][relation].extend(start_tags)
                concept_net_data[word][relation].extend(end_tags)
                concept_net_data[word][relation] = list(set(concept_net_data[word][relation]))
            else:
                if word not in concept_net_data:
                    concept_net_data[word] = {}
                    concept_net_data[word]['Synonym'] = [word]
                
    return concept_net_data

# In[]
def find_entities_using_wiki(search_string):
    wiki_url_1 = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" + search_string + "&language=en&format=json"
    first_search = requests.get(wiki_url_1).json()
    
    first_search_results = list(map(lambda x : (x['label'] if 'label' in x.keys() and x['match']['language'] == 'en' else 'NA', 
                                                    x['description'] if 'description' in x.keys() and x['match']['language'] == 'en' else 'NA'), first_search['search']))
    return first_search_results
    
    """ids = list(map(lambda x : x['id'], first_search['search']))
    
    second_search_ids =[]
    for search_id in ids:
        wiki_url_2 = "https://www.wikidata.org/w/api.php?action=wbgetclaims&entity=" + search_id + "&format=json&property=P31"
        second_search = requests.get(wiki_url_2).json()
        if 'mainsnak' in second_search.keys():
            second_search_ids.extend(list(map(lambda x : x['datavalue']['value']['id'], second_search['mainsnak']['claims']['P31'])))
    
    final_search_values = []
    final_search_responses = []
    for search_id in second_search:
        wiki_url_3 = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=" + search_id + "&format=json"
        final_search_responses.append(requests.get(wiki_url_3).json())
        #final_search['entities'][search_id]['labels']
    
    final_search_responses = set(filter(lambda x : x if 'entities' in x.keys() else 'NA', final_search_responses))
    final_search_responses.remove('NA')
    final_search_responses = list(final_search_responses)
    
    entities = list(map(lambda x : x['entities'][search_id]['labels']['en']['value'] if 'en' in x['entities'][search_id]['labels'].keys() else 'NA', final_search_responses))
    
    print(entities)
    """
# In[]
"""
if __name__ == "__main__":
    logging.basicConfig(filename = "logs/nlp_utilities.log", filemode='a', format = "%(asctime)s:%(msecs)03d %(levelname)s %(filename)s:%(lineno)d %(message)s"
	, datefmt = "%Y-%m-%d %H:%M:%S", level = 'logging.DEBUG')
"""